<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->string('title');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('order_name');
            $table->text('address');
            $table->string('postcode');
            $table->string('mobile');
            $table->string('alt_tel');
            $table->string('email');
            $table->tinyInteger('work_permit');
            $table->tinyInteger('driving_licence');
            $table->integer('created-by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_applications');
    }
}
