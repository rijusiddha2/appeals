<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_references', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_application_id');
            $table->string('name_1');
            $table->string('relationship_1');
            $table->string('telephone_1');
            $table->string('email_1');
            $table->string('name_2');
            $table->string('relationship_2');
            $table->string('telephone_2');
            $table->string('email_2');
            $table->string('dates_unavailable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_references');
    }
}
