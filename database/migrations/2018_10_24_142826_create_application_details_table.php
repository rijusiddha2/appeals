<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id');
            $table->text('work_experience');
            $table->text('triratna_history');
            $table->string('contact_source');
            $table->enum('health', ['good', 'reasonable', 'poor']);
            $table->text('health_details');
            $table->boolean('medication');
            $table->text('medication_details');
            $table->boolean('safeguarding_offence');
            $table->boolean('criminal_offence');
            $table->text('offence_details');
            $table->enum('support_package', ['1', '2', '0']);
            $table->text('support_extras');
            $table->boolean('ethos');
            $table->text('ethos_details');
            $table->boolean('privacy');
            $table->boolean('future_opportunities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_details');
    }
}
