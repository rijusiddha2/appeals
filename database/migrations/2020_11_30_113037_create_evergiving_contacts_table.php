<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvergivingContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evergiving_contacts', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('evergiving_id');
            $table->string('title');
            $table->string('firstName');
            $table->string('keyname');
            $table->string('emailAddress');
            $table->string('AddressLine1');
            $table->string('AddressLine2');
            $table->string('postcode');
            $table->string('source');
            $table->unsignedTinyInteger('dmEmailOptIn')->default('1');
            $table->unsignedTinyInteger('dmMailOptIn')->default('1');
            $table->unsignedTinyInteger('doNotContact')->default('0');
            $table->unsignedTinyInteger('doNotEmail')->default('0');
            $table->unsignedTinyInteger('doNotMail')->default('0');
            $table->unsignedTinyInteger('doNotPhone')->default('0');
            $table->unsignedTinyInteger('doNotSMS')->default('0');
            $table->unsignedTinyInteger('emailThirdParty')->default('0');
            $table->unsignedTinyInteger('MailThirdParty')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evergiving_contacts');
    }
}
