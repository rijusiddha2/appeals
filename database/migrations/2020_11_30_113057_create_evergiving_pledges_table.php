<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvergivingPledgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evergiving_pledges', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('everging_id');
            $table->string('sourceCode');
            $table->string('paymentFrequency');
            $table->string('paymentType');
            $table->unsignedInteger('instalmentValue');
            $table->unsignedInteger('amount');
            $table->date('start_date')->nullable();
            $table->unsignedInteger('paymentDay')->nullable();
            $table->string('accountName')->nullable();
            $table->unsignedInteger('accountNumber')->nullable();
            $table->unsignedInteger('sortCode')->nullable();
            $table->string('bankName')->nullable();
            $table->string('bankAddress')->nullable();
            $table->string('bankPostcode')->nullable();
            $table->date('accountVerified')->nullable();
            $table->string('accountVerifiedby')->nullable();
            $table->date('DDIStartDate')->nullable();
            $table->date('DDIDateReceived')->nullable();
            $table->string('taxClaimable')->default('yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evergiving_pledges');
    }
}
