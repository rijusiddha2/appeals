<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationAppealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_appeals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id');
            $table->enum('type', ['door to door', 'telephone']);
            $table->enum('season', ['winter', 'spring', 'summer', 'autumn']);
            $table->year('year');
            $table->text('comments');
            $table->text('draw');
            $table->text('skills');
            $table->text('difficulties');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_appeals');
    }
}
