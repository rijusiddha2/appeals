<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('block_id');
            $table->text('text');
            $table->integer('image');
            $table->string('title');
            $table->string('caption');
            $table->integer('slider');
            $table->integer('column_width');
            $table->string('link');
            $table->string('link_text');
            $table->string('faq');
            $table->boolean('parallax');
            $table->integer('page_content_id');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_templates');
    }
}
