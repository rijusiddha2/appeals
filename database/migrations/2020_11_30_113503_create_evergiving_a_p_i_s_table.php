<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvergivingAPISTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evergiving_a_p_i_s', function (Blueprint $table) {
            $table->id();
            $table->string('api_key');
            $table->integer('campaign_id')->unsigned();
            $table->integer('revision_id')->unsigned();
            $table->integer('schema_id')->unsigned();
            $table->string('type');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evergiving_a_p_i_s');
    }
}
