<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationRefereesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_referees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id');
            $table->string('name_1');
            $table->string('relationship_1');
            $table->string('telephone_1');
            $table->string('email_1');
            $table->string('name_2');
            $table->string('relationship_2');
            $table->string('telephone_2');
            $table->string('email_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_referees');
    }
}
