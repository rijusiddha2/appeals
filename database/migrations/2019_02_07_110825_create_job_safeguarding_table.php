<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobSafeguardingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_safeguarding', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_application_id');
            $table->tinyInteger('vunerable_sanctions');
            $table->tinyInteger('criminal_offences');
            $table->text('safeguarding_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_safeguarding');
    }
}
