<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Front\PageController@index')->name('home');

Route::get('404', function(){
    abort(404);
});
Route::get('404', function(){
    abort(500);
});
Route::get('errors', function () {
    return view('errors.list');
});
Auth::routes();
Route::prefix('admin')->group(function () {


    Route::get('', 'Admin\DashboardController@index')->name('dashboard');


    Route::prefix('users')->group(function () {
        Route::get('/data', 'Admin\UserController@data')->name('users.data');
        Route::get('/{user}/delete', 'Admin\UserController@destroy')->name('users.delete');
    });
    Route::resource('users', 'Admin\UserController');

    Route::prefix('menus')->group(function () {
        Route::get('/', 'Admin\MenuController@index')->name('menus');
        Route::post('/add-page', 'Admin\MenuController@addPage')->name('haddpagemmenu');

    });


    Route::prefix('pages')->group(function () {
        Route::get('/data', 'Admin\PageController@data')->name('pages.data');
        Route::get('/grapesjs', 'Admin\PageController@getGrapesJs')->name('pages.grapesjs');
        Route::post('/grapesjs/store', 'Admin\PageController@storeGrapesJs')->name('pages.store-grapesjs');
        Route::get('/{page}/delete', 'Admin\PageController@destroy')->name('pages.delete');
        Route::get('/{page}/content/edit', 'Admin\PageController@editContent')->name('pages.edit-content');
        Route::get('/{page}/content/add', 'Admin\PageController@addContent')->name('pages.add-content');
        Route::post('/{page}/content/add', 'Admin\PageController@storeContent')->name('pages.store-content');
        Route::post('/{page}/content/edit', 'Admin\PageController@updateContent')->name('pages.update-content');

    });
    Route::resource('pages', 'Admin\PageController');

    Route::prefix('templates')->group(function () {
        Route::post('/add-content-block', 'Admin\TemplateController@addContentBlock')->name('templates.add-content-block');///AJAX
        Route::get('/data', 'Admin\TemplateController@data')->name('templates.data');
        Route::get('/{template}/delete', 'Admin\TemplateController@destroy')->name('templates.delete');
        Route::get('/{ContentTemplate}/remove/', 'Admin\TemplateController@removeContentBlock')->name('templates.remove-block');
        Route::post('/sortContent', 'Admin\TemplateController@sortContent')->name('templates.sort-content');
    });
    Route::resource('templates', 'Admin\TemplateController');

    Route::prefix('image-uploader')->group(function () {
        Route::get('/data', 'Admin\ImageController@data')->name('image-uploader.data');
        Route::get('/{image}/delete', 'Admin\ImageController@destroy')->name('image-uploader.delete');
        Route::get('/picture-manager/', 'Admin\ImageController@selectImage')->name('images.picture-manager');
        Route::post('/add-image', 'Admin\ImageController@addImage')->name('images.add-image');
        Route::post('/process-sizes', 'Admin\ImageController@processSizes')->name('images.process-sizes');
        Route::post('/save-crop', 'Admin\ImageController@saveCrop')->name('images.saveCrop');
        Route::post('/upload', 'Admin\ImageController@upload')->name('images.upload');

    });
    Route::resource('image-uploader', 'Admin\ImageController');

    Route::prefix('tags')->group(function () {
        Route::get('/data', 'Admin\TagController@data')->name('tags.data');
        Route::get('/{tag}/delete', 'Admin\TagController@destroy')->name('tags.delete');
    });
    Route::resource('tags', 'Admin\TagController');

    Route::prefix('image-sizes')->group(function () {
        Route::get('/{imageSize}/delete', 'Admin\ImageSizeController@destroy')->name('image-sizes.delete');
    });
    Route::resource('image-sizes', 'Admin\ImageSizeController');

    Route::prefix('blocks')->group(function () {
        Route::get('/data', 'Admin\BlockController@data')->name('blocks.data');
        Route::get('/{block}/delete', 'Admin\BlockController@destroy')->name('blocks.delete');
    });
    Route::resource('blocks', 'Admin\BlockController');

    Route::prefix('content')->group(function () {
        Route::get('/data', 'Admin\ContentController@data')->name('content.data');
        Route::get('/{content}/delete', 'Admin\ContentController@destroy')->name('content.delete');
    });
    Route::resource('content', 'Admin\ContentController');

    Route::prefix('slideshows')->group(function () {
        Route::get('/data', 'Admin\SlideshowController@data')->name('slideshows.data');
        Route::get('/{slideshow}/delete', 'Admin\SlideshowController@destroy')->name('slideshows.delete');
        Route::get('/{slideshow}/add-slides', 'Admin\SlideshowController@addSlides')->name('slideshows.add-slides');
        Route::post('/store-slides', 'Admin\SlideshowController@storeSlides')->name('slideshows.store-slides');
        Route::get('/{slide}/delete-slide', 'Admin\SlideshowController@deleteSlide')->name('slideshows.delete-slide');
        Route::post('/sortSlides', 'Admin\SlideshowController@sortSlides')->name('slideshows.sort-slides');
    });

    Route::resource('slideshows', 'Admin\SlideshowController');

    Route::prefix('permissions')->group(function () {
        Route::get('/data', 'Admin\PermissionController@data')->name('permissions.data');
        Route::get('/{permission}/delete', 'Admin\PermissionController@destroy')->name('permissions.delete');
    });

    Route::resource('permissions', 'Admin\PermissionController');

    Route::prefix('roles')->group(function () {
        Route::get('/data', 'Admin\RoleController@data')->name('roles.data');
        Route::get('/{role}/delete', 'Admin\RoleController@destroy')->name('roles.delete');
    });

    Route::resource('roles', 'Admin\RoleController');

    Route::prefix('applications')->group(function () {
        Route::get('/data', 'Admin\ApplicationController@data')->name('applications.data');
        Route::get('/{application}/delete', 'Admin\ApplicationController@destroy')->name('applications.delete');
        Route::get('/{application}/note', 'Admin\ApplicationController@createNote')->name('applications.note');
        Route::post('/{application}/note', 'Admin\ApplicationController@storeNote');
        Route::get('/note/{applicationNote}/delete', 'Admin\ApplicationController@deleteNote')->name('applications.note.delete');
        Route::get('/note/{applicationNote}', 'Admin\ApplicationController@editNote')->name('applications.note.edit');
        Route::put('/note/{applicationNote}', 'Admin\ApplicationController@updateNote')->name('applications.note.update');
        Route::get('/{application}/pdf','Admin\ApplicationController@printPDF')->name('applications.pdf');
    });

    Route::resource('applications', 'Admin\ApplicationController');

    Route::prefix('contacts')->group(function () {
        Route::get('/data', 'Admin\ContactController@data')->name('contacts.data');
        Route::get('/{contact}/delete', 'Admin\ContactController@destroy')->name('contacts.delete');
    });

    Route::resource('contacts', 'Admin\ContactController');

    Route::prefix('faqs')->group(function () {
        Route::get('/data', 'Admin\FaqController@data')->name('faqs.data');
        Route::get('/{faq}/delete', 'Admin\FaqController@destroy')->name('faqs.delete');
    });

    Route::resource('faqs', 'Admin\FaqController');

    Route::prefix('documents')->group(function () {
//        Route::get('/data', 'Admin\FaqController@data')->name('faqs.data');
        Route::get('/{document}/delete', 'Admin\DocumentController@destroy')->name('documents.delete');
    });
    Route::resource('documents', 'Admin\DocumentController');

    Route::prefix('jobs-admin')->group(function () {
        Route::get('/data', 'Admin\JobController@data')->name('jobs-admin.data');
        Route::get('/{job}/delete', 'Admin\JobController@destroy')->name('jobs-admin.delete');

    });
    Route::resource('jobs-admin', 'Admin\JobController');

    Route::prefix('job-applications')->group(function () {
        Route::get('/data', 'Admin\JobApplicationController@data')->name('job-applications.data');
        Route::get('/{jobApplication}/delete', 'Admin\JobApplicationController@destroy')->name('job-applications.delete');
        Route::get('/{jobApplication}/note', 'Admin\JobApplicationController@createNote')->name('job-applications.note');
        Route::post('/{jobApplication}/note', 'Admin\JobApplicationController@storeNote');
        Route::get('/note/{jobApplicationNote}/delete', 'Admin\JobApplicationController@deleteNote')->name('job-applications.note.delete');
        Route::get('/note/{jobApplicationNote}', 'Admin\JobApplicationController@editNote')->name('job-applications.note.edit');
        Route::put('/note/{jobApplicationNote}', 'Admin\JobApplicationController@updateNote')->name('job-applications.note.update');
        Route::get('/{jobApplication}/pdf','Admin\JobApplicationController@printPDF')->name('job-applications.pdf');

    });
    Route::resource('job-applications', 'Admin\JobApplicationController');

    Route::prefix('appeals')->group(function () {
        Route::get('/data', 'Admin\AppealController@data')->name('appeals.data');
    });
    Route::resource('appeals', 'Admin\AppealController');

    Route::prefix('api')->group(function () {
        Route::get('/data', 'Admin\EvergivingAPIController@data')->name('api.data');
        Route::get('/{evergivingApi}/delete', 'Admin\EvergivingAPIController@destroy')->name('api.delete');
        Route::get('/test', 'Admin\EvergivingAPIController@test')->name('api.test');

    });
    Route::resource('api', 'Admin\EvergivingAPIController');



});

// front routes
Route::get('/application', 'Front\ApplicationController@index')->name('application');
Route::post('/application', 'Front\ApplicationController@process')->name('process');
Route::get('/contact', 'Front\ContactController@index')->name('contact');
Route::post('/contact', 'Front\ContactController@process')->name('contact-process');
Route::get('/jobs', 'Front\JobController@index')->name('job');
Route::get('/jobs/application', 'Front\JobController@create')->name('job.apply');
Route::post('/jobs/application', 'Front\JobController@store')->name('job.process');


Route::get('{pageSlug}', 'Front\PageController@index')->name('page');