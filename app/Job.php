<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Job extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'jobs';

    protected $fillable = [
        'title',
        'description',
        'caption',
        'closing_date'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'closing_date'
    ];

    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (Job $item) {

            $item->created_by = Auth::user()->id;
        });

    }
    /**
     * Get all of the user's documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }
}
