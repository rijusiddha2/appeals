<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationAppeal extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'application_id',
        'type',
        'season',
        'year',
        'comments',
        'draw',
        'skills',
        'difficulties',
    ];
}
