<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PageTemplate extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'page_id',
        'template_id',
        'block_id',
        'image',
        'title',
        'caption',
        'text',
        'column_width',
        'link',
        'link_text',
        'slider',
        'parallax',
        'faq',
        'page_content_id'
    ];

    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (PageTemplate $item) {

            $item->created_by = Auth::user()->id;
        });

    }

    /**
     * @param null $image
     * @return null
     */
    public function getImage($image = null)
    {

        $image = Image::findOrFail($image);

       return $image;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function block(){
        return $this->belongsTo('\App\Block');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function images(){
        return $this->belongsTo('\App\Image', 'image');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function slideshow(){
        return $this->belongsTo('\App\Slideshow', 'slider');
    }

    public function getFaqs($category)
    {
        $faqs = Faq::where('category', $category)->get();

        return $faqs;
    }

}
