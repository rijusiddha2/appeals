<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationCurrentHistory extends Model
{
    protected $table = 'job_history_current';

    protected $fillable = [
        'job_application_id',
        'job_title',
        'employer_details',
        'employed_from',
        'employed_to',
        'responsibilities'
    ];

    protected $dates = [
        'employed_from',
        'employed_to'
    ];
}
