<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationReferee extends Model
{
    protected $table = 'job_references';

    /**
     * @var array
     */
    protected $fillable = [
        'job_application_id',
        'name_1',
        'relationship_1',
        'telephone_1',
        'email_1',
        'name_2',
        'relationship_2',
        'telephone_2',
        'email_2',
        'dates_unavailable'
    ];
}
