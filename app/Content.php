<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'columns',
        'editable',
    ];
    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (Content $item) {

            $item->created_by = Auth::user()->id;
        });

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blocks(){
        return $this->belongsToMany('\App\Block')->withPivot('column_width','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function templates()
    {
        return $this->belongsToMany('App\Template');
    }

}