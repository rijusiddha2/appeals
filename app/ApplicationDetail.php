<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationDetail extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'application_id',
        'work_experience',
        'triratna_history',
        'contact_source',
        'health',
        'health_details',
        'medication',
        'medication_details',
        'safeguarding_offence',
        'criminal_offence',
        'offence_details',
        'support_package',
        'support_extras',
        'ethos',
        'ethos_details',
        'privacy',
        'future_opportunities',
    ];
}
