<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    protected $table = 'page_content';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
//     */
    protected $fillable = [
        'pt_id',
        'template_id',
        'page_id',
        'content_id',
        'order'
    ];

    public function block(){
        return $this->belongsToMany('\App\Block', 'block_content', 'content_id')->withPivot('column_width');
    }

    public function blocks(){
        return $this->belongsToMany('\App\Block')->withPivot('column_width','id');
    }

    public function pageTemplate(){
        return $this->hasMany('\App\PageTemplate', 'page_content_id', 'id');
    }

   public static function getBlocks($pageContentId)
   {
       $blocks = PageTemplate::where('page_content_id', $pageContentId)->get();

       return $blocks;
   }
}
