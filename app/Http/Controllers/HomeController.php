<?php

namespace App\Http\Controllers;

use Harimayco\Menu\Facades\Menu;
use Harimayco\Menu\Models\Menus;
use Illuminate\Http\Request;
use App\Page;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::getBySort('main_menu');
        $page = Page::where('url', '/')->firstOrFail();

        return view('front.home', compact('menus', 'page'));
    }
}
