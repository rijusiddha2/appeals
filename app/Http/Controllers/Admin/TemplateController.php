<?php

namespace App\Http\Controllers\Admin;

use App\BlockTemplate;
use Illuminate\Http\Request;
use App\Template;
use App\Content;
use App\ContentTemplate;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class TemplateController extends Controller
{
    /**
     * TemplateController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Template');

        $templates = Template::all();

        return view('admin.templates.index', compact('templates'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $templates = Template::all();

        return Datatables::of($templates)
            ->editColumn('created_at', function ($template) {
                return $template->created_at->format('d/m/y');
            })

            ->editColumn('created_by', function ($template) {
                return $template->user->name;
            })

            ->addColumn('action', function ($template) {
                return '
               
                <a data-toggle="tooltip" data-original-title="View" href="' . route('templates.show', $template->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>
                 <a data-toggle="tooltip" data-original-title="Edit" href="' .route('templates.edit', $template->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>
                <a data-toggle="tooltip" data-original-title="Delete" href="'.route('templates.delete', $template->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>
               
                ';
            })

            ->make(true);
    }

    /**
     * @param Template $template
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show (Template $template)
    {
        Auth::user()->hasPermissionTo('View Template');

        $title = 'Template - ' . $template->name;

        $contents = Content::orderBy('name')->pluck('name', 'id')->toArray();
        $contents = ['-- Please Select --'] + $contents;

        $templateContents = ContentTemplate::select('*', 'content_template.id as content_template_id')
            ->join('contents', 'contents.id', '=', 'content_template.content_id')
            ->where('template_id', $template->id)
            ->orderBy('content_template.order')
            ->get();

        return view('admin.templates.view', compact('template', 'title', 'contents', 'templateContents'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Template');

        return view('admin.templates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Template');

        $this->validate($request, [
            'name'     =>  'required|max:120'

        ]);

        $template = Template::create($request->only('name'));

        return redirect()->route('templates.show', $template)->with('flash_message',
            'Template Created!');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Template $template)
    {
        Auth::user()->hasPermissionTo('Edit Template');

        $title = 'Edit - '. $template->name;

        return view('admin.templates.edit', compact('template', 'title'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Template $template)
    {
        Auth::user()->hasPermissionTo('Edit Template');
        $this->validate($request, [
            'name'  =>  'required|max:120'
        ]);

        $input = [
            'name'   =>  $request['name']
        ];

        $template->fill($input);
        $template->save();

        return redirect()->route('templates.show', $template)->with('flash_message',
            'Template Updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Template $template)
    {
        Auth::user()->hasPermissionTo('Delete Template');
        //Find a tag with a given id and delete
        if (isset($template)) {
            $template->delete();

            return redirect()->route('templates.index')
                ->with('flash_message',
                    'Template successfully deleted.');
        }

        return redirect()->route('templates.index')
            ->with('flash_message',
                'Cannot delete Template. Template not found');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function addContentBlock(Request $request)
    {
        $order = ContentTemplate::orderBy('order', 'desc')->first();
        $contentTemplate = ContentTemplate::create(['template_id' => $request['template'], 'content_id' => $request['content_id'], 'order' => $order->order +1]);
        $contentBlock = Content::find($request['content_id']);

        $returnHTML = '<li id="'.$contentTemplate->id.'">';

        if($contentBlock->editable == true) {
            foreach($contentBlock->blocks as $block) {
                $returnHTML .= '<div class="box">';
                $returnHTML .= '<div class="box-content">';
                $returnHTML .= '<div class="content-blocks">';
                $returnHTML .= '<div class="row">';
                $returnHTML .= '<div class="col-md-'.$block->pivot->column_width.'">';
                $returnHTML .= '<div class="alert alert-success">Content Block Added!</div>';
                $returnHTML .= view('admin.blocks.partials.'.$block->partial.'', compact('contentBlock'))->render();
                $returnHTML .= '</div>';
                $returnHTML .= '</div>';
                $returnHTML .= '<div class="row"><div class="col-md-2 col-md-offset-10"><a href="templates/1/remove" title="delete" class="btn btn-danger btn-template"><i class="fas fa-trash"></i>Remove Content Block</a></div></div>';
                $returnHTML .= '</div>';
                $returnHTML .= '</div>';
                $returnHTML .= '</div>';
                $returnHTML .= '</li>';

            }
        } else {

            $returnHTML .= '<div class="box">';
            $returnHTML .= '<div class="box-content">';
            $returnHTML .= '<div class="col-md-12">';
            $returnHTML .= view('admin.blocks.partials._uneditable', compact('contentBlock'))->render();
            $returnHTML .= '</div>';
            $returnHTML .= '</div>';
            $returnHTML .= '</div>';

        }

        $returnHTML .= '</li>';

        return response()->json( array('success' => true, 'html'=>$returnHTML) );

    }

    /**
     * @param $contentId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeContentBlock($contentId)
    {

        $contentTemplate = ContentTemplate::find($contentId);
        if (isset($contentTemplate)) {
            $contentTemplate->delete();

            return redirect()->back()
                ->with('flash_message',
                    'Content Block successfully deleted.');
        }

        return redirect()->back()
            ->with('flash_message',
                'Cannot delete content block. Content Block not found');
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function sortContent(Request $request)
    {
        $itemID = $request->itemID;
        $itemIndex = $request->itemIndex;

        ContentTemplate::where('id', $itemID)->update(array('order' => $itemIndex));
        return json_encode('success');
    }
}