<?php

namespace App\Http\Controllers\Admin;

use App\UserContract;
use App\UserRole;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View User');

        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show (User $user)
    {
        Auth::user()->hasPermissionTo('View User');

        $title = 'User - ' . $user->name;

        return view('admin.users.view', compact('user', 'title'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add User');

        $roles = Role::pluck('name', 'id')->toArray();
        $roles = ['' => '--- Please Select ---'] + $roles;
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add User');
        //Validate name, email and password fields
        $this->validate($request, [
            'name'      =>  'required|unique:users,deleted_at,NULL|max:120',
            'email'     =>  'required|email|unique:users',
            'password'  =>  'required|min:6|confirmed'
        ]);

        $user = User::create($request->only('name', 'email', 'password'));
        $user->assignRole($request['roles']);

        return redirect()->route('users.show', $user)->with('flash_message',
            'User Created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        Auth::user()->hasPermissionTo('Edit User');

        $title = 'Edit - '. $user->name;

        $roles = Role::pluck('name', 'id')->toArray();
        $roles = ['-- Please Select ---'] + $roles;

        return view('admin.users.edit', compact('user', 'title', 'roles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        Auth::user()->hasPermissionTo('Add User');
        //Validate name, email and password fields
        $this->validate($request, [
            'name'      =>  'required|unique:users,name,'.$user->id.',deleted_at,NULL|max:120',
            'email'=>'required|email|unique:users,email,'.$user->id.'',
        ]);

        $input = [
            'name'          =>  $request['name'],
            'email'         =>  $request['email'],
            'name'          =>  $request['name'],
        ];

        $user->fill($input);
        $user->save();

        //add roles
        $user->assignRole($request['roles']);

        return redirect()->route('users.show', $user)->with('flash_message',
            'User Updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        Auth::user()->hasPermissionTo('Delete User');
        //Find a user with a given id and delete
        if (isset($user)) {
            $user->delete();

            return redirect()->route('users.index')
                ->with('flash_message',
                    'User successfully deleted.');
        }

        return redirect()->route('users.index')
            ->with('flash_message',
                'Cannot delete User! User not found');

    }
}
