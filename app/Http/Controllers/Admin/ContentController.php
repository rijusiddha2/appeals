<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Content;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;

class ContentController extends Controller
{
    /**
     * @var array
     */
    protected $columns = [
        ''  =>  '-- Please Select --',
        1   =>  1,
        2   =>  2,
        3   =>  3,
        4   =>  4,
        6   =>  6,
        12  =>  12

    ];
    /**
     *  BlockController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Content');
        $contents = Content::all();

        return view('admin.content.index', compact('contents'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $contents = Content::all();

        return Datatables::of($contents)

            ->addColumn('action', function ($content) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('content.show', $content->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';

                if(Auth::user()->hasPermissionTo('Edit Content')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('content.edit', $content->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Delete Content')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('content.delete', $content->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                }
                return $menu;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * @param Content $content
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Content $content)
    {
        Auth::user()->hasPermissionTo('View Content');
        return view('admin.content.view', compact('content'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Content');

        $columns = $this->columns;

        return view('admin.content.create', compact('columns'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Content');
        //Validate name, email and password fields
        if(!isset($request['editable'])) {
            $request['editable'] = 0;
        }
        $this->validate($request, [
            'name' => 'required',
            'columns' => 'required',
            'editable' => 'required'
        ]);

        $input = $request->except('_token');

        $content = Content::create($input);

        return redirect()->route('content.show', $content)->with('flash_message',
            'Content Block Created!');
    }

    /**
     * @param Content $content
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Content $content)
    {
        Auth::user()->hasPermissionTo('Edit Content');

        $columns = $this->columns;

        return view('admin.content.edit', compact('content', 'columns'));

    }

    /**
     * @param Request $request
     * @param Content $content
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, Content $content)
    {
        Auth::user()->hasPermissionTo('Edit Content');

        $this->validate($request, [
            'name'      =>  'required',
            'columns'   =>  'required',
            'editable'  =>  'required'
        ]);

        $input = $request->except('_token');

        $content->fill($input);
        $content->save();

        return redirect()->route('content.show', $content)->with('flash_message', 'Content Block Updated!');
    }

    /**
     * @param Content $content
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Content $content)
    {
        Auth::user()->hasPermissionTo('Delete Content');
        if (isset($content)) {
            $content->delete();

            return redirect()->route('content.index')
                ->with('flash_message',
                    'Content Block successfully deleted.');
        }

        return redirect()->route('blocks.index')
            ->with('flash_message',
                'Cannot delete Content Block. Content Block not found');
    }
}
