<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Application;
use App\ApplicationNote;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Barryvdh\DomPDF\Facade as PDF;

class ApplicationController extends Controller
{
    /**
     *  ApplicationController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $applications = Application::all();

        return Datatables::of($applications)
            ->editColumn('name', function ($application) {
                return $application->title. ' '. $application->first_name. ' '. $application->last_name;
            })
            ->editColumn('created_at', function ($application) {
                return $application->created_at->format('d/m/y');
            })
            ->addColumn('action', function ($application) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="'.route('applications.show', $application->id).'" class="tooltip-pivot float-left"><i class="far fa-eye"></i></a>';
                if(Auth::user()->hasPermissionTo('Edit Application')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="'.route('applications.edit', $application->id).'" class="tooltip-pivot float-left"><i class="fas fa-pencil-alt"></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Delete Application')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('applications.delete', $application->id).'" class="tooltip-pivot float-left"><i class="fas fa-trash"></i></a>';
                }
                return $menu;
            })
            ->make(true);
    }

    /**
     * @param Application $application
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Application $application)
    {
        Auth::user()->hasPermissionTo('View Application');
        return view('admin.applications.view', compact('application'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Application');
        return view('admin.applications.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'                    => 'required',
            'last_name'                     => 'required',
            'address'                       => 'required',
            'postcode'                      => 'required',
            'email'                         => 'required|email',
            'mobile'                        => 'required',
            'details.work_experience'       => 'required',
            'details.triratna_history'      => 'required',
            'appeals.type'                  => 'required',
            'appeals.season'                => 'required',
            'appeals.draw'                  => 'required',
            'appeals.skills'                => 'required',
            'appeals.difficulties'          => 'required',
            'details.health'                => 'required',
            'details.health_details'        => 'required_if:details[health], poor',
            'details.medication'            => 'required',
            'details.medication_details'    => 'required_if:details[medication],1',
            'details.safeguarding_offence'  => 'required',
            'details.criminal_offence'      => 'required',
            'details.offence_details'       => 'required_if:details[safeguarding_offence],1|required_if:details[criminal_offence],1',
            'reference.name_1'              => 'required',
            'reference.relationship_1'      => 'required',
            'reference.telephone_1'         => 'required',
            'reference.email_1'             => 'required|email',
            'reference.name_2'              => 'required',
            'reference.relationship_2'      => 'required',
            'reference.telephone_2'         => 'required',
            'reference.email_2'             => 'required|email',
            'details.support_package'      => 'required',
            'details.ethos'                 => 'required',
            'details.ethos_details'         => 'required_if:details[ethos],0',
            'details.privacy'               => 'required',
            'details.future_opportunities'  => 'required',
        ]);

        $application = $request->except('details','reference', 'appeals');
        $details = $request->only('details');
        $appeals = $request->only('appeals');
        $reference = $request->only('reference');


        $application = Application::create($application);

        $details['details']['contact_id'] = $application->id;
        $details['details']['contact_source'] = implode(',', $details['details']['contact_source']);
        $appeals['appeals']['contact_id'] = $application->id;
        $appeals['appeals']['type'] = implode(',', $appeals['appeals']['type']);
        $reference['reference']['contact_id'] = $application->id;
        ApplicationDetail::create($details['details']);
        ApplicationAppeal::create($appeals['appeals']);
        ApplicationReferee::create($reference['reference']);

        return redirect()->route('applications.show', $application)->with('message', 'Application Completed!');
    }

    /**
     * @param Application $application
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Application $application)
    {
        Auth::user()->hasPermissionTo('Edit Application');
        return view('admin.applications.edit', compact('application'));
    }

    /**
     * @param Request $request
     * @param Application $application
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, Application $application)
    {
        $this->validate($request, [
            'first_name'                    => 'required',
            'last_name'                     => 'required',
            'address'                       => 'required',
            'postcode'                      => 'required',
            'email'                         => 'required|email',
            'mobile'                        => 'required',
            'details.work_experience'       => 'required',
            'details.triratna_history'      => 'required',
            'appeals.type'                  => 'required',
            'appeals.season'                => 'required',
            'appeals.draw'                  => 'required',
            'appeals.skills'                => 'required',
            'appeals.difficulties'          => 'required',
            'details.health'                => 'required',
          //  'details.health_details'        => 'required_if:details[health], poor',
            'details.medication'            => 'required',
            'details.medication_details'    => 'required_if:details[medication],1',
            'details.safeguarding_offence'  => 'required',
            'details.criminal_offence'      => 'required',
            'details.offence_details'       => 'required_if:details[safeguarding_offence],1|required_if:details[criminal_offence],1',
            'reference.name_1'              => 'required',
            'reference.relationship_1'      => 'required',
            'reference.telephone_1'         => 'required',
            'reference.email_1'             => 'required|email',
            'reference.name_2'              => 'required',
            'reference.relationship_2'      => 'required',
            'reference.telephone_2'         => 'required',
            'reference.email_2'             => 'required|email',
            'details.support_package'      => 'required',
            'details.ethos'                 => 'required',
            'details.ethos_details'         => 'required_if:details[ethos],0',
            'details.privacy'               => 'required',
            'details.future_opportunities'  => 'required',
        ]);

        $application_input = $request->except('details','reference', 'appeals');
        $details = $request->only('details');
        $appeals = $request->only('appeals');
        $reference = $request->only('reference');


        $application->fill($application_input);
        $application->save();

        $details['details']['contact_id'] = $application->id;
        $details['details']['contact_source'] = implode(',', $details['details']['contact_source']);
        $appeals['appeals']['contact_id'] = $application->id;
        $appeals['appeals']['type'] = implode(',', $appeals['appeals']['type']);
        $reference['reference']['contact_id'] = $application->id;

        $application->details->update($details);
        $application->appeals->update($appeals);
        $application->references->update($reference);

        return redirect()->route('applications.show', $application)->with('flash_message', 'Application Updated!');
    }

    /**
     * @param Application $application
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Application $application)
    {
        Auth::user()->hasPermissionTo('Delete Application');
        if (isset($application)) {
            $application->delete();
            if(isset($application->details)) {
                $application->details->delete();
            }
            if(isset($application->appeals)) {
                $application->appeals->delete();
            }
            if(isset($application->refences)) {
                $application->references->delete();
            }
            return redirect()->route('applications.index')
                ->with('flash_message',
                    'Application successfully deleted.');
        }

        return redirect()->route('applications.index')
            ->with('flash_message',
                'Cannot delete application. Application not found');
    }

    /**
     * @param Application $application
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createNote(Application $application)
    {
        Auth::user()->hasPermissionTo('Add Application Note');
        $title = 'New Note for  - '. $application->first_name. ' '. $application->last_name;

        return view('admin.applications.notes.create', compact('title', 'application'));
    }

    /**
     * @param Request $request
     * @param Application $application
     * @return $this
     */
    public function storeNote(Request $request, Application $application)
    {
        $this->validate($request, [
            'content'       =>  'required'
        ]);

        $request['created_by'] = Auth::user()->id;

        ApplicationNote::create($request->only('content', 'application_id', 'created_by'));

        return redirect()->route('applications.show', $application)
            ->with('flash_message',
                'Note Successfully Added!');

    }

    /**
     * @param Request $request
     * @param ApplicationNote $applicationNote
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editNote(Request $request, ApplicationNote $applicationNote)
    {
        Auth::user()->hasPermissionTo('Edit Application Note');
        $application = Application::findorFail($request['application']);
        $title = 'Edit Note for  - '. $application->name;

        return view('admin.applications.notes.edit', compact('title', 'application', 'applicationNote'));
    }

    /**
     * @param Request $request
     * @param ApplicationNote $applicationNote
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateNote(Request $request, ApplicationNote $applicationNote)
    {
        $this->validate($request, [
            'content'       =>  'required'
        ]);

        $applicationNote->fill($request->only('content'));
        $applicationNote->save();

        $application = Application::find($applicationNote->user_id);

        return redirect()->route('applications.show', $application)
            ->with('flash_message',
                'Note Updated!');
    }

    /**
     * @param ApplicationNote $applicationNote
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function deleteNote(Request $request, ApplicationNote $applicationNote)
    {
        Auth::user()->hasPermissionTo('Delete Application Note');
        $applicationNote = ApplicationNote::findorFail($applicationNote->id);
        $application = Application::find($request['application']);
        if(isset($applicationNote)){
            $applicationNote->delete();
            return redirect()->route('applications.show', $application)
                ->with('flash_message',
                    'Note deleted');
        }
        return redirect()->route('applications.show', $application)
            ->with('flash_message',
                'Unable to delete Note');
    }

    /**
     * @param JobApplication $jobApplication
     * @return mixed
     */
    public function printPDF(Application $application)
    {

        $pdf = PDF::loadView('admin.pdf.application', compact('application'));
        return $pdf->download('application.pdf');
    }

}
