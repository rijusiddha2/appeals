<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use App\Faq;
use App\Page;
use Illuminate\Support\Facades\Auth;


class FaqController extends Controller
{
    /**
     * FaqController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View FAQ');
        $faqs = Faq::all();
        return view('admin.faqs.index', compact('faqs'));
    }

    /**
     * @param Faq $faq
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Faq $faq)
    {
        Auth::user()->hasPermissionTo('View FAQ');
        $title = 'FAQ - '.$faq->question;
        return view('admin.faqs.view', compact('faq', 'title'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $faqs = Faq::all();

        return Datatables::of($faqs)

            ->editColumn('created_at', function ($faq) {
                return $faq->created_at->format('d/m/y');
            })

            ->addColumn('action', function ($faq) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('faqs.show', $faq->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';

                if(Auth::user()->hasPermissionTo('Edit FAQ')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('faqs.edit', $faq->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Delete FAQ')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('faqs.delete', $faq->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                }
                return $menu;
            })
            ->rawColumns(['answer', 'action'])
            ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add FAQ');
        $categories = Page::pluck('title', 'title')->toArray();
        $categories = [' --- Please Select ---'] + $categories;

        return view('admin.faqs.create', compact('categories'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'question'    =>  'required',
                'answer'      =>  'required',
                'category'    =>  'required'
            ]
        );

        Faq::create($request->except('_token'));

        return redirect()->route('faqs.index')->with('flash_message', 'FAQ Succesfully created!');
    }

    /**
     * @param Faq $faq
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Faq $faq)
    {
        Auth::user()->hasPermissionTo('Add FAQ');
        $title = 'FAQ - '.$faq->question;

        $categories = Page::pluck('title', 'title')->toArray();
        $categories = [' --- Please Select ---'] + $categories;

        return view('admin.faqs.edit', compact('faq', 'title', 'categories'));
    }

    /**
     * @param Request $request
     * @param Faq $faq
     */
    public function update(Request $request, Faq $faq)
    {
        $this->validate($request, [
                'question'  =>  'required',
                'answer'    =>  'required',
                'category'  =>  'required'
            ]
        );

        $faq->fill($request->except('_token'));
        $faq->save();

        return redirect()->route('faqs.show', $faq);
    }

    /**
     * @param Faq $faq
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Faq $faq)
    {
        Auth::user()->hasPermissionTo('Delete FAQ');
        if (isset($faq)) {
            $faq->delete();

            return redirect()->route('faqs.index')
                ->with('flash_message',
                    'FAQ deleted!');
        }

        return redirect()->route('faqs.index')
            ->with('flash_message',
                'Cannot delete FAQ! FAQ not found');
    }
}
