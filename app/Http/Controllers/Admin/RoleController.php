<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;

use Session;

class RoleController extends Controller {

    public function __construct() {

        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Role');
        $roles = Role::all();

        return view('admin.roles.index')->with('roles', $roles);
    }
    
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $roles = Role::all();

        return Datatables::of($roles)
            ->editColumn('created_at', function ($role) {
                return $role->created_at->format('d/m/y');
            })
            ->addColumn('action', function ($role) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="'.route('roles.show', $role->id).'" class="tooltip-pivot float-left"><i class="far fa-eye"></i></a>';

                if(Auth::user()->hasPermissionTo('Edit Role')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="'.route('roles.edit', $role->id).'" class="tooltip-pivot float-left"><i class="fas fa-pencil-alt"></i></a>';
                }

                if(Auth::user()->hasPermissionTo('Delete Role')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('roles.delete', $role->id).'" class="tooltip-pivot float-left"><i class="fas fa-trash"></i></a>';
                }
                return $menu;
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Role');
        $permissions = Permission::all();//Get all permissions

        return view('admin.roles.create', ['permissions'=>$permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Role');
        //Validate name and permissions field

        $this->validate($request, [
                'name'=>'required|unique:roles,deleted_at,NULL',
                'permissions' =>'required',
            ]
        );
        $name = $request['name'];
        $role = new Role();
        $role->name = $name;

        $permissions = $request['permissions'];

        $role->save();
        //Looping thru selected permissions
        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            //Fetch the newly created role and assign permission
            $role = Role::where('name', '=', $name)->first();
            $role->givePermissionTo($p);
        }

        return redirect()->route('roles.show', $role->id)
            ->with('flash_message',
                'Role '. $role->name.' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        Auth::user()->hasPermissionTo('View Role');
        $title = 'Role - ' . $role->name;
        return view('admin.roles.view', compact('role', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Auth::user()->hasPermissionTo('Edit Role');
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        $title = 'Edit - '. $role->name;

        return view('admin.roles.edit', compact('role', 'permissions','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->hasPermissionTo('Edit Role');
        $role = Role::findOrFail($id);//Get role with the given id
        //Validate name and permission fields
        $this->validate($request, [
            'name'=>'required|unique:roles,name,'.$id.', deleted_at,NULL',
            'permissions' =>'required',
        ]);

        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();

        $p_all = Permission::all();//Get all permissions

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
            $role->givePermissionTo($p);  //Assign permission to role
        }

        return redirect()->route('roles.show', $role->id)
            ->with('flash_message',
                'Role '. $role->name.' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        Auth::user()->hasPermissionTo('Delete Role');
        if (isset($role)) {
           $role->delete();

           return redirect()->route('roles.index')
               ->with('flash_message',
                   'Role deleted!');
       }

       return redirect()->route('roles.index')
           ->with('flash_message',
               'Cannot delete Role! Role not found');

    }
}