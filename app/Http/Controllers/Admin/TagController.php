<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Tag;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{

    /**
     * TagController constructor.
     */
    public function __construct()
    {

        $this->middleware('auth');

    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Tag');
        $tags = Tag::all();

        return view('admin.tags.index', compact('tags'));
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {

        $tags = Tag::all();

        return Datatables::of($tags)
            ->editColumn('created_at', function ($tag) {
                return $tag->created_at->format('d/m/y');
            })

            ->editColumn('created_by', function ($tag) {
                return $tag->user->name;
            })

            ->addColumn('action', function ($tag) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('tags.show', $tag->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';

                if(Auth::user()->hasPermissionTo('Edit Tag')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('tags.edit', $tag->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }

                if(Auth::user()->hasPermissionTo('Delete Tag')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('tags.delete', $tag->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                }
                return $menu;
            })

            ->make(true);
    }
    /**
     * @param Tag $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show (Tag $tag)
    {
        Auth::user()->hasPermissionTo('View Tag');

        $title = 'Tag - ' . $tag->name;

        return view('admin.tags.view', compact('tag', 'title'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Tag');

        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Tag');
        $this->validate($request, [
            'name'  =>  'required|max:120|unique:tags,deleted_at,NULL',
            'type'  =>  'required',
        ]);

        $tag = Tag::create($request->only('name', 'slug', 'type'));

        return redirect() ->route('tags.show', $tag)->with('flash_message',
            'Tag Created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        Auth::user()->hasPermissionTo('Edit Tag');
        $title = 'Edit - '. $tag->name;

        return view('admin.tags.edit', compact('tag', 'title'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {

        Auth::user()->hasPermissionTo('Edit Tag');

        $this->validate($request, [
            'name'  =>  'required|max:120',
            'type'  =>  'required',
        ]);

        $input = [
            'name'  =>  $request['name'],
            'slug'  =>  $request['slug'],
            'type'  =>  $request['type'],
        ];

        $tag->fill($input);
        $tag->save();

        return redirect() ->route('tags.show', $tag)->with('flash_message',
            'Tag Updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        Auth::user()->hasPermissionTo('Delete Tag');
        if (isset($tag)) {
            $tag->delete();

            return redirect()->route('tags.index')
                ->with('flash_message',
                    'Tag successfully deleted.');
        }
        return redirect()->route('tags.index')
            ->with('flash_message',
                'Cannot delete Tag. Tag not found');
    }
}
