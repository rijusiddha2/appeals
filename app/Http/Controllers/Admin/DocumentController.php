<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Document;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DocumentController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {

        Auth::user()->hasPermissionTo('Add Document');

        $application = $request['model']::findOrFail($request['application_id']);
        $title = 'Add document for '. $application->name;

        return view('admin.documents.create', compact('title', 'application'));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $original = $request->file('document')->getClientOriginalName();

        $path = $request->file('document')->store('documents');

        $this->validate($request, [
                'document'    =>  'mimes:jpeg,jpg,png,doc,docx,pdf,xls,xlm,xla,xlc,xlt,xlw ',

            ]
        );

        $input = [
            'original_filename' =>  $original,
            'filename'          =>  $path,
            'documentable_id'   =>  $request['documentable_id'],
            'documentable_type' =>  'App\\'. $request['documentable_type'],
            'created_by'        =>  Auth::user()->id
        ];

        $view = strtolower($request['documentable_type']);
        if($request['documentable_type'] == 'JobApplication') {
            $view = 'job-application';
        }

        Document::create($input);

        return redirect()->route($view.'s.index')
            ->with('flash_message',
                'Document Added');
    }

    /**
     * @param Document $document
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Document $document)
    {
        Auth::user()->hasPermissionTo('Delete Document');

        if(isset($document)){
            $document->delete();

            return redirect()->route('dashboard')
                ->with('flash_message',
                    'Document successfully deleted.');
        }

        return redirect()->route('documents.index')
            ->with('flash_message',
                'Document could not be deleted.');
    }

}
