<?php

namespace App\Http\Controllers\Admin;

use App\ImageSize;
use App\ImageSizePivot;
use Intervention\Image\ImageManagerStatic as Resize;
use App\Tag;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use App\Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class ImageController extends Controller
{
    /**
     * ImageController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->photos_path = public_path('images/');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Image');
        $images = Image::all();

        $sizes = ImageSize::pluck('size', 'id')->toArray();
        $sizes = ['' => '--- Please Select ---'] + $sizes;

        $tags = Tag::pluck('name', 'id')->toArray();
        $tags = ['' => '--- Please Select ---'] + $tags;


        return view('admin.images.index', compact('images', 'sizes', 'tags'));
    }

    /**
     * @param $imageId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($imageId)
    {
        Auth::user()->hasPermissionTo('View Image');
        $image = Image::findOrFail($imageId);
        return view('admin.images.view', compact('image'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $images = Image::all();

        return Datatables::of($images)
            ->editColumn('created_at', function ($image) {
                return $image->created_at->format('d/m/y');
            })

            ->editColumn('sizes', function ($image) {
                $sizes = $image->sizes;
                $sizeList = [];
                foreach($sizes as $size)
                {
                    array_push($sizeList, $size->size . ' ('.$size->width.' x '. $size->height.')');
                }
                $sizeList = implode(',', $sizeList);
                return $sizeList;
            })

            ->editColumn('thumb', function ($image) {
                $thumb = '<img src="/images/thumbs/'.$image->name.'" alt="'.$image->alt_tag.'" class="img-responsive"/>';
                return $thumb;
            })

            ->editColumn('tags', function ($image) {
                $tags = $image->tags;
                $tagList = [];
                foreach($tags as $tag)
                {
                    array_push($tagList, $tag->name);
                }
                $tagList = implode(',', $tagList);
                return $tagList;
            })


            ->addColumn('action', function ($image) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('image-uploader.show', $image->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';

                if(Auth::user()->hasPermissionTo('Edit Image')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('image-uploader.edit', $image->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Delete Image')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('image-uploader.delete', $image->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                }
                return $menu;
            })
            ->rawColumns(['thumb', 'action'])
            ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Image');

        $sizes = ImageSize::pluck('size', 'id');

        $tags = Tag::pluck('name','id');

        return view('admin.images.create', compact('sizes', 'tags'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Image');
        $image = $request->file('image');
        $imageResize = Resize::make($image->getRealPath());
        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        $request->validate([
            'image'         => 'required',
            'alt_tag'       =>  'required',
        ]);

        $saveName = strtolower(str_replace(' ', '_', $image->getClientOriginalName()));

        $request['name'] = $saveName;

        $imageRecord = Image::create($request->only('image', 'alt_tag', 'title_tag', 'name'));

        $imageRecord->tags()->attach($request['tags']);

        foreach($request['image-size'] as $size) {
            //store image size in pivot
            $input = [
                'size_id'   => $size,
                'image_id' => $imageRecord->id
            ];

            ImageSizePivot::create($input);
        }


        $thumb = $imageResize->resize(125, 125);
        $thumb->save($this->photos_path.'/thumbs/'.str_replace(' ','_', $image->getClientOriginalName()));

        return redirect()->route('image-uploader.index')->with('flash_message', 'Image added');
    }

    /**
     * @param $imageId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($imageId)
    {
        $image = Image::findOrFail($imageId);

        $sizes = ImageSize::pluck('size', 'id');

        $tags = Tag::pluck('name','id');


        return view('admin.images.edit', compact('sizes', 'tags', 'image'));
    }

    /**
     * @param Request $request
     * @param Image $image
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, Image $image)
    {
        Auth::user()->hasPermissionTo('Edit Image');
        //Validate name, email and password fields
        $this->validate($request, [
            'alt_tag'      =>  'required'
        ]);

        $input = [
            'alt_tag'   =>  $request['alt_tag'],
            'title_tag' =>  $request['title_tag']
        ];

        $image->fill($input);
        $image->save();

        $this->processTags($request['tags']);
        $this->processSizes($request['tags']);

        return redirect()->route('image-uploader.store', $image)->with('flash_message', 'Image Updated!');

    }

    /**
     * @param $tags
     * @param $image
     */
    private function processTags($tags, $image)
    {
        DB::table('taggable')
            ->where('taggable_id', $image->id)
            ->where('taggable_type', 'App/Image')
            ->delete();

        $image->tags()->attach($tags);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function processSizes(Request $request)
    {

        $sizes = ImageSize::whereIn('id',$request['image-size'])->get();

        $image = $request->file('image');

        $imageResize = Resize::make($image->getRealPath());
        $filename    = '/original/'.strtolower(str_replace(' ','_',$image->getClientOriginalName()));
        //store original
        $imageResize->save($this->photos_path.$filename);
        $imageDetails = $request->only('alt_tag', 'title_tag');

        return view('admin.images.partials._tabs', compact('sizes', 'filename', 'imageDetails'));

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function selectImage(Request $request)
    {

        if($request['size'] != 'original') {
            $images = Image::join('images_sizes_pivot', 'images_sizes_pivot.image_id', '=', 'image.id' )
                ->join('image_size', 'image_size.id', '=', 'images_sizes_pivot.image_id')
                ->get();
        } else {
            $images = Image::all();
        }

        return view('admin.images.picture-manager', compact('images'));

    }

    /**
     * @param $image
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addImage()
    {
        Auth::user()->hasPermissionTo('Add Image');
        return view('admin.images.picture-manager');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function saveCrop(Request $request)
    {
        $image_url = $request['imgUrl'];
        // resized sizes
        $imgW = $request['imgW'];
        $imgH = $request['imgH'];
        // offsets
        $imgY1 = $request['imgY1'];
        $imgX1 = $request['imgX1'];
        // crop box
        $cropW = $request['width'];
        $cropH = $request['height'];
        // rotation angle
        $angle = $request['rotation'];
        $size = strtolower(str_replace(' ', '_',$request['size']));
        $filename_array = explode('/', $image_url);
        $filename = $filename_array[sizeof($filename_array)-1];
        $filepath = $this->photos_path.'/original/'.$filename;
        Log::info('Try to manipulate photo : '.$filepath.' for user : '.Auth::user()->name);
        $manager = new Resize();
        $image = $manager->make( $filepath );
        Log::info('Fiinsh manager->make(..) for : '.$filepath);
        $image->resize($imgW, $imgH)
            ->rotate(-$angle)
            ->crop($cropW, $cropH, $imgX1, $imgY1);
        $image->save($this->photos_path.'/'.$size.'/'.str_replace(' ','', $filename));
        if( !$image) {
            return Response::json([
                'status' => 'error',
                'message' => 'Server error while cropping',
            ], 200);
        }

        return Response::json([
            'status' => 'success',
            'url' => '/images/'.$size.'/'.$filename
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        Auth::user()->hasPermissionTo('Delete Image');
        if (isset($image)) {
            $image->delete();

            return redirect()->route('image-uploader.index')
                ->with('flash_message',
                    'Image successfully deleted.');
        }
        return redirect()->route('uploader.index')
            ->with('flash_message',
                'Cannot delete Image. Image not found');
    }
}
