<?php

namespace App\Http\Controllers\Admin;

use App\ImageSize;
use App\ImageSizePivot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ImageSizeController extends Controller
{
    /**
     * ImageSizeController constructor.
     */
    public function __construct()
    {

        $this->middleware('auth');

    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Image Size');
        $imageSizes = ImageSize::all();

        return view('admin.image-sizes.index', compact('imageSizes'));
    }

    /**
     * @param ImageSize $imageSize
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show (ImageSize $imageSize)
    {
        Auth::user()->hasPermissionTo('View Image Size');
        $title = 'Image Size - ' . $imageSize->name;

        return view('admin.image-sizes.view', compact('imageSize', 'title'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Image Size');
        return view('admin.image-sizes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Image Size');
        $this->validate($request, [
            'size'  =>  'required|max:120|unique:image_sizes',
            'width'  =>  'required|integer',
            'height'  =>  'required|integer',
        ]);
        $imageSize = ImageSize::create($request->only('size', 'width', 'height'));

        //create folder
        $path = public_path().'/images/'.strtolower(str_replace(' ','_', $imageSize->size));
        File::makeDirectory($path, $mode = 0777, true, true);

        return redirect()->route('image-sizes.store', $imageSize)->with('flash_message',
            'Image Size Created!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImageSize $imageSize)
    {
        Auth::user()->hasPermissionTo('Delete Image Size');
        if (isset($imageSize)) {
            $size = strtolower(str_replace(' ', '_',$imageSize->size));
            $sizeId = $imageSize->id;

            $imageSize->delete();

            //tidies up image folder by deleting all images with this size and tidies up pivot table.
            $this->deleteImages($size, $sizeId);

            return redirect()->route('image-sizes.index')
                ->with('flash_message',
                    'Image Size deleted!');
        }

        return redirect()->route('image-sizes.index')
            ->with('flash_message',
                'Cannot delete Image Size! Image Size not found');

    }

    /**
     * @param $size
     * @param $sizeId
     */
    private function deleteImages($size, $sizeId)
    {
        foreach (glob("/images/".$size."/*.{jpg,png,gif}", GLOB_BRACE) as $filename) {
            if (strpos($filename, $size) !== false) {
                unlink($filename);
            }
        }
        $path = public_path().'/images/'.strtolower(str_replace(' ','_', $size));

        rmdir($path);

        $this->deletePivot($sizeId);
    }

    /**
     * @param $sizeId
     */
    private function deletePivot($sizeId)
    {
        $imageSizes = ImageSizePivot::where('size_id', $sizeId)->get();

        foreach($imageSizes as $imageSize){
            $imageSize->delete();
        }
    }
}
