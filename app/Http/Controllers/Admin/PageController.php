<?php

namespace App\Http\Controllers\Admin;

use App\Grapejs;
use App\Slideshow;
use App\PageTemplate;
use Illuminate\Http\Request;
use App\Faq;
use App\Page;
use App\PageContent;
use App\Image;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use App\Template;
use App\Content;
use Illuminate\Support\Facades\Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;
use App\Http\Controllers\Controller;


class PageController extends Controller
{
    /**
     * PageController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Page');
        $pages = Page::orderBy('title')->get();

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $query = Page::select('*');
        if($request->get('status')) {
            $query->where('status', $request->get('status'));
        }

        $pages = $query->orderBy('title')->get();


        return Datatables::of($pages)
            ->editColumn('published_date', function ($page) {
                return $page->created_at->format('d/m/y');
            })

            ->addColumn('action', function ($page) {
                $content = PageContent::where('page_id',$page->id)->exists();

                if($content) {
                    $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('pages.show', $page->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';
                    if(Auth::user()->hasPermissionTo('Edit Page')) {
                        $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('pages.edit', $page->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                    }
                    if(Auth::user()->hasPermissionTo('Edit Page Content')) {
                        $menu .= '<a data-toggle="tooltip" data-original-title="Edit Content" href="' .route('pages.edit-content', $page->id).'" class="tooltip-pivot pull-left"><i class="fa fa-columns"></i></a>';
                    }
                    if(Auth::user()->hasPermissionTo('Delete Page')){
                        $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('pages.delete', $page->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                    }
                    return $menu;

                } else {
                    $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('pages.show', $page->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';
                    if(Auth::user()->hasPermissionTo('Edit Page')) {
                        $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('pages.edit', $page->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                    }
                    if(Auth::user()->hasPermissionTo('Add Page Content')) {
                        $menu .= '<a data-toggle="tooltip" data-original-title="Add Content" href="' .route('pages.add-content', $page->id).'" class="tooltip-pivot pull-left"><i class="fa fa-columns"></i></a>';
                    }
                    if(Auth::user()->hasPermissionTo('Delete Page')) {
                        $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('pages.delete', $page->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                    }
                    return $menu;
                }

            })

            ->make(true);
    }

    /**
     * @param Pages $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show (Page $page)
    {
        Auth::user()->hasPermissionTo('View Page');
        $title = 'Page - ' . $page->name;

        $content = PageContent::where('page_id', $page->id)->exists();

        return view('admin.pages.view', compact('page', 'title', 'content'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Page');
        $templates = Template::pluck('name', 'id');

        return view('admin.pages.create', compact('templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Page');
        $this->validate($request, [
        'title'                 =>  'required|max:65',
            'url'               =>  'required|unique:pages',
            'status'            =>  'required',
            'meta_description'  => 'required|max:160',
            'og_description'    => 'required|max:160',
            'og_title'          => 'required|max:65',
            'og_image'          => 'required',
            'og_url'            => 'required',
        ]);

        $input = [
            'title'             =>   $request['title'],
            'url'               =>   $request['url'],
            'published_date'    =>   Carbon::createFromFormat('d-m-Y',$request['published_date']),
            'status'            =>   $request['status'],
            'template_id'       =>   $request['template_id']
        ];


        $page = Page::create($input);
        $meta_input = [
            'meta_description'  => $request['meta_description'],
            'og_description'    => $request['og_description'],
            'og_title'          => $request['og_title'],
            'og_image'          => $request['og_image'],
            'og_url'            => $request['og_url']
        ];

        $page->meta()->create($meta_input);

        return redirect()->route('pages.store', $page)->with('flash_message',
            'Page Created!');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        Auth::user()->hasPermissionTo('Edit Page');
        $title = 'Edit - '. $page->title;

        $templates = Template::pluck('name', 'id');

        return view('admin.pages.edit', compact('page', 'title', 'templates'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        Auth::user()->hasPermissionTo('Edit Page');
        $this->validate($request, [
            'title'             =>  'required|max:65',
            'url'               =>  'required|unique:pages,url,'.$page->id.'',
            'status'            =>  'required',
            'meta_description'  => 'required|max:160',
            'og_description'    => 'required|max:160',
            'og_title'          => 'required|max:65',
            'og_image'          => 'required',
            'og_url'            => 'required',
        ]);

        $input = [
            'title'             =>   $request['title'],
            'url'               =>   $request['url'],
            'status'            =>   $request['status'],
            'template_id'       =>   $request['template_id']
        ];

        $page->fill($input);
        $page->save();

        $meta_input = [
            'meta_description'  => $request['meta_description'],
            'og_description'    => $request['og_description'],
            'og_title'          => $request['og_title'],
            'og_image'          => $request['og_image'],
            'og_url'            => $request['og_url']
        ];
        if(isset($page->meta)) {
            $page->meta->update;
        } else {
            $page->meta()->create($meta_input);
        }

        return redirect()->route('pages.show', $page)->with('flash_message',
            'Page Updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        Auth::user()->hasPermissionTo('Delete Page');
        if (isset($page)) {
            $page->delete();

            return redirect()->route('pages.index')
                ->with('flash_message',
                    'Page successfully deleted.');
        }

        return redirect()->route('pages.index')
            ->with('flash_message',
                'Cannot delete Page. Page not found');

    }

    /**
     * @param Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addContent(Page $page)
    {
        Auth::user()->hasPermissionTo('Add Page Content');

        $contentBlocks = Content::join('content_template', 'content_template.content_id', '=', 'contents.id')
            ->where('content_template.template_id', $page->template->id)
            ->orderBy('order')
            ->select('contents.*', 'content_template.template_id',  'content_template.template_id as content_id' )
            ->get();

        //This will need changing
        $images = Image::all();

        $faqs = Faq::pluck('category', 'category')->groupBy('category')->toArray();

        $slideshows = Slideshow::pluck('name', 'id')->toArray();
        $slideshows = ['-- Please Select ---'] + $slideshows;

        return view('admin.pages.add-content', compact('page', 'images', 'contentBlocks', 'slideshows', 'faqs'));
    }

    /**
     * @param Page $page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeContent(Page $page, Request $request)
    {
        Auth::user()->hasPermissionTo('Add Page Content');

        $input['content_id'] = $request['content_id'];
        $input['page_id'] = $page->id;
        $input['template_id'] = $page->template->id;
        $input['order']   = $request['order'];

        $pageContent = PageContent::create($input);

        foreach($request['block'] as $block) {

            $contentInput = [
                'block_id'          => isset($block['block_id']) ? $block['block_id'] : '',
                'text'              => isset($block['text']) ? $block['text'] : '',
                'image'             => isset($block['image']) ? $block['image'] : '',
                'title'             => isset($block['title']) ? $block['title'] : '',
                'caption'           => isset($block['caption']) ? $block['caption'] : '',
                'slider'            => isset($block['slider']) ? $block['slider'] : '',
                'link'              => isset($block['link']) ? $block['link'] : '',
                'link_text'         => isset($block['link_text']) ? $block['link_text'] : '',
                'parallax'          => isset($block['parallax']) ? $block['parallax'] : '',
                'faq'               => isset($block['faq']) ? $block['faq'] : '',
                'column_width'      => isset($block['column_width']) ? $block['column_width'] : '',
                'page_content_id'   => $pageContent->id

            ];

            PageTemplate::create($contentInput);
        }

        Session::flash('success', 'Content added successfully!');
        return view('admin.pages.partials._flash-messages');

    }

    /**
     * @param Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editContent(Page $page)
    {
        Auth::user()->hasPermissionTo('Edit Page');

        $contentBlocks = PageContent::where('page_id', $page->id)
            ->orderBy('order')
            ->get();

        //This will need changing
        $images = Image::all();

        $slideshows = Slideshow::pluck('name', 'id')->toArray();
        $slideshows = ['-- Please Select ---'] + $slideshows;

        $faqs = Faq::pluck('category', 'category')->toArray();

        return view('admin.pages.edit-content', compact('page', 'images', 'contentBlocks', 'slideshows', 'faqs'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateContent(Request $request)
    {
        foreach($request['block'] as $key => $block) {

            $templateId = $block['page_template'];
            $pageTemplate = PageTemplate::findOrFail($templateId);

            $contentInput = [
                'block_id'          => isset($block['block_id']) ? $block['block_id'] : '',
                'text'              => isset($block['text']) ? $block['text'] : '',
                'image'             => isset($block['image']) ? $block['image'] : NULL,
                'title'             => isset($block['title']) ? $block['title'] : '',
                'caption'           => isset($block['caption']) ? $block['caption'] : '',
                'slider'            => isset($block['slider']) ? $block['slider'] : NULL,
                'link'              => isset($block['link']) ? $block['link'] : '',
                'link_text'         => isset($block['link_text']) ? $block['link_text'] : '',
                'parallax'          => isset($block['parallax']) ? $block['parallax'] : NULL,
                'faq'               => isset($block['faq']) ? $block['faq'] : '',
                'column_width'      => isset($block['column_width']) ? $block['column_width'] : '',

            ];
            $pageTemplate->fill($contentInput);
            $pageTemplate->save();
        }

        Session::flash('success', 'Content amended successfully!');
        return view('admin.pages.partials._flash-messages');
    }
}
