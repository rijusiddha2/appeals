<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Auth;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;

use Session;

class PermissionController extends Controller {

    public function __construct() {
        $this->middleware('auth'); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        Auth::user()->hasPermissionTo('View Permission');
        $permissions = Permission::all(); //Get all permissions

        return view('admin.permissions.index')->with('permissions', $permissions);
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $permissions = Permission::all();

        return Datatables::of($permissions)
            ->editColumn('created_at', function ($permission) {
                return $permission->created_at->format('d/m/y');
            })
            ->addColumn('action', function ($permission) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="'.route('permissions.show', $permission->id).'" class="tooltip-pivot float-left"><i class="far fa-eye"></i></a>';
                if(Auth::user()->hasPermissionTo('Edit Permission')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="'.route('permissions.edit', $permission->id).'" class="tooltip-pivot float-left"><i class="fas fa-pencil-alt"></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Delete Permission')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('permissions.delete', $permission->id).'" class="tooltip-pivot float-left"><i class="fas fa-trash"></i></a>';
                }
                return $menu;
            })
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Permission');

        $roles = Role::get(); //Get all roles

        return view('admin.permissions.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Auth::user()->hasPermissionTo('Add Permission');

        $this->validate($request, [
            'name'=>'required|max:40',
        ]);

        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;

        $roles = $request['roles'];

        $permission->save();

        if (!empty($request['roles'])) { //If one or more role is selected
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                $permission = Permission::where('name', '=', $name)->first(); //Match input //permission to db record
                $r->givePermissionTo($permission);
            }
        }

        return redirect()->route('permissions.index')
            ->with('flash_message',
                'Permission '. $permission->name.' added!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        Auth::user()->hasPermissionTo('View Permission');
        $title = 'Role - ' . $permission->name;
        return view('admin.permissions.view', compact('permission', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Auth::user()->hasPermissionTo('Edit Permission');
        $permission = Permission::findOrFail($id);

        $title = 'Edit Permission - '. $permission->name;

        return view('admin.permissions.edit', compact('permission', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->hasPermissionTo('Edit Permission');
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();

        return redirect()->route('permissions.show', $permission->id)
            ->with('flash_message',
                'Permission '. $permission->name.' updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->hasPermissionTo('Delete Permission');
        $permission = Permission::findOrFail($id);

        //Make it impossible to delete this specific permission
        if ($permission->name == "Administer roles & permissions") {
            return redirect()->route('admin.permissions.index')
                ->with('flash_message',
                    'Cannot delete this Permission!');
        }

        $permission->delete();

        return redirect()->route('permissions.index')
            ->with('flash_message',
                'Permission deleted!');

    }
}
