<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use App\JobApplication;
use App\JobApplicationNote;
use Carbon\Carbon;
use App\Document;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;


class JobApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        Auth::user()->hasPermissionTo('View Job Application');


        if(isset($request['job_id'])) {
            $applications = JobApplication::where('job_id', $request['job_id'])->get();
            $job = $request['job_id'];
        } else {
            $applications = JobApplication::all();
        }

        return view('admin.jobs.applications.index', compact('applications', 'job'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {

        if($request->get('job_id')) {
            $applications = JobApplication::where('job_id', $request['job_id'])->get();
        } else {
            $applications = JobApplication::all();
        }

        return Datatables::of($applications)

            ->editColumn('job', function($application) {
                return $application->job->title;
            })
            ->editColumn('name', function($application) {
                return $application->title. ' '. $application->first_name. ' '. $application->last_name;
            })

            ->addColumn('action', function ($application) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('job-applications.show', $application->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';

                if(Auth::user()->hasPermissionTo('Edit Job Application')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('job-applications.edit', $application->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Delete Job Application')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('job-applications.delete', $application->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                }
                return $menu;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(JobApplication $jobApplication)
    {
        Auth::user()->hasPermissionTo('View Job Application');

        return view('admin.jobs.applications.view', compact('jobApplication'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Application $application
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createNote(JobApplication $jobApplication)
    {
        Auth::user()->hasPermissionTo('Add Job Application Note');
        $title = 'New Note for  - '. $jobApplication->first_name. ' '. $jobApplication->last_name;

        $application = $jobApplication;

        return view('admin.jobs.applications.notes.create', compact('title', 'application'));
    }

    /**
     * @param Request $request
     * @param Application $application
     * @return $this
     */
    public function storeNote(Request $request, JobApplication $jobApplication)
    {
        $this->validate($request, [
            'content'       =>  'required'
        ]);

        $request['created_by'] = Auth::user()->id;

        JobApplicationNote::create($request->only('content', 'job_application_id', 'created_by'));

        $application = $jobApplication;

        return redirect()->route('job-applications.show', $application)
            ->with('flash_message',
                'Note Successfully Added!');

    }

    /**
     * @param Request $request
     * @param ApplicationNote $applicationNote
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editNote(Request $request, JobApplicationNote $jobApplicationNote)
    {
        Auth::user()->hasPermissionTo('Edit Job Application Note');
        $jobApplication = JobApplication::findorFail($request['application']);
        $title = 'Edit Note for  - '. $jobApplication->name;

        return view('admin.jobs.applications.notes.edit', compact('title', 'jobApplication', 'jobApplicationNote'));
    }

    /**
     * @param Request $request
     * @param ApplicationNote $applicationNote
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateNote(Request $request, JobApplicationNote $jobApplicationNote)
    {
        $this->validate($request, [
            'content'       =>  'required'
        ]);

        $jobApplicationNote->fill($request->only('content'));
        $jobApplicationNote->save();

        $jobApplication = JobApplication::find($jobApplicationNote->job_application_id);

        return redirect()->route('job-applications.show', $jobApplication)
            ->with('flash_message',
                'Note Updated!');
    }

    /**
     * @param ApplicationNote $applicationNote
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function deleteNote(Request $request, JobApplicationNote $jobApplicationNote)
    {
        Auth::user()->hasPermissionTo('Delete Job Application Note');

        $application = JobApplication::find($request['jobApplication']);
        if(isset($jobApplicationNote)){
            $jobApplicationNote->delete();
            return redirect()->route('job-applications.show', $application)
                ->with('flash_message',
                    'Note deleted');
        }
        return redirect()->route('job-applications.show', $application)
            ->with('flash_message',
                'Unable to delete Note');
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function printPDF(JobApplication $jobApplication)
    {

        $pdf = PDF::loadView('admin.pdf.job-application', compact('jobApplication'));
        return $pdf->download('job-application.pdf');
    }

}
