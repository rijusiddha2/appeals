<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use Carbon\Carbon;
use App\Document;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    /**
     *  JobController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Job');
        $jobs = Job::all();

        return view('admin.jobs.index', compact('jobs'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $jobs = Job::all();

        return Datatables::of($jobs)

            ->editColumn('closing_date', function($job){
                if(!empty($job->closing_date)){
                    return $job->closing_date->format('d/m/Y');
                }
                return 'No end date set';
            })

            ->addColumn('action', function ($job) {

                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('jobs-admin.show', $job->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';
                if(Auth::user()->hasPermissionTo('View Job Application')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Applications" href="' .route('job-applications.index', ['job_id' => $job->id]).'" class="tooltip-pivot pull-left"><i class="fas fa-bars"></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Edit Job')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('jobs-admin.edit', $job->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Delete Job')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('jobs-admin.delete', $job->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                }
                return $menu;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Job');

        return view('admin.jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Job');

        if(!empty($request['closing_date'])) {
            $request['closing_date'] = Carbon::createFromFormat('d-m-Y',$request['closing_date']);
        }

        $this->validate($request, [
                'title'         =>  'required',
                'caption'       =>  'required',
                'job_document'  =>  'mimes:pdf',

            ]
        );

        $job = Job::create($request->only('title', 'caption', 'description', 'closing_date'));

        //store document
        $document = $request->file('job_document')->getClientOriginalName();
        $path = $request->file('job_document')->store('public/documents');
        $docInput = [
            'original_filename' =>  $document,
            'filename'          =>  $path,
            'documentable_id'   =>  $job->id,
            'documentable_type' =>  'App\\Job',
            'created_by'        =>  Auth::user()->id
        ];

        Document::create($docInput);

        return redirect()->route('jobs-admin.show', $job)->with('flash_message',
            'Job Created!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $job = Job::findOrFail($id);
        Auth::user()->hasPermissionTo('View Job');
        return view('admin.jobs.view', compact('job'));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        Auth::user()->hasPermissionTo('Edit Job');

        $job = Job::findOrFail($id);
        return view('admin.jobs.edit', compact('job'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->hasPermissionTo('Edit Block');

        $job = Job::findOrFail($id);

        $request['closing_date'] = Carbon::createFromFormat('d/m/Y',$request['closing_date']);

        $this->validate($request, [
            'title'         =>  'required',
            'caption'       =>  'required',
            'closing_date'  =>  'required|date|after:tomorrow'

        ]);

        $input = $request->except('_token');

        $job->fill($input);
        $job->save();

        return redirect()->route('jobs-admin.show', $job)->with('flash_message', 'Job Updated!');
    }

    /**
     * @param $id
    * @return \Illuminate\Http\RedirectResponse
    * @throws \Exception
    */
    public function destroy($id)
    {
        Auth::user()->hasPermissionTo('Delete Job');
        $job = Job::findOrFail($id);
        if (isset($job)) {
            $job->delete();

            return redirect()->route('jobs-admin.index')
                ->with('flash_message',
                    'Job successfully deleted.');
        }

        return redirect()->route('jobs-admin.index')
            ->with('flash_message',
                'Cannot delete Job. Job not found');
    }
}
