<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Appeal;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class AppealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Appeal');
        $appeals = Appeal::orderBy('created_at', 'desc')->get();

        return view('admin.appeals.index', compact('appeals'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $appeals = Appeal::all();

        return Datatables::of($appeals)

            ->editColumn('total_raised', function ($appeal) {
                return '£'. $appeal->total_raised;
            })
            ->addColumn('action', function ($appeal) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('appeals.show', $appeal->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';

                if(Auth::user()->hasPermissionTo('Edit Appeal')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('appeals.edit', $appeal->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }

                return $menu;
            })
            ->rawColumns(['answer', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Appeal');

        $appeal = [];
        return view('admin.appeals.create', compact('appeal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Appeal');
        //Validate name, email and password fields
        $this->validate($request, [
            'title' => 'required',
            'total_raised' => 'required',
            'supporters' => 'required',
            'intro' => 'required',
        ]);

        $input = $request->except('_token');

        $appeal = Appeal::create($input);

        return redirect()->route('appeals.show', $appeal)->with('flash_message', 'Appeal Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Appeal $appeal)
    {
        Auth::user()->hasPermissionTo('View Appeal');

        $image = $appeal->getImage($appeal->image_id);
        return view('admin.appeals.view', compact('appeal', 'image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Appeal $appeal)
    {
        Auth::user()->hasPermissionTo('Edit Appeal');
        $image = $appeal->getImage($appeal->image_id);
        return view('admin.appeals.edit', compact('appeal', 'image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Appeal $appeal, Request $request)
    {
        Auth::user()->hasPermissionTo('Edit Appeal');
        //Validate name, email and password fields
        $this->validate($request, [
            'title' => 'required',
            'total_raised' => 'required',
            'supporters' => 'required',
            'intro' => 'required',
        ]);

        $input = $request->except('_token');

        $appeal->fill($input);
        $appeal->save();

        return redirect()->route('appeals.show', $appeal)->with('flash_message', 'Appeal Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
