<?php

namespace App\Http\Controllers\Admin;

use Harimayco\Menu\Facades\Menu;
use Illuminate\Http\Request;
use Session;
use App\Page;
use Harimayco\Menu\Models\Menus;
use Harimayco\Menu\Models\MenuItems;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        Auth::user()->hasPermissionTo('View Menu');
        $pages = Page::pluck('title', 'id');
        if (isset($request['menu'])) {
            $indmenu = Menus::find($request['menu']);
            $menus = MenuItems::where('menu',$request['menu'])->orderBy('sort')->get();
        }

        $menuList = Menus::pluck('name', 'id')->prepend('-- Select Menu --', '');
        return view('admin.menus.index', compact('menuList', 'indmenu', 'menus', 'pages'));
    }

    /**
     * @param Request $request
     */
    public function addPage(Request $request)
    {
        Auth::user()->hasPermissionTo('Edit Menu');
        $page = Page::find($request['labelmenu']);

        $menuitem = new MenuItems();
        $menuitem->label = $page->title;
        $menuitem->link = $page->url;
        $menuitem->menu = request()->input("idmenu");
        $menuitem->sort = MenuItems::getNextSortRoot(request()->input("idmenu"));
        $menuitem->save();
    }
}
