<?php

namespace App\Http\Controllers\Admin;

use Yajra\DataTables\DataTables;
use App\Contact;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $contacts = Contact::all();
        return view('admin.contacts.index', compact('contacts'));
    }

    /**
     * @param Contact $contact
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Contact $contact)
    {
        Auth::user()->hasPermissionTo('View Email');
        return view('admin.contacts.view', compact('contact'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $contacts = Contact::all();

        return Datatables::of($contacts)
            ->editColumn('created_at', function ($contact) {
                return $contact->created_at->format('d/m/y');
            })

            ->editColumn('name', function ($contact) {
                return $contact->first_name. ' '. $contact->last_name;
            })

            ->addColumn('action', function ($contact) {
                return '              
                <a data-toggle="tooltip" data-original-title="View" href="' . route('contacts.show', $contact->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';
            })
            ->make(true);
    }
}
