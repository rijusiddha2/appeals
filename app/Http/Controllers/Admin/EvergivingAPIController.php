<?php

namespace App\Http\Controllers\Admin;

use App\EvergivingAPI;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
Use Illuminate\Support\Facades\Http;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class EvergivingAPIController extends Controller
{
    /**
     *  ApplicationController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Evergiving API');
        $api = EvergivingAPI::orderBy('created_at', 'desc')->get();

        return view('admin.evergiving.index', compact('api'));
    }

/**
 *
* @return \Illuminate\Http\JsonResponse
*/
    public function data(Request $request)
    {
        $apis = EvergivingAPI::all();
        return Datatables::of($apis)
            ->editColumn('api_key',function($api){
                return Str::limit($api->api_key, 10);
            })
            ->addColumn('action', function ($api) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('api.show', $api->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';

                if(Auth::user()->hasPermissionTo('Edit Evergiving API')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('api.edit', $api->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Delete Evergiving API')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('api.delete', $api->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                }

                return $menu;
            })
            ->rawColumns(['answer', 'action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Evergiving API');

        return view('admin.evergiving.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Evergiving API');
        //Validate name, email and password fields
        $this->validate($request, [
            'api_key'       =>  'required',
            'campaign_id'   =>  'required',
            'type'          =>  'required',
            'schema_id'     =>  'required'
        ]);

        $input = $request->except('_token');

        $api = EvergivingAPI::create($input);

        return redirect()->route('admin.api.show', $api)->with('flash_message', 'API Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(EvergivingAPI $api)
    {
        Auth::user()->hasPermissionTo('View Evergiving API');

        $apiKey = Str::limit($api->api_key, 10);

        return view('admin.evergiving.view', compact('api', 'apiKey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EvergivingAPI $api)
    {
        Auth::user()->hasPermissionTo('Edit Evergiving API');

        return view('admin.evergiving.edit', compact('api'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EvergivingAPI $api, Request $request)
    {
        Auth::user()->hasPermissionTo('Edit Evergiving API');
        //Validate name, email and password fields
        $this->validate($request, [
            'api_key'       =>  'required',
            'campaign_id'   =>  'required',
            'type'          =>  'required',
            'schema_id'     =>  'required'
        ]);

        $input = $request->except('_token');

        $api->fill($input);
        $api->save();

        return redirect()->route('api.show', $api)->with('flash_message', 'API Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EvergivingAPI $evergivingAPI)
    {
        Auth::user()->hasPermissionTo('Delete Evergiving API');
        if (isset($evergivingAPI)) {
            $evergivingAPI->delete();

            return redirect()->route('api.index')
                ->with('flash_message',
                    'API successfully deleted.');
        }
        return redirect()->route('api.index')
            ->with('flash_message',
                'Cannot delete API. API not found');
    }

    public function test()
    {
        $apis = EvergivingAPI::all();

        foreach($apis as $api) {
            $response = Http::withToken($api->api_key)
                ->withHeaders(['Accept' => 'application/json', 'Content-Type' => 'application/json'])->get('https://api.evergiving.com/api/v4/pledges/next_batch?campaign_id='.$api->campaign_id.'&schema_id='.$api->schema_id.'');

            if($response->successful()){
                $response->json();
            }



        }


        dd(' Not succesful' . $response->json($response->body()));

    }
    public static function postAPI($api, $data)
    {
        $jsonData = json_encode($data);

        $url = "https://thankqapikarunatrust.accessacloud.com/API/WebsiteAPI/v1/".$api."/";

        $sendHeaders = [
            "Content-Type: application/json",
            "X-apikey: ".config('api.thankq.api-key')."",
        ];

        $curlOpts = [
            CURLOPT_TIMEOUT        		=> 60,
            CURLOPT_DNS_CACHE_TIMEOUT	=> 120,
            CURLOPT_HEADER         		=> 0,
            CURLOPT_VERBOSE        		=> 0,
            CURLOPT_NOPROGRESS     		=> 1, // Switch to false to turn on progress
            CURLOPT_RETURNTRANSFER 		=> 1,
            CURLOPT_HTTPHEADER     		=> $sendHeaders,
            CURLOPT_POSTFIELDS     		=> $jsonData,
            CURLOPT_POST           		=> 1,

        ];

        $ch = curl_init($url);

        curl_setopt_array($ch, $curlOpts);

        $res = curl_exec($ch);

        $resObject = json_decode($res);

        return $resObject;
    }
    private function prepareDonorData($data)
    {
        $address = $data->address1;
        if(isset($data->address2)){
            $address .= ', '.$data->address2;
        }
        $this->donorDefault['title']            =   $data->title;
        $this->donorDefault['firstName']        =   $data->first_name;
        $this->donorDefault['keyname']          =   $data->keyname;
        $this->donorDefault['emailAddress']     =   $data->email;
        $this->donorDefault['eveningTelephone'] =   $data->tel_eve;
        $this->donorDefault['AddressLine1']     =   $address;
        $this->donorDefault['AddressLine3']     =   $data->town;
        $this->donorDefault['AddressLine4']     =   $data->county;
        $this->donorDefault['country']          =   ucwords($data->country);
        $this->donorDefault['postcode']         =   $data->postcode;

        return $this->donorDefault;
    }
    private function prepareDonationData($data, $donor, $payment)
    {
        $frequency = 'Monthly';
        if(isset($data['single_method'])) {
            $frequency = 'Single';
        }
        $this->donationDefault['serialNumber']          =   $donor->serialNumber;
        $this->donationDefault['receiptSerialNumber']   =   $donor->serialNumber;
        $this->donationDefault['instalmentValue']       =   $data['upgrade_amount'];
        $this->donationDefault['startDate']             =   Carbon::now()->format('d/m/Y');
        $this->donationDefault['paymentDay']            =   $data['payment_day'];
        $this->donationDefault['taxClaimable']          =   $data['gift_aid'] == 1 ? 'yes' : 'no';
        $this->donationDefault['paymentType']          =   $payment;
        $this->donationDefault['paymentFrequency']          =   $frequency;
        return $this->donationDefault;
    }
    private function prepareDDDonationData($contact, $data, $donor)
    {

        $this->donationDDDefault['serialNumber']          =   $donor->serialNumber;
        $this->donationDDDefault['receiptSerialNumber']   =   $donor->serialNumber;
        $this->donationDDDefault['instalmentValue']       =   $data['upgrade_amount'];
        $this->donationDDDefault['accountName']           =   $contact['account_name'];
        $this->donationDDDefault['accountNumber']         =   $contact['account_no'];
        $this->donationDDDefault['sortCode']              =   $contact['sort_code'];
        $this->donationDDDefault['bankName']              =   $contact['bank_name'];
        $this->donationDDDefault['bankAddress']           =   $contact['bank_address'];
        $this->donationDDDefault['bankPostcode']          =   $contact['bank_postcode'];
        $this->donationDDDefault['startDate']             =   Carbon::now()->format('d/m/Y');
        $this->donationDDDefault['accountVerified']       =   Carbon::now()->format('d/m/Y');
        $this->donationDDDefault['accountVerifiedBy']     =   'loqate';
        $this->donationDDDefault["DDIStartDate"]          =   Carbon::now()->format('d/m/Y');
        $this->donationDDDefault["DDIDateReceived"]       =   Carbon::now()->format('d/m/Y');
        $this->donationDDDefault['paymentDay']            =   $contact['payment_day'];
        $this->donationDDDefault['taxClaimable']          =   $contact['gift_aid'] == 1 ? 'yes' : 'no';
        $this->donationDDDefault['paymentType']          =   $data['method'];

        return $this->donationDDDefault;
    }
}
