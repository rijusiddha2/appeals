<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Block;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;

class BlockController extends Controller
{
    /**
     * @var array
     */
    protected $types = [
            ''      =>  '-- Please Select --',
            'text'  =>  'image',
            'image' =>  'text',
            'video' =>  'video',
            'feeds' =>  'feeds'

        ];
    /**
     *  BlockController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Block');
        $blocks = Block::all();

        return view('admin.blocks.index', compact('blocks'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $blocks = Block::all();

        return Datatables::of($blocks)

            ->addColumn('action', function ($block) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('blocks.show', $block->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';

                if(Auth::user()->hasPermissionTo('Edit Block')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Edit" href="' .route('blocks.edit', $block->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }
                if(Auth::user()->hasPermissionTo('Delete Block')) {
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('blocks.delete', $block->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                }
                return $menu;
            })
            ->rawColumns(['answer', 'action'])
            ->make(true);
    }

    /**
     * @param Block $block
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Block $block)
    {
        Auth::user()->hasPermissionTo('View Block');
        return view('admin.blocks.view', compact('block'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Block');

        $types = $this->types;

        return view('admin.blocks.create', compact('types'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        Auth::user()->hasPermissionTo('Add Block');
        //Validate name, email and password fields
        $this->validate($request, [
            'name' => 'required',
            'partial' => 'required',
            'type' => 'required'
        ]);

        $input = $request->except('_token');

        $block = Block::create($input);

        return redirect()->route('blocks.show', $block)->with('flash_message',
            'Block Created!');
    }

    /**
     * @param Block $block
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Block $block)
    {
        Auth::user()->hasPermissionTo('Edit Block');

        $types = $this->types;

        return view('admin.blocks.edit', compact('block', 'types'));

    }

    /**
     * @param Request $request
     * @param Block $block
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, Block $block)
    {
        Auth::user()->hasPermissionTo('Edit Block');

        $this->validate($request, [
            'name'      =>  'required',
            'partial'   =>  'required',
            'type'      =>  'required'
        ]);

        $input = $request->except('_token');

        $block->fill($input);
        $block->save();

        return redirect()->route('blocks.show', $block)->with('flash_message', 'Block Updated!');
    }

    /**
     * @param Block $block
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Block $block)
    {
        Auth::user()->hasPermissionTo('Delete Block');
        if (isset($block)) {
            $block->delete();

            return redirect()->route('blocks.index')
                ->with('flash_message',
                    'Block successfully deleted.');
        }

        return redirect()->route('blocks.index')
            ->with('flash_message',
                'Cannot delete Block. Block not found');
    }
}
