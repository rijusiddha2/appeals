<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use App\Slide;
use App\Slideshow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;

class SlideshowController extends Controller
{
    /**
     * SlideshowController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->effects = [
            ''  => '--- Please Select ---',
            'Horizontal'  => 'Horizontal',
            'Vertical'  => 'Vertical',
            'Fade'  => 'Fade'
        ];
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Auth::user()->hasPermissionTo('View Slideshow');
        $slideshows = Slideshow::all();
        return view('admin.slideshows.index', compact('slideshows'));
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $slideshows = Slideshow::all();
        return Datatables::of($slideshows)
            ->editColumn('slides', function ($slideshow) {

                if(isset($slideshow->slides)) {
                    return count($slideshow->slides);
                }

                return '0';
            })

            ->editColumn('created_by', function ($slideshow) {
                return $slideshow->user->name;
            })

            ->addColumn('action', function ($slideshow) {
                $menu = '<a data-toggle="tooltip" data-original-title="View" href="' . route('slideshows.show', $slideshow->id) . '" class="tooltip-pivot pull-left"><i class="fa fa-eye"></i></a>';
                if(Auth::user()->hasPermissionTo('Edit Slideshow')){
                    $menu .= ' <a data-toggle="tooltip" data-original-title="Edit" href="' .route('slideshows.edit', $slideshow->id).'" class="tooltip-pivot pull-left"><i class="fas fa-pencil-alt "></i></a>';
                }

                if(Auth::user()->hasPermissionTo('Delete Slideshow')){
                    $menu .= '<a data-toggle="tooltip" data-original-title="Delete" href="'.route('slideshows.delete', $slideshow->id).'" class="tooltip-pivot pull-left"><i class="fa fa-times"></i></a>';
                }
                return $menu;
            })

            ->make(true);
    }

    /**
     * @param Slideshow $slideshow
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Slideshow $slideshow)
    {
        Auth::user()->hasPermissionTo('View Slideshow');
        return view('admin.slideshows.view', compact('slideshow'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Auth::user()->hasPermissionTo('Add Slideshow');
        $effects = $this->effects;
        return view('admin.slideshows.create', compact('effects'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'name'      =>  'required|max:120',
            'effect'    =>  'required',
        ]);

        $slideshow = Slideshow::create($request->only('name', 'controls', 'pagination', 'auto', 'effect', 'parallax'));

        return redirect()->route('slideshows.show', $slideshow)->with('flash_message',
            'Slideshow Created!');
    }

    /**
     * @param Slideshow $slideshow
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addSlides(Slideshow $slideshow)
    {
        Auth::user()->hasPermissionTo('Edit Slideshow');

        $images = Image::join('images_sizes_pivot', 'images_sizes_pivot.image_id', '=', 'images.id')
            ->join('image_sizes', 'image_sizes.id', '=', 'images_sizes_pivot.size_id')
            ->where('image_sizes.size', 'Banner')
            ->select('images.*', 'image_sizes.size')
            ->get();

        return view('admin.slideshows.add-slide', compact('slideshow', 'images'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeSlides(Request $request)
    {
        $this->validate($request, [
            'slideshow_id'  =>  'required',
            'image_id'      =>  'required',
        ]);

        Slide::create($request->except('_token'));

        $slideshow = Slideshow::findOrFail($request['slideshow_id']);

        return redirect()->route('slideshows.show', $slideshow);
    }

    /**
     * @param Slideshow $slideshow
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Slideshow $slideshow)
    {
        Auth::user()->hasPermissionTo('Edit Slideshow');
        $title = 'Edit - '. $slideshow->name;

        $effects = $this->effects;

        return view('admin.slideshows.edit', compact('slideshow', 'title', 'effects'));
    }

    public function update(Request $request, Slideshow $slideshow)
    {
        Auth::user()->hasPermissionTo('Edit Slideshow');

        $this->validate($request, [
            'name'      =>  'required|max:120',
            'effect'    =>  'required',
        ]);

        $input = [
            'name'          =>  $request['name'],
            'controls'      =>  $request['controls'],
            'pagination'    =>  $request['pagination'],
            'auto'          =>  $request['auto'],
            'effect'        =>  $request['effect'],
            'parallax'      =>  $request['parallax']

        ];

        $slideshow->fill($input);
        $slideshow->save();

        return redirect() ->route('slideshows.show', $slideshow)->with('flash_message',
            'Slideshow Updated!');

    }
    /**
     * @param Slideshow $slideshow
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Slideshow $slideshow)
    {
        Auth::user()->hasPermissionTo('Delete Slideshow');
        if (isset($slideshow)) {
            $slideshow->delete();

            return redirect()->route('slideshows.index')
                ->with('flash_message',
                    'Slideshow successfully deleted.');
        }

        return redirect()->route('slideshows.index')
            ->with('flash_message',
                'Cannot delete Slideshow. Slideshow not found');
    }

    /**
     * @param Slide $slide
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteSlide(Slide $slide)
     {
         Auth::user()->hasPermissionTo('Edit Slideshow');
         if (isset($slide)) {
             $slide->delete();

             return redirect()->back()
                 ->with('flash_message',
                     'Slide successfully deleted.');
         }

         return redirect()->back()
             ->with('flash_message',
                 'Cannot delete Slide. Slide not found');
     }
    /**
     * @param Request $request
     * @return false|string
     */
    public function sortSlides(Request $request)
    {
        $itemID = $request->itemID;
        $itemIndex = $request->itemIndex;

        Slide::where('id', $itemID)->update(array('order' => $itemIndex));
        return json_encode('success');
    }
}
