<?php

namespace App\Http\Controllers\Front;


use App\Job;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Harimayco\Menu\Facades\Menu;
use Carbon\Carbon;
use App\JobApplication;
use App\JobApplicationCurrentHistory;
use App\JobApplicationDetail;
use App\JobApplicationPreviousHistory;
use App\JobApplicationQualification;
use App\JobApplicationSafeguarding;
use App\JobApplicationReferee;
use App\Mail\JobApplicationComplete;


use Illuminate\Support\Facades\Mail;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::getByName('main_menu');

        $query = Job::where(function ($query) {
            $query->where('closing_date', '>', Carbon::now())
                ->orWhereNull('closing_date');
        });

        $page = Page::where('title', 'Jobs')->first();

        $jobs = $query->orderBy('closing_date', 'asc')->get();
        return view('front.static.jobs', compact('menus', 'jobs', 'page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //get application form
        $jobs = Job::where(function ($query) {
            $query->where('closing_date', '>', Carbon::now())
                ->orWhereNull('closing_date');
            })
            ->pluck('title', 'id')->toArray();
        $jobs = ['' => '--- Please Select ---'] + $jobs;
        $menus = Menu::getByName('main_menu');

        return view('front.static.jobs-application', compact('jobs', 'menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Process application form

        $this->validate($request, [
            'job_id'                                        => 'required',
            'first_name'                                    => 'required',
            'last_name'                                     => 'required',
            'address'                                       => 'required',
            'postcode'                                      => 'required',
            'email'                                         => 'required|email',
            'mobile'                                        => 'required',
            'driving_licence'                               => 'required',
            'work_permit'                                   => 'required',
            'history_current.job_title'                     => 'required',
            'history_current.employer_details'              => 'required',
            'history_current.employed_from'                 => 'required|date',
            'history_current.employed_to'                   => 'required|date',
            'history_current.responsibilities'              => 'required',
            'history_previous.employed_from'                => 'date|date_format: d-m-y',
            'history_previous.employed_to'                  => 'date|date_format: d-m-y',
            'qualifications.professional_qualifications'    => 'required',
            'qualifications.educational_qualifications'     => 'required',
            'further_details.skills'                        => 'required',
            'safeguarding.vunerable_sanctions'              => 'required',
            'safeguarding.criminal_offences'                => 'required',
            'safeguarding.safeguarding_details'             => 'required_if:safeguarding[vunerable_sanctions],1|required_if:safeguarding[criminal_offences],1',
            'reference.name_1'                              => 'required',
            'reference.relationship_1'                      => 'required',
            'reference.telephone_1'                         => 'required',
            'reference.email_1'                             => 'required|email',
            'reference.name_2'                              => 'required',
            'reference.relationship_2'                      => 'required',
            'reference.telephone_2'                         => 'required',
            'reference.email_2'                             => 'required|email'
        ]);

        $application = $request->except('history_current','history_previous', 'qualifications', 'further_details','safeguarding', 'reference');
        $historyCurrent = $request->only('history_current');
        $historyPrevious = $request->only('history_previous');
        $qualifications = $request->only('qualifications');
        $furtherDetails = $request->only('further_details');
        $safeguarding = $request->only('safeguarding');
        $reference = $request->only('reference');


        $application = JobApplication::create($application);

        $historyCurrent['history_current']['job_application_id'] = $application->id;
        $historyCurrent['history_current']['employed_from'] = Carbon::createFromFormat('d-m-Y', $historyCurrent['history_current']['employed_from']);
        $historyCurrent['history_current']['employed_to'] = Carbon::createFromFormat('d-m-Y', $historyCurrent['history_current']['employed_to']);
        $historyPrevious['history_previous']['job_application_id'] = $application->id;
        $historyPrevious['history_previous']['employed_from'] = Carbon::createFromFormat('d-m-Y', $historyPrevious['history_previous']['employed_from']);
        $historyPrevious['history_previous']['employed_to'] = Carbon::createFromFormat('d-m-Y', $historyPrevious['history_previous']['employed_to']);
        $qualifications['qualifications']['job_application_id'] = $application->id;
        $furtherDetails['further_details']['job_application_id'] = $application->id;
        $safeguarding['safeguarding']['job_application_id'] = $application->id;
        $reference['reference']['job_application_id'] = $application->id;
        JobApplicationCurrentHistory::create($historyCurrent['history_current']);
        JobApplicationPreviousHistory::create($historyPrevious['history_previous']);
        JobApplicationQualification::create($qualifications['qualifications']);
        JobApplicationDetail::create($furtherDetails['further_details']);
        JobApplicationSafeguarding::create($safeguarding['safeguarding']);
        JobApplicationReferee::create($reference['reference']);

        Mail::to('rijusiddha@karuna.org')->send(new JobApplicationComplete($application));

        $menus = Menu::getByName('main_menu');

        return view('front.static.thank-you', compact('menus'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
