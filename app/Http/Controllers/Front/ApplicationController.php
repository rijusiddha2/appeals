<?php

namespace App\Http\Controllers\Front;

use App\ApplicationDetail;
use App\ApplicationReferee;
use App\Mail\ApplicationComplete;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Application;
use App\ApplicationAppeal;
use Harimayco\Menu\Facades\Menu;

use Illuminate\Support\Facades\Mail;


class ApplicationController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $menus = Menu::getByName('main_menu');
        return view('front.static.application', compact('menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function process(Request $request)
    {

        $this->validate($request, [
            'first_name'                    => 'required',
            'last_name'                     => 'required',
            'address'                       => 'required',
            'postcode'                      => 'required',
            'email'                         => 'required|email',
            'mobile'                        => 'required',
            'details.work_experience'       => 'required',
            'details.triratna_history'      => 'required',
            'appeals.type'                  => 'required',
            'appeals.season'                => 'required',
            'appeals.draw'                  => 'required',
            'appeals.skills'                => 'required',
            'appeals.difficulties'          => 'required',
            'details.health'                => 'required',
            'details.health_details'        => 'required_if:details[health], poor',
            'details.medication'            => 'required',
            'details.medication_details'    => 'required_if:details[medication],1',
            'details.safeguarding_offence'  => 'required',
            'details.criminal_offence'      => 'required',
            'details.offence_details'       => 'required_if:details[safeguarding_offence],1|required_if:details[criminal_offence],1',
            'reference.name_1'              => 'required',
            'reference.relationship_1'      => 'required',
            'reference.telephone_1'         => 'required',
            'reference.email_1'             => 'required|email',
            'reference.name_2'              => 'required',
            'reference.relationship_2'      => 'required',
            'reference.telephone_2'         => 'required',
            'reference.email_2'             => 'required|email',
            'details.support_package'      => 'required',
            'details.ethos'                 => 'required',
            'details.ethos_details'         => 'required_if:details[ethos],0',
            'details.privacy'               => 'required',
            'details.future_opportunities'  => 'required',
        ]);

        $application = $request->except('details','reference', 'appeals');
        $details = $request->only('details');
        $appeals = $request->only('appeals');
        $reference = $request->only('reference');


        $application = Application::create($application);

        $details['details']['application_id'] = $application->id;
        $details['details']['contact_source'] = implode(',', $details['details']['contact_source']);
        $appeals['appeals']['application_id'] = $application->id;
        $appeals['appeals']['type'] = implode(',', $appeals['appeals']['type']);
        $reference['reference']['application_id'] = $application->id;
        ApplicationDetail::create($details['details']);
        ApplicationAppeal::create($appeals['appeals']);
        ApplicationReferee::create($reference['reference']);

        Mail::to('rijusiddha@karuna.org')->send(new ApplicationComplete($application));

        $menus = Menu::getByName('main_menu');

        return view('front.static.thank-you', compact('menus'));
    }
}
