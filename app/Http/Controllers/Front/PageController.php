<?php

namespace App\Http\Controllers\Front;

use App\Appeal;
use App\PageContent;
use App\PageTemplate;
use App\Slideshow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use Harimayco\Menu\Facades\Menu;
use Harimayco\Menu\Models\Menus;

class PageController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $page = Page::where('url', $request->getRequestUri())->firstOrFail();

        $contentBlocks = PageContent::where('page_id', $page->id)
            ->where('hide', 1)
            ->orderBy('order')->get();

        $menus = Menu::getByName('main_menu');
        $submenus = Menu::getByName('anchor');

        $results = Appeal::latest()->take(4)->get();
        $resultsGrid = Appeal::latest()->take(12)->get();

        return view('front.pages.index', compact('page', 'menus', 'contentBlocks', 'submenus', 'results', 'resultsGrid'));
    }
}
