<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use Harimayco\Menu\Facades\Menu;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $menus = Menu::getByName('main_menu');
        return view('front.static.contact', compact('menus'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function process(Request $request)
    {
        $this->validate($request, [
            'first_name'                    => 'required',
            'last_name'                     => 'required',
            'email'                         => 'required:email',
            'subject'                       => 'required',
            'message'                       => 'required'
            ]
        );

        if(!empty($request['dob']) ) {
            return redirect()->back();
        }

        $request['ip_address'] = $request->ip();

        $contact = Contact::create($request->except('_token'));

        Mail::to('rijusiddha@karuna.org')->send(new ContactMail($contact));


        return Redirect::back()->with('flash_message', 'You message was successfully sent. One of the team will contact you shortly.');
    }
}
