<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationDetail extends Model
{
    protected $table = 'job_further_details';

    protected $fillable = [
        'job_application_id',
        'skills',
        'disabilities',
        'employed_from',
        'employed_to',
        'responsibilities'
    ];
}
