<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationReferee extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'application_id',
        'name_1',
        'relationship_1',
        'telephone_1',
        'email_1',
        'name_2',
        'relationship_2',
        'telephone_2',
        'email_2'
    ];
}
