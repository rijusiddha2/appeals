<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockTemplate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'template_id',
        'block_id'
    ];

    /**
     * @var string
     */
    protected $table = 'block_template';
}
