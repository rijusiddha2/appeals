<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class EvergivingContact extends Model
{
    /**
     * @param $api
     * @param null $data
     * @return mixed
     */
    public static function contactAPI($key, $campaign, $schema, $revision)
    {
//        //This is not currently used
//        $token = self::authAPI();

        $request = Http::withHeaders([
            'Content-Type' => 'application/json'


        ])->get('https://api.evergiving.com/v4/contact/next_batch?campaign_id=' . $campaign . '&schema_id=' . $schema . '&after_version='. $revision.'');
        return $request;
    }
}
