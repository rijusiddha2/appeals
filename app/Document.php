<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'original_filename', 'filename', 'documentable_id', 'documentable_type', 'created_by'
    ];

    /**
     * Get all of the owning documentableable models.
     */
    public function documentable()
    {
        return $this->morphTo();
    }
}
