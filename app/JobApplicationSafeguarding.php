<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationSafeguarding extends Model
{

    protected $table = 'job_safeguarding';

    /**
     * @var array
     */
    protected $fillable = [
        'job_application_id',
        'vunerable_sanctions',
        'criminal_offences',
        'safeguarding_details'
    ];
}
