<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'first_name',
        'last_name',
        'order_name',
        'gender',
        'address',
        'postcode',
        'email',
        'mobile',
        'alt_tel'
    ];

    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (Application $item) {
            if(Auth::user()) {
                $item->created_by = Auth::user()->id;
            } else {
                $item->created_by = 0;
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function details()
    {
        return $this->hasOne('App\ApplicationDetail');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function appeals()
    {
        return $this->hasOne('App\ApplicationAppeal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function references()
    {
        return $this->hasOne('App\ApplicationReferee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\ApplicationNote');
    }

    /**
     * Get all of the user's documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

}
