<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageMeta extends Model
{
    protected $table = 'page_meta';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'meta_description',
        'og_description',
        'og_title',
        'og_image',
        'og_url'
    ];

    public function meta()
    {
        return $this->belongsTo('\App\Page');
    }
}
