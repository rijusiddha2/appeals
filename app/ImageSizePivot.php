<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ImageSizePivot extends Model
{


    /**
     * @var string
     */
    protected $table = 'images_sizes_pivot';

    /**
     * @var array
     */
    protected $fillable = [
        'size_id',
        'image_id',
    ];

    /**
     *
     */
    public static function boot() {
        parent::boot();
        //while creating/inserting item into db
        static::creating(function (ImageSizePivot $item) {
            $item->created_by = Auth::user()->id;
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('\App\User', 'created_by');
    }
}
