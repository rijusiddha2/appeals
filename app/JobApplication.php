<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class JobApplication extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'job_id',
        'title',
        'other_title',
        'first_name',
        'last_name',
        'order_name',
        'address',
        'postcode',
        'mobile',
        'alt_tel',
        'email',
        'work_permit',
        'driving_licence'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'closing_date'
    ];

    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (JobApplication $item) {

            $item->created_by = Auth::user()->id;
        });

    }

    public function job()
    {
        return $this->belongsTo('\App\Job', 'job_id', 'id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function currentHistory()
    {
        return $this->hasOne('App\JobApplicationCurrentHistory');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function previousHistory()
    {
        return $this->hasOne('App\JobApplicationPreviousHistory');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function qualifications()
    {
        return $this->hasOne('App\JobApplicationQualification');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function further()
    {
        return $this->hasOne('App\JobApplicationDetail');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function safeguarding()
    {
        return $this->hasOne('App\JobApplicationSafeguarding');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function references()
    {
        return $this->hasOne('App\JobApplicationReferee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Get all of the user's documents.
     */
    public function documents()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\JobApplicationNote');
    }
}
