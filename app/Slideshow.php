<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Slideshow extends Model
{
   use SoftDeletes;

    protected $fillable = [
        'name',
        'controls',
        'pagination',
        'auto',
        'effect',
        'parallax',
    ];
    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (Slideshow $item) {

            $item->created_by = Auth::user()->id;
        });

    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('\App\User', 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function slides()
    {
        return $this->hasMany('\App\Slide')->orderBy('order');
    }
}
