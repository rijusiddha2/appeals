<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobApplicationNote extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'job_application_id',
        'content',
        'created_by',
    ];

    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (JobApplicationNote $item) {
            if(Auth::user()) {
                $item->created_by = Auth::user()->id;
            } else {
                $item->created_by = 0;
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
}
