<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appeal extends Model
{
    protected $fillable = [
        'title',
        'total_raised',
        'supporters',
        'intro',
        'image_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image(){
        return $this->belongsTo('\App\Image', 'image_id');
    }

    /**
     * @param null $image
     * @return null
     */
    public function getImage($image = null)
    {

        $image = Image::findOrFail($image);

        return $image;
    }
}
