<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvergivingAPI extends Model
{
    protected $fillable = [
        'api_key',
        'campaign_id',
        'schema_id',
        'type',
        'revision_id'
    ];
}
