<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Slide extends Model
{
    protected $fillable = [
        'image_id',
        'slideshow_id',
        'title',
        'caption',
        'link',
        'link_text',
    ];
    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (Slide $item) {

            $item->created_by = Auth::user()->id;
        });

    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function images(){
        return $this->belongsTo('\App\Image', 'image_id');
    }
}
