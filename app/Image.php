<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\EventListener\FragmentListener;

class Image extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
        'name',
        'alt_tag',
        'title_tag'
    ];

    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (Image $item) {

            $item->created_by = Auth::user()->id;
        });

    }

    /**
     * Get all of the images tags.
     */
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sizes()
    {
        return $this->belongsToMany('App\ImageSize', 'images_sizes_pivot', 'image_id', 'size_id');
    }

    /**
     * @param $columnWidth
     * @return Image[]|\Illuminate\Database\Eloquent\Collection|string
     */
    public static function getImageSizes($columnWidth)
    {
        switch ($columnWidth) {
            case $columnWidth == 12:
                $images = self::getImages(['Full Image']);
                break;
            case $columnWidth >=6 && $columnWidth <12;
                $images = self::getImages(['Large Landscape']);
                break;
            case $columnWidth >=3 && $columnWidth <6:
                $images = self::getImages(['Medium Landscape', 'Medium Square']);
                break;
            case $columnWidth <3:
                $images = self::getImages(['Small Landscape']);
                break;
            default:
                $images = "";
        }

        return($images);
    }

    /**
     * @param $size
     * @return Image[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getImages($size)
    {
        if($size) {
            $images = Image::join('images_sizes_pivot', 'images_sizes_pivot.image_id', '=', 'images.id')
                ->join('image_sizes', 'image_sizes.id', '=', 'images_sizes_pivot.size_id')
                ->where('image_sizes.size', $size)
                ->get();

            return $images;
        }

        $images = Image::all();

        return $images;
    }

    /**
     * @param $columnWidth
     * @param $imageName
     * @return string
     */
    public static function getImageSize($columnWidth, $imageName)
    {
        switch ($columnWidth) {
            case $columnWidth == 12:
                $image = 'full_image';
                break;
            case $columnWidth >=5 && $columnWidth <12;
                $image ='large_landscape';
                break;
            case $columnWidth >3 && $columnWidth <5:
                if (file_exists( public_path() . '/images/medium_landscape/'.$imageName . '')) {
                    $image = 'medium_landscape';
                } else {
                    $image = 'medium_square';
                }

                break;
            case $columnWidth <=3:
                $image = 'small_landscape';
                break;
            default:
                $image = "";
        }

        return $image;
    }
}
