<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class ImageSize extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'image_sizes';

    /**
     * @var array
     */
    protected $fillable = [
        'size',
        'width',
        'height'
    ];
    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (ImageSize $item) {

            $item->created_by = Auth::user()->id;
        });

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('\App\User', 'created_by');
    }
}
