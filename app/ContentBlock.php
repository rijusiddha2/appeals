<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentBlock extends Model
{
    /**
     * @var string
     */
    protected $table = 'block_content';
}
