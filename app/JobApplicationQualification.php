<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationQualification extends Model
{

    protected $table = 'job_qualifications';

    protected $fillable = [
        'job_application_id',
        'professional_qualifications',
        'educational_qualifications'
    ];
}
