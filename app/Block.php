<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Block extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'partial',
        'type'
    ];

    /**
     *
     */
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db
        static::creating(function (Block $item) {

            $item->created_by = Auth::user()->id;
       });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contents(){
        return $this->belongsToMany('\App\Content');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contentBlock(){
        return $this->hasOne('\App\ContentBlock');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pageTemplates(){
        return $this->hasMany('\App\PageTemplate');
    }
}
