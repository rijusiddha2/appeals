function addpagesmenu() {
    $("#spincustomu").show();

    $.ajax({
        data : {
            labelmenu : $("#pages-item-name").val(),
            idmenu : $("#idmenu").val()
        },

        url : addpagemenur,
        type : 'POST',
        success : function(response) {

            window.location = "";

        },
        complete: function(){
            $("#spincustomu").hide();
        }

    });
}
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
//
//
//
// window.Vue = require('vue');
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */
//
// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

window.$ = window.jQuery = require('jquery');
require('jquery-ui/ui/widgets/sortable.js');
require('bootstrap-sass');
require('bootstrap-select');
require('bootstrap-datepicker');
require('bxslider/dist/jquery.bxslider.min.js');
require('@fortawesome/fontawesome-free');
require('croppic');
require('bootstrap-sass/assets/javascripts/bootstrap/tooltip');

$(document).ready(function(){
    var grapesjs = require('grapesjs/dist/grapes');
    $('[data-toggle="tooltip"]').tooltip();
    if ($('.selectpicker').length) {
        $('.selectpicker').selectpicker();
    }
    if ($('.datepicker').length) {
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    }
    $('.top-menu a.clickable').click(function () {
        $(this).siblings('ul.sub-menu').slideToggle('1000');
        $('.nav-link').children().toggleClass('fa-caret-down fa-caret-up');

    })
    if ($('.bxslider').length) {
        $('.bxslider').bxSlider({
            mode: 'fade',
            speed: 1000,
            pager: false,
            wrapperClass: 'slider',
            prevText: '',
            nextText: ''
        });
    }
    $(".anchor").click(function(e) {
        e.preventDefault();
        var dest = $(this).attr('href');
        console.log(dest);
        $('html,body').animate({ scrollTop: $(dest).offset().top }, 'slow');
    });
    $('.navbar-toggle').on('click', function() {
        $('#sidebar-nav').css('width', '250px');
    });

    $('#sidebar-nav').on('click', function(){
        $('#sidebar-nav').css('width', '0');
    })

    $(window).scroll(function(){
        if($(this).scrollTop() > 100){
            $('.navbar-collapse').hide();
            $('#scroll-nav').fadeIn();
        } else {
            $('.navbar-collapse').fadeIn();
            $('#scroll-nav').hide();
        }

    })

    $('.top-link').click(function(){
        $('html, body').animate({scrollTop:0}, 'slow');
    })




})

