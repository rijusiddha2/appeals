<?php

namespace app\controllers;

use app\models\UploadPreferencesForm;
use Yii;
use app\models\User;
use app\models\UserSearch;
use app\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'import-mailchimp'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete', 'import-mailchimp'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return ( ! Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = "/admin";

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = "/admin";

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new User();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->createMailchimp($model);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $email = md5($model->email);
            $existingTags = Yii::$app->mailchimp->get('/lists/ce80c7b680/members/'.$email.'/tags/');
            if($existingTags == 404 && ($model->weekly_email == 1 || $model->yoga_email == 1 || $model->phone_list == 1 || $model->other_email == 1)) {
                $this->createMailchimp($model);
            } else {
                $this->updateMailchimp($model);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImportMailchimp ()
    {

        $model = new UploadPreferencesForm();

        if (Yii::$app->request->isPost) {
            $importOkay = false;

            $model->attachedFile = UploadedFile::getInstance($model, 'attachedFile');

            if ($model->validate()) {

                $report = Yii::$app->nbcfileuploader->handleUpload($model->attachedFile);

                if ( $report['success'] ) {
                    $importOkay = true;
                }

            } else {
                foreach($model->errors as $line){
                    $report['errors'][] = VarDumper::dumpAsString($line);
                }
            }

            //var_dump($report);die;
            // var_dump($model->attachedFile->tempName);die;
            $handle = fopen($model->attachedFile->tempName, "r");

            while ($data = fgetcsv($handle, 1000, ",")) {

                $email = $data[0];

                $record = User::findOne(array('email' => $email));
                if ($record) {
                    $record->other_email = 1;
                    $record->save();
                } else {
                    $model = new User();
                    $model->email = $email;
                    $model->first_name = $data[1];
                    $model->last_name = $data[2];
                    $model->order_name = $data[3];
                    $model->mobile = $data[7];
                    $model->landline = $data[6];
                    $model->weekly_email = 1;
                    $model->mailing_only = 1;
                    $model->save();
                }

            }

            var_dump('finished'); die;
        } else {
            //var_dump('no post');die;
            $model = new UploadPreferencesForm();

            return $this->render('import',
                [
                    'model' => $model
                ]);
        }

//        $handle = fopen($_POST['file'], "r");
//        $uploadedFile = CUploadedFile::getInstance('file');
//        $newFilePath = "tmp/{$uploadedFile->name}";
//        $uploadSuccess = $uploadedFile->saveAs($newFilePath);
//        if (!$uploadSuccess) {
//            throw new CHttpException('Error uploading file.');
//        }
    }

    /**
     * @param $model
     */
    private function createMailchimp($model)
    {
        if($model->weekly_email == 1 || $model->yoga_email == 1 || $model->phone_list == 1 || $model->other_email == 1){
            $test = Yii::$app->mailchimp->post("/lists/ce80c7b680/members/", [
                "email_address" => $model->email,
                "status" => "subscribed",
                "merge_fields" => [
                    "FNAME" => $model->first_name,
                    "LNAME" => $model->last_name,
                    "MMERGE3" => $model->order_name,

                ],
            ]);
        }
        if($model->weekly_email == 1) {
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/27981/members/',[
                "email_address" => $model->email

            ]);
        }

        if ($model->yoga_email == 1){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/27985/members/',[
                "email_address" => $model->email

            ]);
        }

        if ($model->phone_list == 1){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/27977/members/',[
                "email_address" => $model->email

            ]);
        }

        if ($model->other_email == 1){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/27973/members/',[
                "email_address" => $model->email

            ]);
        }

    }
    private function updateMailchimp($model)
    {
        $email = md5($model->email);
        $existingTags = Yii::$app->mailchimp->get('/lists/ce80c7b680/members/'.$email.'/tags/');
        if($existingTags->total_items == 0 && ($model->weekly_email == 1 || $model->yoga_email == 1 || $model->phone_list == 1 || $model->other_email == 1)) {
            $this->createMailchimp($model);
        }
        foreach($existingTags->tags as $tags) {
            if($tags->name == 'weekly') {
                $weekly = true;
            }
            if($tags->name == 'Other') {
                $other = true;
            }
            if($tags->name == 'Phone') {
                $phone = true;
            }
            if($tags->name == 'yoga') {
                $yoga = true;
            }
            if($tags->name == 'male') {
                $male = true;
            }
            if($tags->name == 'female') {
                $female = true;
            }
            if($tags->name == 'Order Member') {
                $orderMember = true;
            }
            if($tags->name == 'GFR') {
                $gfr = true;
            }
            if($tags->name == 'Mitra') {
                $mitra = true;
            }
        }


        if($model->weekly_email == 1) {
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/27981/members/',[
                "email_address" => $model->email

            ]);
        } elseif(isset($weekly)){
            Yii::$app->mailchimp->delete('/lists/ce80c7b680/segments/27981/members/'.$email.'');
        }


        if ($model->yoga_email == 1){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/27985/members/',[
                "email_address" => $model->email

            ]);
        } elseif (isset($yoga)){
            Yii::$app->mailchimp->delete('/lists/ce80c7b680/segments/27985/members/'.$email.'');
        }


        if ($model->phone_list == 1){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/27977/members/',[
                "email_address" => $model->email

            ]);
        } elseif(isset($phone)){
            Yii::$app->mailchimp->delete('/lists/ce80c7b680/segments/27977/members/'.$email.'');
        }

        if ($model->other_email == 1){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/27973/members/',[
                "email_address" => $model->email

            ]);
        } elseif(isset($other)){
            Yii::$app->mailchimp->delete('/lists/ce80c7b680/segments/27973/members/'.$email.'');
        }

        if ($model->sex == 'male'){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29073/members/',[
                "email_address" => $model->email

            ]);
        } elseif(isset($male)){
            Yii::$app->mailchimp->delete('/lists/ce80c7b680/segments/29073/members/'.$email.'');
        }
        if ($model->sex == 'female'){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29077/members/',[
                "email_address" => $model->email

            ]);
        } elseif(isset($female)){
            Yii::$app->mailchimp->delete('/lists/ce80c7b680/segments/29077/members/'.$email.'');
        }
        if ($model->triratna_status == 'mitra'){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29089/members/',[
                "email_address" => $model->email

            ]);
        } elseif(isset($mitra)){
            Yii::$app->mailchimp->delete('/lists/ce80c7b680/segments/29089/members/'.$email.'');
        }
        if ($model->triratna_status == 'GFR'){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29085/members/',[
                "email_address" => $model->email

            ]);
        } elseif(isset($gfr)){
            Yii::$app->mailchimp->delete('/lists/ce80c7b680/segments/29085/members/'.$email.'');
        }
        if ($model->triratna_status == 'order member'){
            Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29081/members/',[
                "email_address" => $model->email

            ]);
        } elseif(isset($orderMember)){
            Yii::$app->mailchimp->delete('/lists/ce80c7b680/segments/29081/members/'.$email.'');
        }

    }

    public function actionSynchMailchimp()
    {
        $users = User::find()->all();

        foreach($users as $user){

            $email = md5($user->email);

            $mailchimp =  Yii::$app->mailchimp->get('/lists/ce80c7b680/members/'.$email.'');

            if (isset($mailchimp->email_address) && $mailchimp != '404' && !empty($mailchimp->email_address) && $mailchimp->status != 'unsubscribed') {


                if ($user->sex == 'male') {

                    Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29073/members/', [
                        "email_address" => $mailchimp->email_address

                    ]);

                }
                if ($user->sex == 'female') {
                    Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29077/members/', [
                        "email_address" => $mailchimp->email_address

                    ]);
                }
                if ($user->triratna_status == 'mitra') {
                    Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29089/members/', [
                        "email_address" => $mailchimp->email_address

                    ]);
                }
                if ($user->triratna_status == 'GFR') {
                    Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29085/members/', [
                        "email_address" => $mailchimp->email_address

                    ]);
                }
                if ($user->triratna_status == 'order member') {
                    Yii::$app->mailchimp->post('/lists/ce80c7b680/segments/29081/members/', [
                        "email_address" => $mailchimp->email_address

                    ]);
                }
            }
        }
    }
}
