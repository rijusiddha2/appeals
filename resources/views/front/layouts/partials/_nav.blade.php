<div class="row">
    <div class="container-fluid">
        <div class="masthead">
            <nav>
                <div id="app-navbar">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="slide-collapse" data-target="#sidebar-nav" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse" >
                        <div class="row">
                            <div class="col-md-4">
                                <ul class="nav">
                                    @foreach($menus as $menu)
                                        <li {{ $menu['child'] ? 'class="dropdown"' : '' }} {{ Request::is('/') && $menu['link'] == '/' ? 'class="hidden"' : '' }}>
                                            <a {{ $menu['child'] ? 'class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"' : '' }} class="{{ $menu['class'] }}" href="{{ url(
                                        $menu['link'])
                                        }}">{{
                                        $menu['label']
                                        }}</a>
                                            @if($menu['child'])
                                                <ul class="dropdown-menu">
                                                    @foreach($menu['child'] as $subMenu)
                                                        <li>
                                                            <a class="{{ $subMenu['class'] }}href="{{ route('page', $subMenu['link'])}}">{{ $subMenu['label'] }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('home') }}"><img src=" {{ asset('images/logo.png') }}" class="img-responsive logo"/></a>
                            </div>
                            <div class="col-md-4">
                                <ul class="nav contact-buttons">
                                    <li>
                                        <a href="{{ route('contact') }}" >Contact Us</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('application') }}" class="btn btn-rounded">Apply Now</a>
                                    </li>

                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Collapsed Hamburger -->

                <div class="" id="sidebar-nav">
                    <i class="fas fa-times pull-right"></i>
                    <img src=" {{ asset('images/logo.png') }}" class="img-responsive"/>
                    <ul class="nav">
                        @foreach($menus as $menu)
                            <li {{ $menu['child'] ? 'class="dropdown"' : '' }}>
                                <a {{ $menu['child'] ? 'class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"' : '' }} href="{{ route('page', $menu['link']) }}">{{ $menu['label'] }}</a>
                                @if($menu['child'])
                                    <ul class="dropdown-menu">
                                        @foreach($menu['child'] as $subMenu)
                                            <li>
                                                <a href="{{ route('page', $subMenu['link'])}}">{{ $subMenu['label'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                            <li>
                                <a href="{{ route('contact') }}" >Contact Us</a>
                            </li>
                            <li>
                                <a href="{{ route('application') }}" class="btn btn-rounded">Apply Now</a>
                            </li>
                    </ul>
                </div>
                <div class="" id="scroll-nav">
                    <div class="collapse navbar-collapse" >
                        <div class="row">
                            <div class="col-md-4">
                                <ul class="nav">
                                    @foreach($menus as $menu)
                                        <li {{ $menu['child'] ? 'class="dropdown"' : '' }} {{ Request::is('/') && $menu['link'] == '/' ? 'class="hidden"' : '' }}>
                                            <a {{ $menu['child'] ? 'class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"' : '' }} class="{{ $menu['class'] }}" href="{{ url(
                                        $menu['link'])
                                        }}">{{
                                        $menu['label']
                                        }}</a>
                                            @if($menu['child'])
                                                <ul class="dropdown-menu">
                                                    @foreach($menu['child'] as $subMenu)
                                                        <li>
                                                            <a class="{{ $subMenu['class'] }}href="{{ route('page', $subMenu['link'])}}">{{ $subMenu['label'] }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('home') }}"><img src=" {{ asset('images/logo-orange.png') }}" class="img-responsive logo"/></a>

                            </div>
                            <div class="col-md-4">
                                <ul class="nav contact-buttons">
                                    <li>
                                        <a href="{{ route('contact') }}" >Contact Us</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('application') }}" class="btn btn-rounded-dark">Apply Now</a>
                                    </li>

                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>