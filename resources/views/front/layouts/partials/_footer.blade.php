<!-- Site footer -->
<footer class="footer">
    <div class="row">
        <div class="col-lg-3">
            <img class="regulator img-responsive" src="images/FR-Logo3.png" alt="Funding Regulator Logo"/>
        </div>
        <div class="col-lg-6">
            <img class="img-responsive" src="images/footer-logo.png" alt="Logo" class="footer-logo"/>
            <h4>Karuna Appeals</h4>
            <p>72 Holloway Road, London, N7 8JG</p>
            <p>&nbsp;</p>
            <p>The Karuna Trust Founded 1987, Charity No. 327461. Aid for India Founded 1980, Charity No. 280551</p>
            <p>For more on Karuna please visit our mother site <a target="_blank" href="https://www.karuna.org">www.karuna.org</a>
        </div>
        <div class="col-lg-3">
            <a href="/privacy-policy" class="btn btn-rounded">Privacy Policy</a>
        </div>
    </div>
</footer>