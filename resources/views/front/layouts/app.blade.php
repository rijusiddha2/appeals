<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')
    <meta name="author" content="Rijusiddha Drewett | Karuna Trust">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Karuna | Appeals') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" >
    <link rel="stylesheet" href="https://use.typekit.net/azs3wkc.css">
    @yield('styles')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        @include('front.layouts.partials._nav')

        @if(Session::has('flash_message'))
            <div style="margin-top:15px;" class="col-md-8" id="success-message">
                <div class="alert alert-success"><em> {{ session('flash_message')  }}</em>
                </div>
            </div>
        @endif
        @if (count($errors) > 0)
            <div class="row">
                <div class="container-fluid">
                    <div style="margin-top:15px;" class="col-md-8">

                        @include ('errors.index')
                    </div>
                </div>
            </div>
        @endif
        <div id="content">
            @yield('content')
        </div>

        @include('front.layouts.partials._footer')
    </div>
    @yield('scripts')
    @if(url('/'))
        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
    @endif
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>