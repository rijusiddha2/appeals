<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.=
w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="D"http://www.w3.org/1999/xhtml">
<head>
    <title>New Appeals Website Enquiry</title>
</head>
<style>
    .email-header{
        background-color:red;
        text-align: center;
        color:white;
        padding:10px;
        font-size: 24px;
    }
</style>

<body>
    <div class="email-header">
        <strong>Karuna</strong> | Appeals
    </div>
    <p>A new website enquiry has been received.</p>

    <table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
        <tbody>
            <tr>
                <td width="50%">
                    Name
                </td>

                <td width="50%">
                   {{ $contact->first_name }} {{ $contact->last_name }}
                </td>
            </tr>
            <tr>
                <td width="50%">
                    Email
                </td>

                <td width="50%">
                    {{ $contact->email  }}
                </td>
            </tr>
            <tr>
                <td width="50%">
                    Subject
                </td>

                <td width="50%">
                    {{ $contact->subject  }}
                </td>
            </tr>
            <tr>
                <td width="50%">
                    Message
                </td>

                <td width="50%">
                    {{ $contact->message  }}
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>