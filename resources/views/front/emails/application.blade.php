<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.=
w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>New application for {{ $application->title  }} {{ $application->first_name }} {{ $application->last_name }}</title>

</head>
<style>
    .email-header{
        background-color:red;
        text-align: center;
        color:white;
        padding:10px;
        font-size: 24px;
    }
</style>

<body>
<div class="email-header">
    <strong>Karuna</strong> | Appeals
</div>
<p>A new website application has been created for {{ $application->title  }} {{ $application->first_name }} {{ $application->last_name }}.</p>

<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Personal Contact Details</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td width="50%">
                Name
            </td>

            <td width="50%">
                {{ $application->title  }} {{ $application->first_name }} {{ $application->last_name }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Gender
            </td>

            <td width="50%">
                {{ $application->gender  }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Address
            </td>

            <td width="50%">
                {{ $application->address  }}<br/>
                {{ $application->postcode }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Email
            </td>

            <td width="50%">
                {{ $application->email  }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Mobile
            </td>

            <td width="50%">
                {{ $application->mobile  }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Alternative Telephone
            </td>

            <td width="50%">
                {{ $application->alt_tel  }}
            </td>
        </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
        <tr>
            <th colspan="2">Application Details</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="50%">
                Work Experience
            </td>
            <td width="50%">
                {{ $application->details->work_experience }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Triratna Involvement
            </td>
            <td width="50%">
                {{ $application->details->triratna_history }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                How found out about Karuna Appeals
            </td>
            <td width="50%">
                {{ $application->details->contact_source }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Health and Fitness level
            </td>
            <td width="50%">
                {{ $application->details->health == 1 ? 'Yes' : 'No' }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Health Details
            </td>
            <td width="50%">
                {{ $application->details->health_details }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Any phsychiatric treatment or psychological medication?
            </td>
            <td width="50%">
                {{ $application->details->medication  == 1 ? 'Yes' : 'No' }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Phsychiatric details
            </td>
            <td width="50%">
                {{ $application->details->medication_details }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Safeguarding investigations?
            </td>
            <td width="50%">
                {{ $application->details->safeguarding_offence == 1 ? 'Yes' : 'No' }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Criminal Offences?
            </td>
            <td width="50%">
                {{ $application->details->criminal_offence  == 1 ? 'Yes' : 'No' }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Safegaurding and/or Criminal details
            </td>
            <td width="50%">
                {{ $application->details->offence_details }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Support Package
            </td>
            <td width="50%">
                {{ $application->details->support_package }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Additional Support Details
            </td>
            <td width="50%">
                {{ $application->details->support_extras }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Ethos Commitment?
            </td>
            <td width="50%">
                {{ $application->details->ethos  == 1 ? 'Yes' : 'No' }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Ethos Details if not agreed
            </td>
            <td width="50%">
                {{ $application->details->ethos_details }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Data Consent
            </td>
            <td width="50%">
                {{ $application->details->privacy  == 1 ? 'Yes' : 'No'}}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Keep details for future opportunities
            </td>
            <td width="50%">
                {{ $application->details->future_opportunites == 1 ? 'Yes' : 'No' }}
            </td>
        </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
        <tr>
            <th colspan="2">Appeal Details</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="50%">
                Type of appeal(s) interested in
            </td>
            <td width="50%">
                {{ $application->appeals->type }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Season
            </td>
            <td width="50%">
                {{ $application->appeals->season }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Year
            </td>
            <td width="50%">
                {{ $application->appeals->year }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                Other Comments
            </td>
            <td width="50%">
                {{ $application->appeals->comments }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                What is it that draws you to want to do a Karuna Appeal?
            </td>
            <td width="50%">
                {{ $application->appeals->draw }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                What do you think you would bring to the Appeal?
            </td>
            <td width="50%">
                {{ $application->appeals->skills }}
            </td>
        </tr>
        <tr>
            <td width="50%">
                What difficulties might you anticipate doing an Appeal?
            </td>
            <td width="50%">
                {{ $application->appeals->difficulties }}
            </td>
        </tr>
    </tbody>
</table>
</body>

</html>