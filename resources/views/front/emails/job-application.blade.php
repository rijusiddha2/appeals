<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.=
w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>New application for {{ $application->title  }} {{ $application->first_name }} {{ $application->last_name }}</title>

</head>
<style>
    .email-header{
        background-color:red;
        text-align: center;
        color:white;
        padding:10px;
        font-size: 24px;
    }
</style>

<body>
<div class="email-header">
    <strong>Karuna</strong> | Appeals
</div>
<p>A new website job application ({{ $application->job->title }}) has been created for {{ $application->title  }} {{ $application->first_name }} {{ $application->last_name }}.</p>

<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Personal Contact Details</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Name
        </td>

        <td width="50%">
            {{ $application->title  }} {{ $application->first_name }} {{ $application->last_name }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Address
        </td>

        <td width="50%">
            {{ $application->address  }}<br/>
            {{ $application->postcode }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Email
        </td>

        <td width="50%">
            {{ $application->email  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Mobile
        </td>

        <td width="50%">
            {{ $application->mobile  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Alternative Telephone
        </td>

        <td width="50%">
            {{ $application->alt_tel  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Work Permit
        </td>

        <td width="50%">
            {{ $application->work_permit == 1 ? 'Yes' : 'No'  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Driving Licence
        </td>

        <td width="50%">
            {{ $application->driving_licence == 1 ? 'Yes' : 'No'  }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Employment details (current or recent)</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Job Title
        </td>
        <td width="50%">
            {{ $application->currentHistory->job_title }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Employer Name / Address
        </td>
        <td width="50%">
            {{ $application->currentHistory->employer_details }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Date employed from
        </td>
        <td width="50%">
            {{ $application->currentHistory->employed_from->format('d/m/Y') }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Date employed to
        </td>
        <td width="50%">
            {{ $application->currentHistory->employed_to->format('d/m/Y') }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Responsibilities
        </td>
        <td width="50%">
            {{ $application->currentHistory->responsibilities }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Employment details (previous)</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Job Title
        </td>
        <td width="50%">
            {{ $application->previousHistory->job_title }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Employer Name / Address
        </td>
        <td width="50%">
            {{ $application->previousHistory->employer_details }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Date employed from
        </td>
        <td width="50%">
            {{ $application->previousHistory->employed_from->format('d/m/Y') }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Date employed to
        </td>
        <td width="50%">
            {{ $application->previousHistory->employed_to->format('d/m/Y') }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Responsibilities
        </td>
        <td width="50%">
            {{ $application->previousHistory->responsibilities }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Qualifications and Education</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Professional Qualifications
        </td>
        <td width="50%">
            {{ $application->qualifications->professional_qualifications }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Educational Qualifications
        </td>
        <td width="50%">
            {{ $application->qualifications->educational_qualifications }}
        </td>
    </tr>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th colspan="2">Further Details</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
           Skills
        </td>
        <td width="50%">
            {{ $application->further->skills }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Disabilities
        </td>
        <td width="50%">
            {{ $application->further->disabilities }}
        </td>
    </tr>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th colspan="2">Safeguarding</th>
    </tr>
    </thead>
    <tr>
        <td width="50%">
            Safeguarding Investigations
        </td>
        <td width="50%">
            {{ $application->safeguarding->vunerable_sanctions == 1 ? 'Yes' : 'No' }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Criminal Offences
        </td>
        <td width="50%">
            {{ $application->safeguarding->criminal_offences == 1 ? 'Yes' : 'No' }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Safeguarding Details (if applicable)
        </td>
        <td width="50%">
            {{ $application->safeguarding->safeguarding_details }}
        </td>
    </tr>
    </tbody>
</table>
</body>

</html>