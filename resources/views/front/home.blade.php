@extends('front.layouts.app')

@section('content')
        <div class="flex-center position-ref full-height">

            <div>
                {{ $page->content }}
            </div>
        </div>
    @endsection