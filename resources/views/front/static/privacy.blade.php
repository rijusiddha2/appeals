<h1>Legal Information</h1>
<p><a id="pageTop"></a></p>
<div id="accordion" class="panel-group">
    <div class="panel panel-default">
        <div id="headingOne" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed"><h4 class="panel-title">Introduction</h4></a>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" style="height: 0px;">
            <div class="panel-body">
                <p>In this Privacy Policy, <strong> "We", "Us" and "Our" mean Karuna Trust</strong> registered in England and Wales, charity number 327461. Our registered office is 72 Holloway Road, London, N7 8JG.</p>
                <p>We are firmly committed to protecting your privacy and aim to be clear when we collect your information and use it only as you would reasonably expect.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="headingTwo" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo"><h4 class="panel-title">How we use Personal Information</h4></a>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse in" style="">
            <div class="panel-body">
                <p>We will not use your personal information unless we have first told you how we will use it or it is obvious how we will use it. We collect and use personal information for:</p>
                <ul>
                    <li>Fundraising</li>
                    <li>Processing donations</li>
                    <li>Supporter development and administration</li>
                    <li>Processing general enquiries</li>
                    <li>Volunteer recruitment</li>
                    <li>Volunteer administration</li>
                    <li>Governance and Trustee management</li>
                    <li>Employee recruitment</li>
                    <li>Employee administration</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="headingThree" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="collapsed"><h4 class="panel-title">Our legal basis for processing personal information</h4></a>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body">
                <p>Our lawful basis for the purposes that we process personal information is consent, for the performance of a contract or for our legitimate interests.</p>
                <p>The law allows us to collect and use personal data if it is necessary for our legitimate business interest and so long as its use is fair, balanced and does not unduly impact your rights. In many situations, the best approach for our members is to process personal data because of our legitimate interests, rather than consent.</p>
                We will ask for your consent to send you updates about our work via post, email, telephone, or text messages. You can withdraw consent for this at any time.
                <p>We may also share your personal information where we are compelled by law to do so.</p>
                <p>We process employee personal information to meet our legal obligations as an employer.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="headingFour" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour" class="collapsed"><h4 class="panel-title">How we collect personal information</h4></a>
        </div>
        <div id="collapseFour" class="panel-collapse collapse">
            <div class="panel-body">
                <p>We collect personal information from you directly through our website, paper forms and sometimes over the telephone.</p>
                <p>We may collect information about the software on your computer (your browser version etc.) and your IP address (your connection with the internet) to improve your interaction with the Website and for our records. This may happen automatically without your being aware of it.</p>
                <p>We may use cookies (small text files which we and other website operators store on your computer when you visit our websites) to deliver a better and more personalised interaction. They enable us to recognise you when you return to the Website, store information about your preferences, and improve the way your searches are processed. They also enable us to generate statistics about the number of visitors we have and how they use the Website and the internet. You can set your browser to reject our cookies if you wish (you should consult your browser help section for details), but this might restrict your use of the website and other websites.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="headingFive" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive" class="collapsed"><h4 class="panel-title">The personal information that we collect</h4></a>
        </div>
        <div id="collapseFive" class="panel-collapse collapse">
            <div class="panel-body">
                <p>The type and quantity of information we collect and how we use it depends on why you are providing it.</p>
                <p><span style="text-decoration: underline;">For fundraising and processing donations</span> we will collect:</p>
                <ul>
                    <li>Your name</li>
                    <li>Your contact details</li>
                    <li>Your mailing preferences</li>
                    <li>Your bank or credit card details</li>
                </ul>
                <p><span style="text-decoration: underline;">For supporter development</span> we may also collect:</p>
                <ul>
                    <li>Your interests</li>
                    <li>Your date of birth</li>
                    <li>Your occupation</li>
                    <li>Information about your demographic</li>
                    <li>Information about your specialities</li>
                    <li>Information about your qualifications</li>
                </ul>
                <p><span style="text-decoration: underline;">For volunteer recruitment and administration</span>, we will collect:</p>
                <ul>
                    <li>Your name</li>
                    <li>Your contact details</li>
                    <li>Your employment history</li>
                    <li>Your interests or information which may support your application</li>
                    <li>Your mailing preferences</li>
                    <li>Your tax details, date of birth and photo ID</li>
                    <li>Your bank details</li>
                    <li>Your medical information, which is optional</li>
                    <li>Your likeness in an image or video, which is optional</li>
                    <li>Your unspent criminal record offences under the Rehabiliation of Offenders Act 1974</li>
                </ul>
                <p><span style="text-decoration: underline;">For employee recruitment and administration</span>, we will collect:</p>
                <ul>
                    <li>Your name</li>
                    <li>Your contact details</li>
                    <li>Your employment history</li>
                    <li>Your interests or information which may support your application</li>
                    <li>Your mailing preferences</li>
                    <li>Your tax details, date of birth and photo ID</li>
                    <li>Your bank details</li>
                    <li>Your gender, nationality, ethnicity, religion and if you have a disability insofar as we have a legal obligation to do so</li>
                    <li>Your spiritual or religious preferences, which are optional and only recorded if volunteered at the point of application</li>
                    <li>Your medical information, which is optional&nbsp;</li>
                    <li>Your spent and/or unspent criminal record offences, under the Rehabilitation of Offenders Act 1974, depending on the role applied for&nbsp;</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="headingSix" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix" class="collapsed"><h4 class="panel-title">How we may share personal information</h4></a>
        </div>
        <div id="collapseSix" class="panel-collapse collapse">
            <div class="panel-body">
                <p>We may share some of your personal information with organisations that carry out processing operations on our behalf, such as web services companies and mailing organisations. We carry out checks on these companies before we work with them and put a contract in place that sets out our expectations and requirements, especially regarding how they manage the personal information that we give to them.</p>
                <p>We do not sell or share personal information to third parties for the purposes of marketing. But, if we run an event in partnership with another named organisation your details may need to be shared with them. We will be very clear what will happen to your personal information if you register for such an event.</p>
                <p>We will only ever share your data in other circumstances if we have your explicit and informed consent, or we are required by law to do so.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="headingSeven" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven" class="collapsed"><h4 class="panel-title">How long we keep personal information</h4></a>
        </div>
        <div id="collapseSeven" class="panel-collapse collapse">
            <div class="panel-body">
                <p dir="ltr">We will only keep personal information for as long as we have a valid reason for keeping it. After that we delete or dispose of the information securely.</p>
                <p dir="ltr"><span style="text-decoration: underline;">If you are a donor or are considering becoming one:</span></p>
                <ul>
                    <li>Information collected by our fundraisers is kept for the length of that appeal and then destroyed unless you sign up to become a donor.
                    </li>
                    <li>Donor information is kept on record for the length of time you are a supporter and then 6 months post leaving in case of any queries relating to your support.</li>
                    <li>Financial information relating to donor contributions is kept for the length of time you support us and then 7 years post ceasing your donations to comply with UK Tax laws. Please note that we do not store any Credit or Debit Card details once they have been processed.</li>
                </ul>
                <p dir="ltr"><span style="text-decoration: underline;">If you are a volunteer or have applied to be one:</span></p>
                <ul>
                    <li>Volunteer application information is kept for the length of the application process and destroyed if the applicant does not proceed to become a volunteer, unless consent is given to retain information for the purposes of future applications.</li>
                    <li>Contact details of the individuals who have consented to join our volunteer recruitment mailing list will be kept and may be removed at any time, either by unsubscribing via the emails or by contacting the office. </li>
                    <li>Any volunteer photos or videos given to us with prior consent for the purposes of recruitment will be kept indefinitely which will be stated clearly on the consent form.</li>
                    <li>Accepted volunteer information is kept indefinitely to ensure volunteer needs are met and available if the applicant decides to volunteer for &nbsp;another appeal at a later date.</li>
                </ul>
                <p><span style="text-decoration: underline;">If you are an employee or have applied to be one:</span></p>
                <ul>
                    <li>Employment application information is kept for the length of the application process and destroyed if the applicant does not proceed to become an employee of Karuna.</li>
                    <li>Employee information is kept &nbsp;for the duration of their employment and 6 years after employment ceases in accordance with UK Employment Law.</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="headingEight" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight" class="collapsed"><h4 class="panel-title">Your rights</h4></a>
        </div>
        <div id="collapseEight" class="panel-collapse collapse">
            <div class="panel-body">
                <p>You have a right to know what personal data we hold, who we acquired it from, how we process it, the logic involved in any automatic processing, and who we disclose it to.</p>
                <p>You have a right to ask us not to process your personal data for direct marketing purposes.</p>
                <p>You have a right to ask us not to make decisions based solely on the automatic processing of your personal information.</p>
                <p>You have a right to ask us not to process your personal information in a way that is likely to cause unwarranted and substantial damage or distress.</p>
                <p>You have a right to ask us to erase your personal information.</p>
                <p>These statutory rights are qualified by exceptions and exemptions.</p>
                <p>To exercise any of these rights, please contact us using the address below.</p>
                <p>You can find out more about your rights from the Information Commissioner, who regulates data protection and privacy. The Information Commissioner's website is <a href="https://www.ico.org.uk" target="_blank">www.ico.org.uk</a></p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="headingTen" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="true" aria-controls="collapseTen" class="collapsed"><h4 class="panel-title">Changes to this policy</h4></a>
        </div>
        <div id="collapseTen" class="panel-collapse collapse">
            <div class="panel-body">
                <p>We may change this Privacy Policy from time to time. If we make any significant changes in the way we treat your personal information we will make this clear on our website or by contacting you directly.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="headingEleven" class="panel-heading">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="true" aria-controls="collapseEleven" class="collapsed"><h4 class="panel-title">Contact</h4></a>
        </div>
        <div id="collapseEleven" class="panel-collapse collapse">
            <div class="panel-body">
                <p>Please feel free to contact us with any questions, comments or queries regarding this Privacy Policy. All questions should be directed to the Data Protection contact at <a href="mailto:info@karuna.org">info@karuna
                        .org</a>
                           or 020 7700 3434</p>
            </div>
        </div>
    </div>
</div>