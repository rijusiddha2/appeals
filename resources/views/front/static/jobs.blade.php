@extends('front.layouts.app')

@section('meta')
    <title>{{$page->title}}</title>
    <meta name="description" content="Test">
    <meta name="keywords" content="{{ $page->meta->meta_keywords }}">
    <meta property="og:description" content="{{ $page->meta->og_description }}" />
    <meta property="og:url" content="{{ $page->meta->og_url }}" />
    <meta property="og:image" content="{{ $page->meta->og_image }}" />
    <meta property="og:title" content="{{ $page->meta->og_title }}" />
@endsection
@section('content')
    <div id="carousel">
        <div class="slider">
            <div class="bxslider">
                <div class="parallax" style="background-image:url('/images/banner/karuna-jobs-header-alt.jpg')">
                    <div class="parallax" style="background-image:url('/images/banner/karuna-jobs-header-alt.jpg')">
                        <div class="image-text">
                            <div class="title">
                                Job opportunities
                            </div>
                            <div class="caption" >
                                Meaningful work at Karuna
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-block">

    </div>
    <div class="content-block">
        <div class="row">
            <div class="col-md-offset-1 col-md-4">
                <h1>Join Us</h1>
            </div>
            <div class="col-md-6">
                <p>
                    Ever considered what it’s like to work for a Buddhist organisation? Ready for a career change?<br/>
                    We’re a UK charity inspired by Buddhist values, working to end caste-based discrimination, poverty and inequality in India and Nepal. We offer a generous support package including pension
                    contributions.<br/>
                    Karuna fosters a culture of equality, generosity and community. We support the personal development of our staff, with significant allowances for both vacations and retreats.  We also spend time on
                    retreat together as a team each year to connect with one another as people, not just colleagues. This ethos continues into our office and fundraising environments.<br/>
                    We welcome all applicants with the necessary skills and experience for our positions. If you think you would enjoy working in a Buddhist Team-Based Right Livelihood, founded by the Triratna Buddhist community, please
                    find our job opportunities below.
                </p>
            </div>
        </div>
    </div>
    <hr class="divider">
    @if(isset($jobs))
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h1>Current Jobs</h1>

                @foreach($jobs as $job)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ $job->title }}</h3>
                        </div>
                        <div class="panel-body">
                            <h4 class="text-center">{{ $job->caption }}</h4>
                            <p>Closing Date: {{ !empty($job->closing_date) ? $job->closing_date->format('d/m/Y') : 'No Closing Date: Please enquire for details'}}</p>
                            <p><a target="_blank" href="images/{{$job->documents->first()->filename}}">Click here for Job Description</a></p>
                            <a href="{{ route('job.apply') }}" class="btn btn-rounded-dark">Apply Now</a>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>

    @else
        <h3>We currently have no positions we are recruiting for.</h3>
    @endif
    </div>

@endsection