@extends('front.layouts.app')
@section('content')
    <div class="image-parallax" style="background-image:url('/images/banner/orange-rangoli-background-02.jpg')">
        <div class="image-text">
            <div class="caption">
               Thanks For Contacting Us!
            </div>
        </div>
    </div>
    <div class="content-block">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <p class="alert alert-success text-center">Thank you for your application! A member of the team will contact you shortly to discuss your application.</p>
            </div>
        </div>
    </div>
@endsection