@extends('front.layouts.app')
@section('content')
    <div id="carousel">
        <div class="slider">
            <div class="bxslider">
                <div class="parallax" style="background-image:url('/images/banner/karuna-jobs-header-alt.jpg')">
                    <div class="parallax" style="background-image:url('/images/banner/karuna-jobs-header-alt.jpg')">
                        <div class="image-text">
                            <div class="caption">
                                Job Application
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-block">

    </div>

    {{ Form::open(['route' => 'job.process']) }}

    <div class="content-block">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <h4 class="text-center">Thank you for applying for a position within Karuna. Please complete the questions below and press submit. We aim to acknowledge your application within one week. If you have not received a response within that time, please contact us.</h4>
            </div>
        </div>
    </div>
    <div class="content-block">
        <hr class="divider"/>
    </div>

    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Your Details</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('job_id', 'What is the position you are applying for?', ['control-label']) }}
                                {{ Form::select('job_id', $jobs, old('jobs'), ['class' => 'form-control selectpicker', 'required']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('title', 'Title', ['control-label']) }}
                                {{ Form::text('title', old('title'), ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('first_name', 'First Name', ['control-label']) }}
                                {{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('last_name', 'Last Name', ['control-label']) }}
                                {{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('order_name', 'Order Name (if applicable)', ['control-label']) }}
                                {{ Form::text('order_name', old('order_name'), ['class' => 'form-control']) }}
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="form-group">
                                {{ Form::label('address', 'Address', ['control-label']) }}
                                {{ Form::textarea('address', old('address'), ['class' => 'form-control', 'rows' => 4, 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('postcode', 'Postcode', ['control-label']) }}
                                {{ Form::text('postcode', old('postcode'), ['class' => 'form-control', 'required']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('mobile', 'Mobile Number', ['control-label']) }}
                                {{ Form::text('mobile', old('mobile'), ['class' => 'form-control', 'required']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('alt_tel', 'Alternative Number', ['control-label']) }}
                                {{ Form::text('alt_tel', old('alt_tel'), ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('email', 'Email Address', ['control-label']) }}
                                {{ Form::email('email', old('email'), ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                <p>Do you require a UK work permit?</p>
                                <label>
                                    {{ Form::radio('work_permit', 0, old('work_permit')) }}
                                    No
                                </label>
                                <br/>
                                <label>
                                    {{ Form::radio('work_permit', 1, old('work_permit')) }}
                                    Yes
                                </label>
                            </div>
                            <div class="form-group">
                                <p>Do you hold a current valid UK driving licence?</p>
                                <label>
                                    {{ Form::radio('driving_licence', 0, old('driving_licence')) }}
                                    No
                                </label>
                                <br/>
                                <label>
                                    {{ Form::radio('driving_licence', 1, old('driving_licence')) }}
                                    Yes
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Employment History</h3>
                </div>
                <div class="panel-body">
                    <p>Please give a brief outline of your past work experience.</p>
                    <h4>Current or most recent employment</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('history_current[job_title]', 'Job title', ['control-label']) }}
                                {{ Form::text('history_current[job_title]', old('history_current[job_title]'), ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('history_current[employer_details]', 'Employer Name and Address', ['control-label']) }}
                                {{ Form::textarea('history_current[employer_details]', old('history_current[employer_details]'), ['class' => 'form-control', 'rows' => 4, 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('history_current[employed_from]', 'Date employed from', ['control-label']) }}
                                {{ Form::text('history_current[employed_from]', old('history_current[employed_from]'), ['class' => 'form-control datepicker', 'placeholder' => 'Format d-m-y', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('history_current[employed_to]', 'Date employed to', ['control-label']) }}
                                {{ Form::text('history_current[employed_to]', old('history_current[employed_to]'), ['class' => 'form-control datepicker', 'placeholder' => 'Format d-m-y', 'required']) }}
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('history_current[responsibilities]', 'Outline of responsibilities', ['control-label']) }}
                                {{ Form::textarea('history_current[responsibilities]', old('history_current[responsibilities]'), ['class' => 'form-control', 'rows' => 15, 'required']) }}
                            </div>
                        </div>
                    </div>
                    <h4>Previous Employment</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('history_previous[job_title]', 'Job title', ['control-label']) }}
                                {{ Form::text('history_previous[job_title]', old('history_previous[job_title]'), ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('history_previous[employer_details]', 'Employer Name and Address', ['control-label']) }}
                                {{ Form::textarea('history_previous[employer_details]', old('history_previous[employer_details]'), ['class' => 'form-control', 'rows' => 4, 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('history_previous[employed_from]', 'Date employed from', ['control-label']) }}
                                {{ Form::text('history_previous[employed_from]', old('history_previous[employed_from]'), ['class' => 'form-control datepicker','placeholder' => 'Format d-m-y', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('history_previous[employed_to]', 'Date employed to', ['control-label']) }}
                                {{ Form::text('history_previous[employed_to]', old('history_previous[employed_to]'), ['class' => 'form-control datepicker', 'placeholder' => 'Format d-m-y', 'required']) }}
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('history_previous[responsibilities]', 'Outline of responsibilities', ['control-label']) }}
                                {{ Form::textarea('history_previous[responsibilities]', old('history_previous[responsibilities]'), ['class' => 'form-control', 'rows' => 15, 'required']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Qualifications and Education</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('qualifications[professional_qualifications]', 'Please give a brief outline of any professional qualifications, including the year and place of study. Please include membership of professional societies or associations.', ['control-label']) }}
                                {{ Form::textarea('qualifications[professional_qualifications]', old('qualifications[professional_qualifications]'), ['class' => 'form-control', 'rows' => 10, 'required']) }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('qualifications[educational_qualifications]', 'Please give a brief outline of your education qualifications, including the year and place of study.', ['control-label']) }}
                                <br/>
                                <br/>
                                {{ Form::textarea('qualifications[educational_qualifications]', old('qualifications[educational_qualifications]'), ['class' => 'form-control', 'rows' => 10, 'required']) }}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Further Details</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('further_details[skills]', 'Please use this section to state your reasons for applying for this post. Outline the skills and experience you have gained, either in paid work, unpaid/voluntary work, or through your leisure activities which you think are relevant. Please address the person specification.', ['control-label']) }}
                                {{ Form::textarea('further_details[skills]', old('further_details[skills]'), ['class' => 'form-control', 'rows' => 10, 'required']) }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('further_details[disabilities]', 'If you have a disability please tell us about any adjustments we may need to make to assist you at interview.', ['control-label']) }}
                                <br/>
                                <br/>
                                {{ Form::textarea('further_details[disabilities]', old('further_details[disabilities]'), ['class' => 'form-control', 'rows' => 10, 'required']) }}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Safeguarding</h3>
                </div>
                <div class="panel-body">
                    <p>Karuna is committed to protecting children, young people and vulnerable adults from all forms of physical or psychological violence, injury or abuse, neglect, maltreatment, exploitation and sexual abuse. To read
                        our safeguarding policy, please visit <a href="https://www.karuna.org">www.karuna.org</a></p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <p>Have you been the subject of any disciplinary investigation and/or sanction by any organisation due to concerns about your behaviour towards children and/or vulnerable adults?</p>
                                <label>
                                    {{ Form::radio('safeguarding[vunerable_sanctions]', 0, old('safeguarding[vunerable_sanctions]')) }}
                                    No
                                </label>
                                <br/>
                                <label>
                                    {{ Form::radio('safeguarding[vunerable_sanctions]', 1, old('safeguarding[vunerable_sanctions]')) }}
                                    Yes
                                </label>
                            </div>
                            <div class="form-group">
                                <p>Do you have any criminal record offences which are currently unspent under the Rehabilitation of Offenders Act 1974 (You are not required to disclose anything that is deemed ‘spent’).</p>
                                <label>
                                    {{ Form::radio('safeguarding[criminal_offences]', 0, old('safeguarding[criminal_offences]')) }}
                                    No
                                </label>
                                <br/>
                                <label>
                                    {{ Form::radio('safeguarding[criminal_offences]', 1, old('safeguarding[criminal_offences]')) }}
                                    Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            {{ Form::label('safeguarding[safeguarding_details]', 'If you have answered YES to either question above or if you have any reason to believe that by working for Karuna you may pose a risk to the safety of children or vulnerable adults, please give additional details here.') }}
                            {{ Form::textarea('safeguarding[safeguarding_details]', old('safeguarding[safeguarding_details]'), ['class' => 'form-control', 'rows' => 8]) }}
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">References</h3>
                </div>
                <div class="panel-body">
                    <p>We will only contact referees after interview and with your knowledge. Referees should be able to comment on your work. If you practise Buddhism within the context of the Triratna Buddhist Community you may also provide details of community members, preceptors, kalyana mitras and centre chairs.</p>
                    <hr/>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('reference[name_1]', 'Name Referee 1', ['control-label']) }}
                                {{ Form::text('reference[name_1]', old('reference[relationship_1]'), ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('reference[relationship_1]', 'Context of Relationship', ['control-label']) }}
                                {{ Form::text('reference[relationship_1]', old('reference[relationship_1]'), ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('reference[telephone_1]', 'Telephone', ['control-label']) }}
                                {{ Form::text('reference[telephone_1]', old('reference[telephone_1]'), ['class' => 'form-control', 'required']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('reference[email_1]', 'Email', ['control-label']) }}
                                {{ Form::email('reference[email_1]', old('reference[email_1]'), ['class' => 'form-control', 'required']) }}
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {{ Form::label('reference[name_2]', 'Name Referee 2', ['control-label']) }}
                                {{ Form::text('reference[name_2]', old('reference[name_2]'), ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('reference[relationship_2]', 'Context of Relationship', ['control-label']) }}
                                {{ Form::text('reference[relationship_2]', old('reference[relationship_2]'), ['class' => 'form-control', 'required']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('reference[telephone_2]', 'Telephone', ['control-label']) }}
                                {{ Form::text('reference[telephone_2]', old('reference[telephone_2]'), ['class' => 'form-control', 'required']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('reference[email_2]', 'Email', ['control-label']) }}
                                {{ Form::email('reference[email_2]', old('reference[email_2]'), ['class' => 'form-control', 'required']) }}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {{ Form::label('reference[dates_unavailable]', 'Please tell us any days/dates you are unavailable for interview.', ['control-label']) }}
                                {{ Form::text('reference[dates_unavailable]', old('reference[dates_unavailable]'), ['class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Your Privacy</h3>
                </div>
                <div class="panel-body">
                    <p>Karuna takes your privacy seriously. We will use your personal information to process your application and only in other ways which you would reasonably expect.We will never sell or trade your details. To read more about how we value and respect your privacy and data, please visit <a target="_blank" href="https://www.karuna.org/privacy-policy">www.karuna.org</a>.</p>
                    <p>We are registered as a data controller with the UK Information Commissioner’s Office as Karuna Trust, Registration No. 327461.</p>

                    <h2>Thank You</h2>
                    <p>Thank you for your interest in working with Karuna. We aim to acknowledge your application within one week. If you have not received a response within that time, please <a target="_blank" href="{{ route('contact') }}
                    ">contact us.</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    {{ Form::submit('Apply', ['class' => 'btn btn-success pull-right']) }}
                </div>
            </div>
        </div>
    </div>



    {{ Form::close() }}
@endsection