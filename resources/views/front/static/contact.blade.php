@extends('front.layouts.app')
@section('content')
    <div id="carousel">
        <div class="slider">
            <div class="bxslider">
                <div class="parallax" style="background-image:url('/images/banner/orange-rangoli-background-02.jpg')">
                    <div class="parallax" style="background-image:url('/images/banner/orange-rangoli-background-02.jpg')">
                        <div class="image-text">
                            <div class="caption" >
                                Contact Us
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::open(['route' => 'contact']) }}

    <div class="content-block">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <h4 class="text-center">Not ready to apply yet? If you would like to have an informal chat about what is involved on an Appeal or which Karuna Appeal might work best for you, fill out your contact details and we’ll get back
                    to you.
                </h4>
            </div>
        </div>
    </div>
    <div class="content-block">
        <hr class="divider"/>
    </div>

    <div class="row">
        <div class="col-md-offset-2 col-md-3">
            <p class="contact-header">TELEPHONE</p>
            <p class="contact-telephone"><a href="tel:020 7700 3434">020 7700 3434</a></p>

            <p class="contact-header">EMAIL</p>
            <p><a href="{{ config('mail.from.address') }}">{{ config('mail.from.address') }}</a></p>

            <p class="contact-header">POST</p>

            <p> The Karuna Trust<br/>
                72 Holloway Road<br/>
                London<br/>
                N7 8JG<br/>
                United Kingdom
            </p>
        </div>
        <div class="col-md-5">
            {{ Form::open(['route' => 'contact']) }}
            <p class="contact-header">GENERAL ENQUIRIES</p>
            <div class="form-group col-md-6 no-padding-left">
                {{ Form::label('first_name', 'First Name *', ['control-label']) }}
                {{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'required']) }}
            </div>
            <div class="form-group col-md-6 no-padding-right">
                {{ Form::label('last_name', 'Last Name *', ['control-label']) }}
                {{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'required']) }}
            </div>
            <div class="form-group">
                {{ Form::label('email', 'Email *', ['control-label']) }}
                {{ Form::email('email', old('email'), ['class' => 'form-control', 'required']) }}
            </div>

            <div class="form-group">
                {{ Form::label('subject', 'Subject *', ['control-label']) }}
                {{ Form::text('subject', old('subject'), ['class' => 'form-control', 'required']) }}
            </div>

            <div class="form-group">
                {{ Form::hidden('dob', old('dob'), ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('message', 'Message *', ['control-label']) }}
                {{ Form::textarea('message', old('message'), ['class' => 'form-control']) }}
            </div>

            {{ Form::submit('Submit', ['class' => 'btn btn-rounded-dark']) }}
            {{ Form::close() }}
        </div>

    </div>


@endsection