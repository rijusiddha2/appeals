<div class="stat-wrapper">
    <div class="row">
        <div class="col-md-offset-2 col-md-4 col-lg-offset-2 col-lg-5">
            <img src="/images/medium_landscape/{!! $block->images->name !!}" alt="{!! $block->images->alt !!}" title="{!! $block->images->title !!}" class="img-responsive"/>
            <div class="stat stat-md">{!! $block->caption !!}</div>
        </div>
        <div class="col-md-3 col-lg-4">
            <div class="stat-text-md">
                {!! $block->text !!}
            </div>
        </div>

    </div>
</div>