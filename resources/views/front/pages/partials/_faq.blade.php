<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    @foreach($block->getFaqs($block->faq) as $faq)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{!! $faq->id !!}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{!! $faq->id !!}" aria-expanded="false" aria-controls="collapse{!! $faq->id !!}">
                        {!! $faq->question !!}
                    </a>
                </h4>
            </div>
            <div id="collapse{!! $faq->id !!}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{!! $faq->id !!}">
                <div class="panel-body">
                    {!! $faq->answer !!}
                </div>
            </div>
        </div>
    @endforeach
    <p>&nbsp;</p>
    <p>If your question is not answered here, please get in touch.</p>
    <a href="{!! route('contact') !!}" class="btn btn-rounded-dark">Contact Us</a>
</div>