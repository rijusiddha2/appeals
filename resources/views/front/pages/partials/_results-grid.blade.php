@foreach($resultsGrid as $result)
    @if($loop->iteration <= 9)
        <div class="col-lg-4">
            <div class="img-grid">
                <div class="image">
                    <img src="/images/medium_landscape/{!! $result->image->name !!}" class="img-responsive" alt="{!! $result->image->alt_tag !!}" title="{!! $result->image->alt_tag !!}"/>
                </div>
                <div class="img-text">
                    <h1 class="text-center">TOTAL = £{{$result->total_raised}}</h1>
                    <p class="text-center">{{ $result->title }}</p>
                </div>
            </div>
        </div>

    @endif
@endforeach