<div class="image-parallax image-button" style="background-image:url('/images/full_image/{!! $block->images->name !!}')">
    <div class="image-text">
        <div class="title">
            {!! $block->title !!}
        </div>
        <div class="caption">
            {!! $block->caption !!}
        </div>

        @if($block->link)
            <a class="btn btn-rounded" href="{!! $block->link !!}">{!! $block->link_text !!}</a>
        @endif
    </div>
</div>