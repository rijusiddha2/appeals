<div class="appeals-dates"
@if (Request::is('*telephone-appeals'))
     style="padding-bottom:0;"
    @endif
    >
    <div class="row">
        <div class="col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-1">
            <div class="img-wrapper">
                <div class="image">
{{--                    {{dd($block)}}--}}
                    <img src="/images/large_landscape/{!! $block->images->name !!}" alt="{!! $block->images->alt !!}" title="{!! $block->images->title !!}" class="img-responsive"/>
                </div>
            </div>
        </div>
        <div class="appeals-dates-text absolute-right">
            <div class="text">
                {!! $block->text !!}
            </div>
        </div>
    </div>
</div>