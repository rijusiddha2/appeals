<div id="carousel">
    <div class="slider">
        <div class="bxslider">
            @if($block->slideshow->parallax)
                @foreach($block->slideshow->slides as $slide)
                    <div class="parallax" style="background-image:url('/images/banner/{!! $slide->images->name !!}');">
                        <div class="parallax" style="background-image:url('/images/banner/{!! $slide->images->name !!}');"></div>
                        <div class="image-text">
                            <div class="title">
                                {!! $slide->title !!}
                            </div>
                            <div class="caption" >
                                {!! $slide->caption !!}
                            </div>
                        </div>
                    </div>

                @endforeach
            @else
                @foreach($block->slideshow->slides as $slide)
                    <div>
                        <img src="/images/banner/{!! $slide->images->name !!}" class="img-responsive" alt="{!! $slide->images->alt_tag !!}" title="{!! $slide->images->alt_tag !!}"/>
                        <div class="image-text">
                            <div class="title">
                                {!! $slide->title !!}
                            </div>
                            <div class="caption" >
                                {!! $slide->caption !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

    </div>

</div>