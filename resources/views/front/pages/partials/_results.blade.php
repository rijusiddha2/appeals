@foreach($results as $result)
    <div class="row" style="{{ $loop->last ? '' : 'padding-bottom:5%;' }}">
        @if($loop->iteration  % 2 == 0)
            <div class="col-lg-5 col-lg-offset-1">
                <div class="text">
                    <h1>{{$result->title}}</h1>
                    <h3>TEAM TOTAL: £{{ number_format($result->total_raised,0) }}</h3>
                    <h3 style="margin-top: 10px;">SUPPORTERS CONNECTED: {{$result->supporters}}</h3>
                    <p>{{$result->intro}}</p>
                </div>

            </div>
            <div class="col-lg-5 test">
                <div class="img-wrapper">
                    <div class="image">
                        <img src="/images/large_landscape/{!! $result->image->name !!}" class="img-responsive" alt="{!! $result->image->alt_tag !!}" title="{!! $result->image->alt_tag !!}"/>
                    </div>
                </div>

            </div>

         @else
            <div class="col-lg-5 col-lg-offset-1">
                <div class="img-wrapper">
                    <div class="image">
                        <img src="/images/large_landscape/{!! $result->image->name !!}" class="img-responsive" alt="{!! $result->image->alt_tag !!}" title="{!! $result->image->alt_tag !!}"/>
                    </div>
                </div>

            </div>
            <div class="col-lg-5 ">
                <div class="text">
                    <h1>{{$result->title}}</h1>
                    <h3>TEAM TOTAL: £{{ number_format($result->total_raised,0) }}</h3>
                    <h3 style="margin-top: 10px;">SUPPORTERS CONNECTED: {{$result->supporters}}</h3>
                    <p>{{$result->intro}}</p>
                </div>
            </div>
        @endif
    </div>
@endforeach