<div class="img-wrapper">
    <div class="image">
        <img src="/images/{!! \App\Image::getImageSize($block->column_width, $block->images->name)!!}/{!! $block->images->name !!}" alt="{!! $block->images->alt !!}" title="{!! $block->images->title !!}" class="img-responsive"/>
    </div>
    <div class="img-text">
        {!! $block->text !!}
    </div>
</div>