<div class="stat-wrapper">
    <div class="row">
        <div class="col-md-offset-6 col-md-3 col-lg-offset-4 col-md-4">

                <img src="/images/small_landscape/{!! $block->images->name !!}" alt="{!! $block->images->alt !!}" title="{!! $block->images->title !!}" class="img-responsive"/>
                <div class="stat stat-sm">{!! $block->caption !!}</div>

        </div>
        <div class="col-md-3">
            <div class="stat-text-sm">
                {!! $block->text !!}
            </div>
        </div>

    </div>
</div>