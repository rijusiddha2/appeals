<div class="appeals-dates">
    <div class="row">
        <div class="col-md-offset-5 col-md-6">
            <div class="img-wrapper">
                <div class="image">
                    <img src="/images/large_landscape/{!! $block->images->name !!}" alt="{!! $block->images->alt !!}" title="{!! $block->images->title !!}" class="img-responsive"/>
                </div>
            </div>
        </div>
        <div class="appeals-dates-text absolute-left">
            <div class="text">
                {!! $block->text !!}
            </div>
        </div>
    </div>
</div>