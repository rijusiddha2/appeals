<div class="blockquote">
    <div class="row">
        <div class="col-sm-2 col-sm-offset-5 col-xs-2 col-xs-offset-5">
            <img src="/images/quotation-marks.png" alt="quote marks" class="img-responsive quote center-block"/>
        </div>
    </div>
    {!! $block->text !!}
</div>
