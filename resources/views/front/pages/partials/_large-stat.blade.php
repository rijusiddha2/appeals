<div class="stat-wrapper">
    <div class="row">
        <div class="col-md-7 col-md-offset-1 col-lg-6">
            <img src="/images/large_landscape/{!! $block->images->name !!}" alt="{!! $block->images->alt !!}" title="{!! $block->images->title !!}" class="img-responsive"/>
            <div class="stat stat-lg">{!! $block->caption !!}</div>
        </div>
        <div class="col-md-2 col-lg-3">
            <div class="stat-text-lg">
                {!! $block->text !!}
            </div>
        </div>

    </div>
</div>