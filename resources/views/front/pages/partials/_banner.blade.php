@if($block->parallax)
    <div class="image-parallax" style="background-image:url('/images/{!! \App\Image::getImageSize($block->column_width, $block->images->name)!!}/{!! $block->images->name !!}')">
        <div class="image-text">
            <div class="title">
                {!! $block->title !!}
            </div>
            <div class="caption">
                {!! $block->caption !!}
            </div>
            @if($block->link)
                <a class="btn btn-rounded" href="{!! $block->link !!}">{!! $block->link_text !!}</a>
            @endif
        </div>
    </div>
@else
    <div class="image">
        <img src="/images/{!! \App\Image::getImageSize($block->column_width, $block->images->name)!!}/{!! $block->images->name !!}" alt="{!! $block->images->alt !!}" title="{!! $block->images->title !!}" class="img-responsive"/>
        <div class="image-text">
            <div class="title">
                {!! $block->title !!}
            </div>
            <div class="caption">
                {!! $block->caption !!}
            </div>
            @if($block->link)
                <a class="btn btn-rounded" href="{!! $block->link !!}">{!! $block->link_text !!}</a>
            @endif
        </div>

    </div>
@endif

