<div id="sub-menu" class="hidden-md hidden-sm hidden-xs">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 text-center">
            @foreach($submenus as $menu)
                <a class="{!! $menu['class'] !!}" href="{!! url($menu['link']) !!}">{!! $menu['label']  !!}</a>
            @endforeach
        </div>
    </div>
</div>

