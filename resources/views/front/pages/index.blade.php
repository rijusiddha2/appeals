@extends('front.layouts.app')
@section('meta')
    <title>{{$page->title}}</title>
    <meta name="description" content="Test">
    <meta name="keywords" content="{{ $page->meta->meta_keywords }}">
    <meta property="og:description" content="{{ $page->meta->og_description }}" />
    <meta property="og:url" content="{{ $page->meta->og_url }}" />
    <meta property="og:image" content="{{ $page->meta->og_image }}" />
    <meta property="og:title" content="{{ $page->meta->og_title }}" />
@endsection
@section('styles')
    <link rel="stylesheet" href="https://use.typekit.net/azs3wkc2.css">
    @endsection
@section('content')
        @foreach ($contentBlocks as $content)
            <div class="container-fluid"
                 @if($content->background)
                style="background:url('/images/{{ $content->background }}') no-repeat center top; background-size:contain;  "
                @endif>
                <div class="content-block {{ $content->id }}"
                     @if(($content->content_id == 16 && $content->page_id == 8)|| $content->id == 47 || $content->content_id == 23 || $content->content_id == 31)
                        style="padding-bottom:0;"
                     @endif
                >
                    <div class="row">
                        <?php $i = 0;?>
                        @foreach($content->getBlocks($content->id) as $block)
                            <div class="col-lg-{{ $block->column_width }} {{ $content->offset > 0 && $i == 0 ? 'col-lg-offset-'.$content->offset.'' : '' }}">
                                @include('front.pages.partials.'.$block->block->partial)
                            </div>
                            <?php $i++; ?>
                        @endforeach

                    </div>
                </div>
            </div>
        @endforeach
    <div class="container-fluid">
        <div class="content-block">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">Follow Us</h1>
                    <div class="social-icons">
                        <a target="_blank" class="https://www.facebook.com/karuna.org/"><i class="fab fa-facebook-square"></i></a>
                        <a target="_blank" class="https://twitter.com/karuna_trust_uk"><i class="fab fa-twitter-square"></i></a>
                        <a target="_blank" class="https://www.instagram.com/karunatrust"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="top-link">
        <i class="fas fa-chevron-up"></i>
        <h3 class="text-center">BACK TO TOP</h3>
    </div>

@endsection