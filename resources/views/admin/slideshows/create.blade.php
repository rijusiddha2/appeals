@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::open(['url' => 'admin/slideshows'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Slideshow
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('name', 'Name')  }}
                        {{ Form::text('name', old('name'), ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('controls', 'Controls?')  }}
                        {{ Form::checkbox('controls', 1, old('controls'))  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('pagination', 'Pagination?')  }}
                        {{ Form::checkbox('pagination', 1, old('pagination'))  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('auto', 'Auto Play')  }}
                        {{ Form::checkbox('auto', 1, old('auto'))  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('effect', 'Effect')  }}
                        {{ Form::select('effect', $effects, old('effect_id'), ['class="form-control selectpicker"'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('parallax', 'Parallax')  }}
                        {{ Form::checkbox('parallax', 1, old('parallax'))  }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('slideshows.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Create</button>
            {{ Form::close()  }}
        </div>
    </div>


@stop
