@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($slideshow, ['route' => ['slideshows.update', $slideshow->id], 'method' => 'PUT']) }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Slideshow
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('name', 'Name')  }}
                        {{ Form::text('name', $slideshow->name, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('controls', 'Controls?')  }}
                        {{ Form::checkbox('controls', $slideshow->controls)  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('pagination', 'Pagination?')  }}
                        {{ Form::checkbox('pagination', $slideshow->pagination)  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('auto', 'Auto Play')  }}
                        {{ Form::checkbox('auto', $slideshow->auto)  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('effect', 'Effect')  }}
                        {{ Form::select('effect', $effects, $slideshow->effect, ['class="form-control selectpicker"'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('parallax', 'Parallax')  }}
                        {{ Form::checkbox('parallax', $slideshow->parallax)  }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('slideshows.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <button type="submit" class="btn btn-info"><i class="fas fa-check"></i> Update</button>
            {{ Form::close()  }}
        </div>
    </div>


@stop
