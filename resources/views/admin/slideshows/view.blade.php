@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Slideshow Details
                </div>
                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $slideshow->name }}</dd>

                        <dt>Controls</dt>
                        <dd>{{ $slideshow->controls ? 'Yes' : 'No'}}</dd>

                        <dt>Pagination</dt>
                        <dd>{{ $slideshow->pagination ? 'Yes' : 'No'}}</dd>

                        <dt>Effect</dt>
                        <dd>{{ $slideshow->effect }}</dd>

                        <dt>Parallax</dt>
                        <dd>{{ $slideshow->parallax ? 'Yes' : 'No'}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-info alert-block" role="alert">
        Slides below can be re-ordered by dragging and dropping
    </div>
    <ul id="sortable">
        @if(isset($slideshow->slides))
            @foreach($slideshow->slides as $slide)
                <li id="{{ $slide->id }}">
                    <div class="box">
                        <div class="box-header">
                            Slide {{ $loop->index + 1 }}
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-md-8">
                                    <p>
                                        <img class="img-responsive" src="/images/banner/{{ $slide->images->name }}" alt="{{ $slide->images->alt_tag }}" title="{{ $slide->images->title_tag }}"/>
                                    </p>
                                    <div class="form-action clearfix">
                                        <p>
                                            <a href="{{ route('slideshows.delete-slide', $slide->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete Slide</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <dl>
                                        <dt>Title</dt>
                                        <dd>{{ $slide->title }}</dd>
                                        <dt>Caption</dt>
                                        <dd>{{ $slide->caption }}</dd>
                                        <dt>Link</dt>
                                        <dd>{{ $slide->link ? $slide->link : 'N/A' }}</dd>
                                        <dt>Link Text</dt>
                                        <dd>{{ $slide->link_text ? $slide->link_text : 'N/A' }}</dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        @endif
    </ul>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('slideshows.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit Slideshow')
                <a href="{{ route('slideshows.add-slides',$slideshow->id) }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Slides</a>
            @endcan
            @can('Edit Slideshow')
                <a href="{{ route('slideshows.edit', $slideshow->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @endcan
            @can('Delete Slideshow')
                <a href="{{ route('slideshows.delete', $slideshow->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan
        </div>
    </div>
@stop
@section('scripts')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#sortable").sortable({
                update: function () {
                    $.map($(this).find('li'), function(el) {
                        var itemID = el.id;
                        var itemIndex = $(el).index();

                        $.ajax({
                            url: '{{ route('slideshows.sort-slides') }}',
                            type: 'POST',
                            data: {itemID: itemID, itemIndex: itemIndex},
                            dataType: 'json',
                            success: function (data) {
                                console.log(data)
                            },
                            error: function (err) {
                                console.log('Error!', err)
                            },
                        })
                    })
                }
            });
        });
    </script>
@endsection