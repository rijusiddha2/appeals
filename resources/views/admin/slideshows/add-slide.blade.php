@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::open(['route' => 'slideshows.store-slides', 'enctype' => 'multipart/form-data'])  }}

    <div class="box">
        <div class="box-header">
            Add Slide
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <div id="image0">
                        <div class="placeholder">
                            <button
                                    type="button"
                                    class="btn btn-primary"
                                    data-toggle="modal"
                                    data-image="image0"
                                    data-target="#imageModal0">
                                Select Image
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        {{ Form::hidden('slideshow_id',$slideshow->id ) }}
                        {{ Form::label('title', 'Title', ['class' => 'control-label']) }}
                        {{ Form::text('title', old('title'), ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('caption', 'Caption', ['class' => 'control-label']) }}
                        {{ Form::text('caption', old('caption'), ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('link', 'Link', ['class' => 'control-label']) }}
                        {{ Form::text('link', old('link'), ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('link_text', 'Link Text', ['class' => 'control-label']) }}
                        {{ Form::text('link_text', old('link_text'), ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>
            <div class="form-action clearfix">
                <div class="pull-right">
                    <button type="submit" class="btn btn-success pull-right"><i class="fas fa-check"></i> Add Slide</button>
                </div>
            </div>

        </div>
    </div>
    {{ Form::close() }}
@include('admin.images.partials.add-image-modal')
    @endsection

@section('scripts')
    <script>
        $(function() {
            $('#imageModal0').on("show.bs.modal", function (e) {
                var image = $(e.relatedTarget).data('image');
                $('.image').click(function(){
                    $('#' + image + '').html('<input type="hidden" name="image_id" value="' + $(this).data('id') + '"/><img src="' + $(this).attr('src') + '" alt="' +
                        $(this).attr('alt')+ '" class="img-responsive"/>');
                    $('#imageModal0').modal('hide');
                });

            });
        });
    </script>
@endsection