@extends('admin.layouts.dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            @can('Add Slideshow')
                <div class="box-buttons">
                    <a href="{{ route('slideshows.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
                </div>
            @endcan

            @if (isset($title))
                {{ $title  }}
            @else
                Slideshows
            @endif

        </div>

        <div class="box-content">
            <table class="table table-bordered" id="slideshow-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Slides</th>
                    <th>Created By</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    @can('Add Slideshow')
        <a class="btn btn-success pull-right" href="{{ route('slideshows.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Slideshow</a>
    @endcan

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#slideshow-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('slideshows.data')  }}'
                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'slides', name: 'slides' },
                    { data: 'created_by', name: 'created_by' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop