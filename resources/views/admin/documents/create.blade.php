@extends('admin.layouts.dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            {{ $title  }}
        </div>
        <div class="box-content">
            {{ Form::open(['url' => 'admin/documents', 'enctype' => 'multipart/form-data']) }}

            <div class="form-group">
                {{ Form::hidden('documentable_id', $application->id) }}
                {{ Form::file('document') }}
            </div>
            <div class="form-group">
                {{ Form::label('documentable_type', 'Type') }}
                {{ Form::select('documentable_type', ['Application' => 'Application', 'Job' => 'Job', 'JobApplication' => 'Job Application' ], old('document_type'), ['class' => 'form-control selectpicker']) }}
            </div>

        </div>
    </div>
    <div class="form-action clearfix">
        <div class="float-right">
            <button type="submit" class="btn btn-success"> <span data-feather="plus"></span> Add Document</button>
            {{ Form::close() }}
        </div>
    </div>

@stop
