@extends('admin.layouts.dashboard')
@section('content')
    {{ Form::open(['url' => 'admin/permissions']) }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    New Permission
                </div>

                <div class="box-content">

                    <div class="form-group">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', '',['class' => 'form-control']) }}
                    </div><br>
                    @if(!$roles->isEmpty())
                        <h4>Assign Permission to Roles</h4>

                        @foreach ($roles as $role)
                            {{ Form::checkbox('roles[]',  $role->id ) }}
                            {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                        @endforeach
                    @endif
                </div>
            </div>
            <div class="form-action clearfix">
                <div class="pull-right">
                    <a href="{{ route('permissions.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
                    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Create</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

@endsection