@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            {{ $title  }}
                        </div>

                        <div class="box-content">
                            <dl class="dl-horizontal">
                                <dt>Name</dt>
                                <dd>{{ $permission->name }}</dd>

                                <dt>Created at</dt>
                                <dd>{{ $permission->created_at->format('d/m/Y h:i') }}</dd>

                            </dl>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('permissions.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit Permissions')
                <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Edit</a>
            @endcan
            @can('Delete Permissions')
                <a href="{{ route('permissions.delete', $permission->id) }}" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan

        </div>
    </div>
@stop