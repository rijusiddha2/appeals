{{-- \resources\views\permissions\index.blade.php --}}
@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            @can('Add Permission')
                <div class="box-buttons">
                    <a href="{{ route('permissions.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
                </div>
            @endcan
            Permissions

        </div>

        <div class="box-content">
            <div class="table-responsive">
            <table class="table table-bordered table-striped" id="permissions-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Permissions</th>
                    <th>Created At</th>
                    <th></th>
                </tr>
                </thead>

            </table>
        </div>
        </div>
    </div>
    @can('Add Permission')
        <a class="btn btn-success pull-right" href="{{ route('permissions.create') }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Permission</a>
    @endcan


    </div>

@endsection


@section('scripts')

    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $(function() {
            $.noConflict(true)
            $('#permissions-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('permissions.data')  }}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'created_at', name: 'created_at' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],

            });

        });

    </script>
@stop