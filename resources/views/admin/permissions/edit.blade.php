@extends('admin.layouts.dashboard')
@section('content')
    {{ Form::model($permission, ['route' => ['permissions.update', $permission->id], 'method' => 'PUT']) }}{{-- Form model binding to automatically populate our fields with permission data --}}

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    {{ $title  }}
                </div>

                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('name', 'Permission Name') }}
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </div>

                </div>
            </div>
            <div class="form-action clearfix">
                <div class="pull-right">
                    <a href="{{ route('permissions.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Save</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

@endsection