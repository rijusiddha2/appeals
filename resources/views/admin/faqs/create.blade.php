@extends('admin.layouts.dashboard')
@section('content')
    {{ Form::open(['url' => 'admin/faqs']) }}
        <div class="box">
            <div class="box-header">
                New FAQ
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('category', 'Category') }}
                            {{ Form::select('category', $categories, old('category'), ['class' => 'form-control selectpicker']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('question', 'Question') }}
                            {{ Form::text('question', old('question'), ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('answer', 'Answer') }}
                            {{ Form::textarea('answer', old('answer'), ['class' => 'form-control editor']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-action clearfix">
            <div class="pull-right">
                <a href="{{ route('faqs.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
                <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Create</button>
            </div>
        </div>
    {{ Form::close() }}

@endsection