@extends('admin.layouts.dashboard')
@section('content')
    {{ Form::model($faq, ['route' => ['faqs.update', $faq->id], 'method' => 'PUT']) }}
        <div class="box">
            <div class="box-header">
                {{ $title }}
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('category', 'Category') }}
                            {{ Form::select('category', $categories, $faq->category, ['class' => 'form-control selectpicker']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('question', 'Question') }}
                            {{ Form::text('question', $faq->question, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('answer', 'Answer') }}
                            {{ Form::textarea('answer', $faq->answer, ['class' => 'form-control editor']) }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="form-action clearfix">
            <div class="pull-right">
                <a href="{{ route('faqs.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
                <button type="submit" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Update</button>
            </div>
        </div>
    {{ Form::close() }}
@endsection