@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    {{ $title  }}
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Question</dt>
                        <dd>{{ $faq->question }}</dd>

                        <dt>Answer</dt>
                        <dd>{{ $faq->answer }}</dd>

                        <dt>Category</dt>
                        <dd>{{ $faq->category }}</dd>

                        <dt>Created at</dt>
                        <dd>{{ $faq->created_at->format('d/m/Y') }}</dd>
                    </dl>
                </div>
            </div>
        </div>

    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('faqs.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            @can('Edit FAQ')
                <a href="{{ route('faqs.edit', $faq->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Edit</a>
            @endcan
            @can('Delete FAQ')
                <a href="{{ route('faqs.delete', $faq->id) }}" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan
        </div>
    </div>
@stop
