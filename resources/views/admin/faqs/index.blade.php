@extends('admin.layouts.dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            @can('Add FAQ')
                <div class="box-buttons">
                    <a href="{{ route('faqs.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
                </div>
            @endcan
            FAQ's
        </div>

        <div class="box-content">
            <table class="table table-bordered" id="faq-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Category</th>
                    <th>Created At</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    @can('Add FAQ')
        <a class="btn btn-success pull-right" href="{{ route('faqs.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add FAQ</a>
    @endcan

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#faq-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('faqs.data')  }}',

                },
                order: [[ 0, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'question', name: 'question' },
                    { data: 'answer', name: 'answer' },
                    { data: 'category', name: 'category' },
                    { data: 'created_at', name: 'created_at' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],

            });

        });
    </script>
@stop