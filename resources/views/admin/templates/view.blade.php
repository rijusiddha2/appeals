@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Template Details
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $template->name }}</dd>

                        <dt>Created at</dt>
                        <dd>{{ $template->created_at->format('d/m/Y h:i') }}</dd>

                    </dl>
                </div>
            </div>
        </div>
    </div>
{{ Form::open(['id' => 'template']) }}
    <div class="alert alert-info alert-block" role="alert">
        Content blocks below can be re-ordered by dragging and dropping
    </div>
    <ul id="sortable">
    @if (isset($template->contents))
        @foreach($templateContents as $contentBlock)
            <li id="{{ $contentBlock->content_template_id }}">
            <div class="box">
                <div class="box-content">
                    <div id="content-blocks">
                        @if($contentBlock->editable == true)
                            <div class="row">
                                @foreach($contentBlock->blocks as $block)
                                    <div class="col-md-{{ $block->pivot->column_width }}">

                                        @include('admin.blocks.partials.'.$block->partial)
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-md-2 col-md-offset-10">
                                    <a href="{{ route('templates.remove-block', $contentBlock->content_template_id) }}" title="delete" class="btn btn-danger btn-template"><i class="fas fa-trash"></i>
                                        Remove Content Block</a>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    @include('admin.blocks.partials._uneditable')
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 col-md-offset-10">
                                    <a href="{{ route('templates.remove-block', $contentBlock->content_template_id) }}" title="delete" class="btn btn-danger btn-template"><i class="fas fa-trash"></i>
                                        Remove Content
                                        Block</a>

                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            </li>
        @endforeach
    @endif
    </ul>
    {{ Form::close() }}

    {{ Form::open(['r']) }}
    <div class="box">
        <div class="box-header">
            Add Content Block
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::select('content_id', $contents, null, ['class' => 'form-control selectpicker', 'id' => 'content-block'])  }}
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-6">
                    <div class="pull-right">
                        <a href="" class="btn btn-success pull-right" id="add-block"><i class="fas fa-check"></i> Add Content Block</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('templates.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <a href="{{ route('templates.edit', $template->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            <a href="{{ route('templates.destroy', $template->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
        </div>
    </div>
@stop
@section('scripts')

    <script>

        $(document).ready(function(){

            $("#sortable").sortable({
                update: function () {
                    $.map($(this).find('li'), function(el) {
                        var itemID = el.id;
                        var itemIndex = $(el).index();

                        $.ajax({
                            url: '{{ route('templates.sort-content') }}',
                            type: 'POST',
                            data: {itemID: itemID, itemIndex: itemIndex},
                            dataType: 'json',
                            success: function (data) {
                                console.log(data)

                            },
                            error: function (err) {
                                console.log('Error!', err)
                            },
                        })
                    })
                }
            });


            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') }
            });
            $('#add-block').click(function(e){
                e.preventDefault();
                var block = $('#content-block').val();

                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{ URL::route('templates.add-content-block') }}',
                    type: 'POST',
                    data: {'content_id': block, 'template': {{ $template->id }}},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (response, block) {
                        console.log(response,block);
                        $('#sortable').append(response.html);
                        $('#content-blocks').append('<div class="row"><div class="col-md-2 col-md-offset-10"><a href="templates/1/remove" title="delete" class="btn btn-danger btn-template"><i class="fas fa-trash"></i>Remove Content Block</a></div></div>')

                    }
                });
            });
        });
    </script>
@endsection