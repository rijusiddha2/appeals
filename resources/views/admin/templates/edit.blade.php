@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($template, ['route' => ['templates.update', $template->id], 'method' => 'PUT'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Template Details
                </div>
                <div class="box-content">


                    <div class="form-group">
                        {{ Form::label('name', 'Name')  }}
                        {{ Form::text('name', $template->name, ['class' => 'form-control'])  }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <button type="submit" class="btn btn-success pull-right"><i class="fas fa-check"></i> Update</button>
        </div>
    </div>
    {{ Form::close()  }}

@endsection
