@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            <div class="box-buttons">
                <a href="{{ route('templates.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
            </div>

            @if (isset($title))
                {{ $title  }}
            @else
                Templates
            @endif

        </div>

        <div class="box-content">
            <table class="table table-bordered" id="templates-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Created By</th>
                    <th>Created On</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <a class="btn btn-success pull-right" href="{{ route('templates.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Template</a>

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#templates-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('templates.data')  }}',
                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'created_by', name: 'created_by' },
                    { data: 'created_at', name: 'created_at' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],

            });

        });

    </script>
@stop