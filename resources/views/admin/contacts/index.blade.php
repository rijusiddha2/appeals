@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
             Emails from Contact Form
        </div>
        <div class="box-content">
            <table class="table table-bordered" id="contacts-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Subject</th>
                    <th>Sent</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

@stop

@section('scripts')

    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#contacts-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('contacts.data')  }}',

                },
                order: [[ 4, "desc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'subject', name: 'subject' },
                    { data: 'created_at', name: 'created_at' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],

            });


        });
    </script>
@stop