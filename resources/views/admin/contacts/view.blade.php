@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Email
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $contact->first_name }} {{ $contact->last_name }}</dd>

                        <dt>Email</dt>
                        <dd>{{ $contact->email }} </dd>

                        <dt>Subject</dt>
                        <dd>{{ $contact->subject }}</dd>

                        <dt>Message</dt>
                        <dd>{{ $contact->message }}</dd>

                        <dt>Sent</dt>
                        <dd>{{ $contact->created_at->format('d/m/Y h:i') }}</dd>

                        <dt>IP Address</dt>
                        <dd>{{ $contact->ip_address }}</dd>
                    </dl>
                </div>
            </div>
            <div class="form-action clearfix">
                <div class="pull-right">
                    <a href="{{ route('contacts.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
                </div>
            </div>
        </div>
    </div>

@endsection