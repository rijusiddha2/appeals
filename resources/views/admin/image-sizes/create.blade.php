@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::open(['url' => 'admin/image-sizes'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Image Size
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('size', 'Size')  }}
                        {{ Form::text('size', old('size'), ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('width', 'Width')  }}
                        {{ Form::text('width', old('width'), ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('height', 'Height')  }}
                        {{ Form::text('height', old('height'), ['class' => 'form-control'])  }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('image-sizes.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Create</button>
            {{ Form::close()  }}
        </div>
    </div>


@stop
