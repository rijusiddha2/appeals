@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($image, ['route' => ['image-sizes.update', $image->id], 'method' => 'PUT'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Image Size
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('size', 'Size')  }}
                        {{ Form::text('size', $imageSize->size, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('width', 'Width')  }}
                        {{ Form::text('width', $imageSize->width, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('height', 'Height')  }}
                        {{ Form::text('height', $imageSize->height,['class' => 'form-control'])  }}
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <button type="submit" class="btn btn-success pull-right"><i class="fas fa-check"></i> Update</button>
            {{ Form::close()  }}
        </div>
    </div>

@stop
