@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Image Sizes Details
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Size</dt>
                        <dd>{{ $imageSize->size }}</dd>

                        <dt>Width</dt>
                        <dd>{{ $imageSize->width }}</dd>

                        <dt>Height</dt>
                        <dd>{{ $imageSize->height }}</dd>
                    </dl>
                </div>
            </div>
        </div>

    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('image-sizes.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <a href="{{ route('image-sizes.edit', $imageSize->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            <a href="{{ route('image-sizes.delete, $imageSize->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
        </div>
    </div>
@stop