@extends('admin.layouts.dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            @can('Add Image Size')
                <div class="box-buttons">
                    <a href="{{ route('image-sizes.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
                </div>
            @endcan

            @if (isset($title))
                {{ $title  }}
            @else
                Image Sizes
            @endif

        </div>

        <div class="box-content">
            <table class="table table-bordered" id="image-sizes-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Size</th>
                    <th>Width</th>
                    <th>Height</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($imageSizes as $imageSize)
                        <tr>
                            <td>{{ $imageSize->id }}</td>
                            <td>{{ $imageSize->size }}</td>
                            <td>{{ $imageSize->width }}</td>
                            <td>{{ $imageSize->height }}</td>
                            <td>
                                <a data-toggle="tooltip" data-original-title="View" href="{{ route('image-sizes.show', $imageSize->id) }}" class="tooltip-pivot pull-left"><i class="far fa-eye"></i></a>
                                @can('Delete Image Size')
                                    <a data-toggle="tooltip" data-original-title="Delete" href="{{ route('image-sizes.delete', $imageSize->id) }}" class="tooltip-pivot pull-left"><i class="fas fa-trash"></i></a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @can('Add Image size')
        <a class="btn btn-success pull-right" href="{{ route('image-sizes.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Image Size</a>
    @endcan

@stop
