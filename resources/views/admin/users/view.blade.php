@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    User Details
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $user->name }}</dd>

                        <dt>Email</dt>
                        <dd>{{ $user->email }}</dd>

                        <dt>Created at</dt>
                        <dd>{{ $user->created_at->format('d/m/Y h:i') }}</dd>

                        <dt>Roles</dt>
                        <dd>
                            <ul>
                                @foreach($user->getRoleNames() as $role)
                                    <li>{{ $role }}</li>
                                    @endforeach
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('users.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit User')
                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @endcan
            {{--<a href="{{ route('password.request') }}" class="btn btn-primary"><i class="fas fa-key"></i> Change Password</a>--}}
            @can('Delete User')
                <a href="{{ route('users.delete', $user->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan
        </div>
    </div>
@stop