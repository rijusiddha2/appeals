@extends('admin.layouts.dashboard')

@section('content')
    @if(isset($user))
        {{ Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT'])  }}
    @else
        {{ Form::open(['url' => 'users'])  }}
    @endif
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    User Details
                </div>
                <div class="box-content">

                    <div class="form-group">
                        {{ Form::label('name', 'Name')  }}
                        {{ Form::text('name', isset($user->name) ? $user->name : old('name'), ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('email', 'Email')  }}
                        {{ Form::email('email', isset($user->email) ? $user->email : old('email'), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-groups">
                        {{ Form::label('roles', 'Roles')  }}
                        {{ Form::select('roles[]', $roles, old('roles', isset($user->roles) ? $user->roles->pluck('id')->toArray() : ''), ['class' => 'form-control selectpicker', 'multiple'])  }}
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('users.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <button type="submit" class="btn btn-success "><i class="fas fa-edit"></i> Save</button>
        </div>
    </div>

    {{ Form::close()  }}
@stop