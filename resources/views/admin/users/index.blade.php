@extends('admin.layouts.dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            @can('Add User')
                <div class="box-buttons">
                    <a href="{{ route('users.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
                </div>
            @endcan
            Users
        </div>

        <div class="box-content">
            <table class="table table-bordered" id="users-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Created at</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                   @foreach($users as $user)
                       <tr>
                           <td>{{ $user->id }}</td>
                           <td>{{ $user->name }}</td>
                           <td>{{ $user->email }}</td>
                           <td>{{ $user->created_at->format('d/m/Y') }}</td>
                           <td>
                               <a data-toggle="tooltip" data-original-title="View" href="{{ route('users.show', $user->id) }}" class="tooltip-pivot pull-left"><i class="far fa-eye"></i></a>
                               @can('Edit User')
                                    <a data-toggle="tooltip" data-original-title="Edit" href="{{ route('users.edit', $user->id) }}" class="tooltip-pivot float-left"><i class="fas fa-pencil-alt "></i></a>
                               @endcan
                               @can('Delete User')
                                    <a data-toggle="tooltip" data-original-title="Delete" href="{{ route('users.delete', $user->id) }}" class="tooltip-pivot float-left"><i class="fas fa-trash"></i></a>
                               @endcan
                           </td>
                       </tr>
                   @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @can('Add User')
        <a class="btn btn-success pull-right" href="{{ route('users.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add User</a>
    @endcan
@stop