@extends('admin.layouts.dashboard')
@section('content')
    {{ Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'PUT']) }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    {{ $title  }}
                </div>

                <div class="box-content">

                    <div class="form-group">
                        {{ Form::label('name', 'Role Name') }}
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </div>

                    <h5><b>Assign Permissions</b></h5>
                    @foreach ($permissions as $permission)

                        {{ Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
                        {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>

                    @endforeach

                </div>
            </div>
            <div class="form-action clearfix">
                <div class="pull-right">
                    <a href="{{ route('roles.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Save</button>
                </div>
            </div>


            {{ Form::close() }}
        </div>
    </div>

@endsection