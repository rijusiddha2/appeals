@extends('admin.layouts.dashboard')
@section('content')
    {{ Form::open(['url' => 'admin/roles']) }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">

                <div class="box-header">
                    New Role
                </div>
                <div class="box-content">

                    <div class="form-group">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </div>

                    <h5><b>Assign Permissions</b></h5>

                    <div class='form-group'>
                        @foreach ($permissions as $permission)
                            {{ Form::checkbox('permissions[]',  $permission->id ) }}
                            {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>

                        @endforeach
                    </div>
                </div>
            </div>
            <div class="form-action clearfix">
                <div class="pull-right">
                    <a href="{{ route('roles.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
                    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Create</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection