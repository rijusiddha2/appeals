@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            @can('Add Role')
                <div class="box-buttons">
                    <a href="{{ route('roles.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
                </div>
            @endcan
            Roles

        </div>

        <div class="box-content">
            <div class="table-responsive">
            <table class="table table-bordered table-striped" id="roles-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Role</th>
                    <th>Created At</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        </div>
    </div>
    @can('Add Role')
        <a class="btn btn-success pull-right" href="{{ route('roles.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Add Role</a>
    @endcan
    </div>

@endsection

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            $('#roles-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('roles.data')  }}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'created_at', name: 'created_at' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],

            });

        });

    </script>
@stop