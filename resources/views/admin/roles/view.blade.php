@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    {{ $title  }}
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $role->name }}</dd>

                        <dt>Created at</dt>
                        <dd>{{ $role->created_at->format('d/m/Y h:i') }}</dd>
                    </dl>
                </div>
            </div>
        </div>
        @if ($role->permissions)
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        Permissions
                    </div>
                    <div class="box-content">
                        @if(isset($role->permissions))
                            <ul>
                                @foreach ($role->permissions as $permission)
                                    <li>{{ $permission->name  }}</li>
                                @endforeach
                            </ul>
                        @else
                            <p>No permissions found for this Role.</p>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('roles.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            @can('Edit Role')
                <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Edit</a>
            @endcan
            @can('Delete Role')
                <a href="{{ route('roles.delete', $role->id) }}" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan
        </div>
    </div>
@stop
