@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            <div class="box-buttons">
                <a href="{{ route('jobs-admin.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
            </div>

            @if (isset($title))
                {{ $title  }}
            @else
                Jobs
            @endif

        </div>
        <div class="box-content">
            <table class="table table-bordered" id="jobs-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Caption</th>
                    <th>Closing Date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <a class="btn btn-success pull-right" href="{{ route('jobs-admin.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Job</a>

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#jobs-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('jobs-admin.data')  }}'
                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'caption', name: 'caption' },
                    { data: 'closing_date', name: 'closing_date' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop