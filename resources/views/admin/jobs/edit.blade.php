@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($job, ['route' => ['jobs-admin.update', $job->id], 'method' => 'PUT'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Job Details
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('title', 'Title')  }}
                        {{ Form::text('title', $job->title, ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('caption', 'Caption')  }}
                        {{ Form::text('caption', $job->caption, ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('closing_date', 'Closing Date')  }}
                        {{ Form::text('closing_date', $job->closing_date->format('d/m/Y'), ['class' => 'form-control datepicker'])  }}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('description', 'Description')  }}
                        {{ Form::textarea('description', $job->description, ['class' => 'form-control editor'])  }}
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('jobs-admin.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Update</button>
        </div>
    </div>

    {{ Form::close()  }}

@endsection
