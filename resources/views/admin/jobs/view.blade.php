@extends('admin.layouts.dashboard')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Job Details
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Title</dt>
                        <dd>{{ $job->title }}</dd>
                        <dt>Caption</dt>
                        <dd>{{ $job->caption }}</dd>
                        <dt>Closing Date</dt>
                        <dd>{{ !empty($job->closing_date) ? $job->closing_date->format('d/m/Y') : 'No end date'}}</dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Job Description
                </div>
                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Description</dt>
                        <dd>{!! $job->description !!}</dd>

                    </dl>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    Job Description Document
                </div>

                <div class="box-content">
                    <embed src="{{ asset('/images/'.$job->documents->first()->filename)  }}" width="100%" height="500px">
                </div>
            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('jobs-admin.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit Job')
                <a href="{{ route('jobs-admin.edit', $job->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @endcan
            @can('Delete Job')
                <a href="{{ route('jobs-admin.delete', $job->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan
        </div>
    </div>
@stop