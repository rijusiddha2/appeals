@extends('admin.layouts.dashboard')

@section('content')

    <ul class="nav nav-tabs" id="application" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">Employment History</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="qualifications-tab" data-toggle="tab" href="#qualifications" role="tab" aria-controls="qualifications" aria-selected="false">Qualifications</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="further-tab" data-toggle="tab" href="#further" role="tab" aria-controls="further" aria-selected="false">Further Details</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="safeguarding-tab" data-toggle="tab" href="#safeguarding" role="tab" aria-controls="safeguarding" aria-selected="false">Safeguarding</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="references-tab" data-toggle="tab" href="#references" role="tab" aria-controls="references" aria-selected="false">References</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="documents-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="documents" aria-selected="false">Documents</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="notes-tab" data-toggle="tab" href="#notes" role="tab" aria-controls="notes" aria-selected="false">Notes</a>
        </li>
    </ul>

    <div class="tab-content" id="applicationContent">

        @include('admin.jobs.applications.partials._details')
        @include('admin.jobs.applications.partials._history')
        @include('admin.jobs.applications.partials._qualifications')
        @include('admin.jobs.applications.partials._further')
        @include('admin.jobs.applications.partials._safeguarding')
        @include('admin.jobs.applications.partials._references')
        @include('admin.jobs.applications.partials._documents')
        @include('admin.jobs.applications.partials._notes')
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('job-applications.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit Job Application')
                <a href="{{ route('job-applications.edit', $jobApplication->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>  Edit</a>
            @endcan
            @can('Add Job Application Note')
                <a href="{{ route('job-applications.note', $jobApplication->id) }}" class="btn btn-success"><i class="fas fa-paperclip"></i>  Add Note</a>
            @endcan
            <a href="{{route('job-applications.pdf', $jobApplication->id)}}" class="btn btn-secondary"><i class="fas fa-download"></i> Download and Print as PDF</a>
            @can('Add Document')
                <a href="{{ route('documents.create', ['model' => 'App\JobApplication', 'application_id' => $jobApplication->id]) }}" class="btn btn-success"><i class="fas fa-file"></i>  Add Document</a>
            @endcan
            @can('Delete Job Application')
                <a href="{{ route('job-applications.delete', $jobApplication->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i>  Delete</a>
            @endcan
        </div>
    </div>
@stop
