<div id="details" class="tab-pane fade active in" role="tabpanel" aria-labelledby="details-tab">
    <div class="box">

        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $jobApplication->title  }} {{ $jobApplication->first_name }} {{ $jobApplication->last_name }}</dd>
                        <dt>Order Name</dt>
                        <dd>{{ $jobApplication->order_name }}</dd>
                        <dt>Address</dt>
                        <dd>{{ $jobApplication->address  }} <br/> {{ $jobApplication->postcode }}</dd>
                    </dl>
                </div>

                <div class="col-md-6">
                    <dl class="dl-horizontal">
                        <dt>Email</dt>
                        <dd>{{ $jobApplication->email  }}</dd>
                        <dt>Mobile</dt>
                        <dd>{{ $jobApplication->mobile  }}</dd>
                        <dt>Telephone (Alt)</dt>
                        <dd>{{ $jobApplication->alt_tel }}</dd>
                        <dt>Work Permit</dt>
                        <dd>{{ $jobApplication->work_permit == 1 ? 'Yes' : 'No' }}</dd>
                        <dt>Driving Licence</dt>
                        <dd>{{ $jobApplication->driving_licence == 1 ? 'Yes' : 'No' }}</dd>
                        <dt>Created By</dt>
                        <dd>{{ $jobApplication->created_by != 99 ? $jobApplication->author->name : 'Web'  }}</dd>

                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
