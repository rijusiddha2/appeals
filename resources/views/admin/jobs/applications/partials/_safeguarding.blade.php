<div id="safeguarding" class="tab-pane fade" role="tabpanel" aria-labelledby="safeguarding-tab">
    <div class="box">

        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <dl>
                        <dt>Disciplinary investigations towards children and/or vulnerable adults</dt>
                        <dd>{{ count($jobApplication->safeguarding) && $jobApplication->safeguarding->vunareable_sanctions ? 'Yes' : 'No'}}</dd>
                        <dt>Criminal Offences</dt>
                        <dd>{{ count($jobApplication->safeguarding) && $jobApplication->safeguarding->criminal_offences == 1 ? 'Yes' : 'No'  }}</dd>
                    </dl>
                </div>

                <div class="col-md-6">
                    <dl >
                        <dt>Safeguarding details</dt>
                        <dd>{{ count($jobApplication->safeguarding) ? $jobApplication->safeguarding->safeguarding_details : ''}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>