<div id="further" class="tab-pane fade active" role="tabpanel" aria-labelledby="further-tab">
    <div class="box">

        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <dl class="dl-horizontal">
                        <dt>Skills</dt>
                        <dd>{{ count($jobApplication->further) ? $jobApplication->further->skills : ''}}</dd>

                    </dl>
                </div>

                <div class="col-md-6">
                    <dl class="dl-horizontal">
                        <dt>Disabilites</dt>
                        <dd>{{ count($jobApplication->further) ? $jobApplication->further->disabilities : '' }}</dd>

                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>