<div id="history" class="tab-pane fade" role="tabpanel" aria-labelledby="history-tab">
    <div class="box">
        <div class="box-content">
            <div class="row">
                @if(isset($jobApplication->currentHistory))
                    <div class="col-md-6">
                        <p><strong>Current / Recent employment history</strong></p>
                        <hr/>
                        <dl>
                            <dt>Job Title</dt>
                            <dd>{{ $jobApplication->currentHistory->job_title }}</dd>
                            <dt>Details of Previous Employer</dt>
                            <dd>{{ $jobApplication->currentHistory->employer_details }}</dd>
                            <dt>Employed From</dt>
                            <dd>{{ $jobApplication->currentHistory->employed_from->format('d/m/Y') }}</dd>
                            <dt>Employed To</dt>
                            <dd>{{ $jobApplication->currentHistory->employed_to->format('d/m/Y') }}</dd>
                            <dt>Responsibilities</dt>
                            <dd>{{ $jobApplication->currentHistory->responsibilities }}</dd>
                        </dl>
                    </div>
                    <div class="col-md-6">
                        <p><strong>Previous employment history</strong></p>
                        <hr/>
                        <dl>
                            <dt>Job Title</dt>
                            <dd>{{ $jobApplication->previousHistory->job_title }}</dd>
                            <dt>Details of Previous Employer</dt>
                            <dd>{{ $jobApplication->previousHistory->employer_details }}</dd>
                            <dt>Employed From</dt>
                            <dd>{{ $jobApplication->previousHistory->employed_from->format('d/m/Y') }}</dd>
                            <dt>Employed To</dt>
                            <dd>{{ $jobApplication->previousHistory->employed_to->format('d/m/Y') }}</dd>
                            <dt>Responsibilities</dt>
                            <dd>{{ $jobApplication->previousHistory->responsibilities }}</dd>
                        </dl>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                            No referee data recorded!
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>