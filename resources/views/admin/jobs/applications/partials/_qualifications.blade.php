<div id="qualifications" class="tab-pane fade" role="tabpanel" aria-labelledby="qualifications-tab">
    <div class="box">

        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <dl>
                        <dt>Professional Qualifications</dt>
                        <dd>{{ count($jobApplication->qualifications) ? $jobApplication->qualifications->professional_qualifications : ''}}</dd>

                    </dl>
                </div>

                <div class="col-md-6">
                    <dl>
                        <dt>Educational Qualifications</dt>
                        <dd>{{ count($jobApplication->qualifications) ? $jobApplication->qualifications->educational_qualifications  : '' }}</dd>

                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>