@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            <div class="box-buttons">
                <a href="{{ route('job-applications.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
            </div>

            @if (isset($title))
                {{ $title  }}
            @else
                Job Applications
            @endif

        </div>
        <div class="box-content">
            @if(isset($job))
                {{ Form::hidden('job_id', $job, ['id' => 'job-id']) }}
            @endif
            <table class="table table-bordered" id="job-application-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Job</th>
                    <th>Name</th>
                    <th>Applied</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <a class="btn btn-success pull-right" href="{{ route('job-applications.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Job Application</a>

@stop

@section('scripts')
    <script>

        $(function() {
            $.noConflict(true)
            var oTable = $('#job-application-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('job-applications.data')  }}',
                    data: function (d) {
                        d.job_id = $('#job-id').val();
                    }
                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'job', name: 'job' },
                    { data: 'name', name: 'name' },
                    { data: 'created_at', name: 'created_at' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop