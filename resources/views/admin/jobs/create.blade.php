@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::open(['url' => 'admin/jobs-admin', 'enctype' => 'multipart/form-data', 'files' => true,])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Create New Job
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('title', 'Title')  }}
                        {{ Form::text('title', old('title'), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('caption', 'Caption')  }}
                        {{ Form::text('caption', old('caption'), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('closing_date', 'Closing Date')  }}
                        {{ Form::text('closing_date', old('closing_date'), ['class' => 'form-control datepicker'])  }}
                    </div>
                    <div class="alert-info">Only PDF versions can be uploaded</div>
                    <div class="form-group">
                        {{ Form::label('job_document', 'Job Document')  }}
                        {{ Form::file('job_document')  }}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('description', 'Description')  }}
                        {{ Form::textarea('description', old('description'), ['class' => 'form-control editor'])  }}
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('jobs-admin.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Create</button>
        </div>
    </div>

    {{ Form::close()  }}

@endsection
