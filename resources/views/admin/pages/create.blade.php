@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::open(['url' => 'admin/pages'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Page Details
                </div>
                <div class="box-content">

                    <div class="form-group">
                        {{ Form::label('title', 'Title')  }}
                        {{ Form::text('title', old('title'), ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('url', 'URL')  }}
                        {{ Form::text('url', old('url'), ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('published_date', 'Publish Date')  }}
                        {{ Form::text('published_date', old('published_date'),['class' => 'form-control datepicker'])  }}

                    </div>

                    <div class="form-group">
                        {{ Form::label('status', 'Status')  }}
                        {{ Form::select('status',['draft' => 'Draft', 'published' => 'publish'], old('status'), ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('template_id', 'Template')  }}
                        {{ Form::select('template_id', $templates, old('template_id'), ['class' => 'form-control'])  }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                   Page Excerpt
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('excerpt', 'Excerpt')  }}
                        {{ Form::textarea('excerpt', old('excerpt'), ['class' => 'form-control'])  }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    Page Meta
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('meta_keywords', 'Meta Keywords')  }}
                                {{ Form::text('meta_keywords', old('meta_keywords'),['class' => 'form-control'])  }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('meta_description', 'Meta Description')  }}
                                {{ Form::textarea('meta_description', old('meta_description'), ['class' => 'form-control'])  }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('og_title', 'og:title')  }}
                                {{ Form::text('og_title', old('og_title'),['class' => 'form-control'])  }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('og_description', 'og:escription')  }}
                                {{ Form::textarea('og_description', old('meta_description'), ['class' => 'form-control'])  }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('og_url', 'og:url')  }}
                                {{ Form::text('og_url', old('og_url'),['class' => 'form-control'])  }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('og_image', 'og:image')  }}
                                {{ Form::text('og_image', old('og_image'),['class' => 'form-control'])  }}
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <button type="submit" class="btn btn-success pull-right"><i class="fas fa-check"></i> Create</button>
            {{ Form::close()  }}
        </div>
    </div>
    @include('admin.images.partials.add-image-modal')

@endsection
@section('scripts');
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<script>
    $(function() {
        $('#imageModal0').on("show.bs.modal", function (e) {
            var image = $(e.relatedTarget).data('image');
            $('.image').click(function(){
                $('#' + image + '').html('<input type="hidden" name="image_id" value="' + $(this).data('id') + '"/><img src="' + $(this).attr('src') + '" alt="' +
                    $(this).attr('alt')+ '" class="img-responsive"/>');
                $('#imageModal0').modal('hide');
            });
        });

    });
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
</script>
    @stop
