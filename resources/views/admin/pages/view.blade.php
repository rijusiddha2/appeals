@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    User Details
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Title</dt>
                        <dd>{{ $page->title }}</dd>

                        <dt>URL</dt>
                        <dd>{{ $page->url }}</dd>


                        <dt>Status</dt>
                        <dd>{{ $page->status }}</dd>

                        <dt>Template</dt>
                        <dd>{{ $page->template->name }}</dd>

                    </dl>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                   Excerpt
                </div>
                <div class="box-content">
                    {{ $page->excerpt }}
                </div>
            </div>
        </div>
    </div>
    @if(isset($page->meta))
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Meta
                </div>
                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Meta Keywords</dt>
                        <dd>{{ $page->meta->meta_keywords }}</dd>

                        <dt>Meta Descriptions</dt>
                        <dd>{{ $page->meta->meta_description }}</dd>

                        <dt>og:title</dt>
                        <dd>{{ $page->meta->og_title }}</dd>

                        <dt>og:description</dt>
                        <dd>{{ $page->meta->og_description }}</dd>

                        <dt>og:url</dt>
                        <dd>{{ $page->meta->og_url }}</dd>

                        <dt>og:image</dt>
                        <dd>{{ $page->meta->og_image }}</dd>

                    </dl>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('pages.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @if(!empty($content))
                <a href="{{ route('pages.edit-content', $page->id) }}" class="btn btn-primary"><i class="fas fa-columns"></i> Edit Content</a>
            @else
                <a href="{{ route('pages.add-content', $page->id) }}" class="btn btn-primary"><i class="fas fa-columns"></i> Add Content</a>
            @endif
            <a href="{{ route('pages.destroy', $page->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
        </div>
    </div>
@stop