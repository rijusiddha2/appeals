@extends('admin.layouts.dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            {{ $page->name }} Content
        </div>
    </div>
    <?php $i = 0; ?>
    @foreach($contentBlocks as $content)

        {{ Form::open() }}

        {{ Form::hidden('order', $content->order) }}
        <div class="box">
            <div class="box-content">
                <div class="row">
                    <?php $r = rand(1, 999); ?>
                    @foreach($content->pageTemplate as $pageTemplate)

                        <div class="col-md-{{ $pageTemplate->column_width }}" style="margin-bottom:20px;">
                            <div class="flash-message-{{ $r }}"></div>
                            {{ Form::hidden('block['.$r.'][block_id]', $pageTemplate->block_id ) }}
                            {{ Form::hidden('block['.$r.'][column_width]', $pageTemplate->column_width ) }}
                            {{ Form::hidden('block['.$r.'][page_template]', $pageTemplate->id ) }}
                            @include('admin.blocks.partials.forms.'.$pageTemplate->block->partial.'')
                        </div>


                        <?php $r++ ?>
                    @endforeach
                        @if($pageTemplate->block->name != 'divider' && $pageTemplate->block->name != 'submenu' &&  $pageTemplate->block->name != 'results' && $pageTemplate->block->name != 'results grid')
                            <button type="submit" class="pull-right btn btn-success update-content" >Update Content</button>
                        @endif
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <?php $i++; ?>
    @endforeach
</div>

@stop

@section('scripts')
    <script>
        $(function() {
            $('.modal').on("show.bs.modal", function (e) {

                image = $(e.relatedTarget).data('image');
                block = $(e.relatedTarget).data('block');
                $('.image').click(function(){

                    $('#' + image + '').html('<input type="hidden" name="block['+ block + '][image]" value="' + $(this).data('id') + '"/><img src="' + $(this).attr('src') + '" alt="' +
                        $(this).attr('alt')+ '" class="img-responsive"/>');
                    $('#imageModal').modal('hide');

                });


            });

            $('form').submit(function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                var order = $(this).find('input[name="order"]').val();

                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{ URL::route('pages.update-content', $page->id) }}',
                    type: 'POST',
                    data: data,
                    dataType: 'html',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (response) {
                        console.log(response)
                        $('div.flash-message-' + order + '').html(response).fadeOut(3000);
                    }
                });
            });
        });
    </script>
@endsection