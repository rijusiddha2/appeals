@extends('admin.layouts.dashboard')

@section('content')

    <div id="soratable">
        <?php $i = 0; ?>
    @foreach($contentBlocks as $content)
        {{ Form::open() }}
        {{ Form::hidden('content_id', $content->id ) }}
        {{ Form::hidden('order', $i) }}
         <div class="box">
            <div class="box-content">
                <div class="row">
                    <?php $r = 0; ?>
                    @foreach($content->blocks as $block)
                        <div class="col-md-{{ $block->pivot->column_width }}">
                            <div class="flash-message-{{ $i}}"></div>
                            {{ Form::hidden('block['.$r.'][block_id]', $block->id ) }}
                            {{ Form::hidden('block['.$r.'][column_width]', $block->pivot->column_width ) }}
                            @include('admin.blocks.partials.forms.'.$block->partial.'')
                        </div>
                        <?php $r ++; ?>
                    @endforeach
                    @if($block->name != 'divider' && $block->name != 'submenu' &&  $block->name != 'results' && $block->name != 'results grid')
                        <button type="submit" class="pull-right btn btn-success update-content" >Add Content</button>
                    @endif
                </div>
            </div>
        </div>
        {{ Form::close() }}
        <?php $i++ ?>
    @endforeach
    </div>

@stop

@section('scripts')
    <script>

        $(function() {
            $('.modal').on("show.bs.modal", function (e) {
                console.log('here');
                var image = $(e.relatedTarget).data('image');
                var block = $(e.relatedTarget).data('block');
                $('.image').click(function(){
                    $('#' + image + '').html('<input type="hidden" name="block['+ block + '][image]" value="' + $(this).data('id') + '"/><img src="' + $(this).attr('src') + '" alt="' +
                        $(this).attr('alt')+ '" class="img-responsive"/>');
                    $('.modal').modal('hide');
                });
            });

            $('form').submit(function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                var order = $(this).find('input[name="order"]').val();

                $(this).find('.update-content').attr("disabled", "disabled");

                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{ URL::route('pages.store-content', $page->id) }}',
                    type: 'POST',
                    data: data,
                    dataType: 'html',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (response) {
                        console.log(response)
                        $('div.flash-message-' + order + '').html(response).fadeOut(3000);
                        {{--$('#content-blocks').append(response.html)--}}
                        {{--$('#content-blocks').append('<div class="row"><div class="col-md-2 col-md-offset-10"><a href="{{ route('templates.remove-block', $block->id) }}" title="delete" class="btn btn-danger btn-template"><i "class="fas fa-trash"></i> Remove Content Block</a></div></div>')--}}

                    }
                });
            });
        });
    </script>
@endsection