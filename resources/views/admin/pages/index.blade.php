@extends('admin.layouts.dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            Filters
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        {{ Form::label('status', 'Status')  }}
                        {{ Form::select('status', ['' => '-- All --', 'draft' => 'Draft', 'published' => 'Published'], old('team_id'),  [ 'class' => 'form-control selectpicker', 'id' => 'statusFilter'])  }}
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="box-buttons">
                <a href="{{ route('pages.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
            </div>

            @if (isset($title))
                {{ $title  }}
            @else
                Pages
            @endif

        </div>

        <div class="box-content">
            <table class="table table-bordered" id="pages-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>URL</th>
                    <th>Status</th>
                    <th>Published Date</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <a class="btn btn-success pull-right" href="{{ route('pages.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Page</a>

@stop

@section('scripts')

    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#pages-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('pages.data')  }}',
                    data: function (d) {
                        d.status = $('#statusFilter').val();
                    }
                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'url', name: 'url' },
                    { data: 'status', name: 'status' },
                    { data: 'published_date', name: 'published_date' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });
            $('#statusFilter').on('change', function (e) {
                oTable.draw();
            });

        });

    </script>
@stop