@extends('admin.layouts.dashboard')

@section('content')

    {{ Form::open(['url' => 'admin/image-uploader', 'enctype' => 'multipart/form-data', 'files' => true, 'id' => 'image-form'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Image Details
                </div>
                <div class="box-content">

                    <div class="form-group">
                        {{ Form::label('image', 'Image')  }}
                        {{ Form::file('image')  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('alt_tag', 'Alt Tag')  }}
                        {{ Form::text('alt_tag', old('alt_tag'), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('title_tag', 'Title Tag')  }}
                        {{ Form::text('title_tag', old('title_tag'), ['class' => 'form-control'])  }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Extra Details
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('image-size', 'Image Size')  }}
                        {{ Form::select('image-size[]', $sizes, old('image-size[]'), ['class' => 'form-control selectpicker', 'id' => 'image-sizes', 'data-live-search' => 'true', 'multiple' => 'multiple'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('tags', 'Tags')  }}
                        {{ Form::select('tags[]', $tags, old('tags[]'), ['class' => 'form-control selectpicker', 'id' => 'tags', 'data-live-search' => 'true', 'multiple' => 'multiple']) }}
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="" class="btn btn-success" id="get-sizes"><i class="fas fa-search-plus"></i> Get Sizes</a>
            <a href="{{ route('image-uploader.index')  }}" class="btn btn-secondary"><span data-feather="list"></span> Back to list</a>
        </div>
    </div>

@endsection

@section('scripts')
    <script>


        $('#get-sizes').click(function(e) {
            e.preventDefault();
            var form = new FormData(document.getElementById('image-form'));
            //append files
            var file = document.getElementById('image').files[0];
            if (file) {
                form.append('image', file);
            }
            $.ajax({
                /* the route pointing to the post function */
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ URL::route('images.process-sizes') }}',
                type: 'POST',
                async:false,
                data:form,
                dataType: 'html',
                processData: false,
                contentType:false,

                /* remind that 'data' is the response of the AjaxController */
                success: function (response) {
                    $('#main').append(response)
                    $('#get-sizes').replaceWith('<button type="submit" class="btn btn-primary"><span data-feather="edit"></span> Create</button>');

                    {{--$('#content-blocks').append('<div class="row"><div class="col-md-2 col-md-offset-10"><a href="{{ route('templates.remove-block', $block->id) }}" title="delete" class="btn btn-danger btn-template"><i "class="fas fa-trash"></i> Remove Content Block</a></div></div>')--}}

                },
                complete: function () {
                    if($('#croppic1').length) {
                        var image1 = $('.croppic1').attr('src');
                        var eyeCandy1 = $('#croppic1');
                        var croppicContainerPreloadOptions1 = {
                            cropUrl:'{{ url('admin/image-uploader/save-crop')}}',
                            cropData: {
                                '_token' : $('meta[name="csrf-token"]').attr('content'),
                                'width' : eyeCandy1.data('width'),
                                'height': eyeCandy1.data('height'),
                                'size'  : eyeCandy1.data('size')
                            },
                            loadPicture:image1,
                            enableMousescroll:true,

                            loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',

                        }
                        var cropContainerPreload1 = new Croppic('croppic1', croppicContainerPreloadOptions1);
                    }
                    if($('#croppic2').length) {
                        var image2 = $('.croppic2').attr('src');
                        var eyeCandy2 = $('#croppic2');
                        var croppicContainerPreloadOptions2 = {
                            cropUrl:'{{ url('admin/image-uploader/save-crop')}}',
                            cropData: {
                                '_token' : $('meta[name="csrf-token"]').attr('content'),
                                'width' : eyeCandy2.data('width'),
                                'height': eyeCandy2.data('height'),
                                'size'  : eyeCandy2.data('size')
                            },
                            loadPicture:image2,
                            enableMousescroll:true,

                            loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',

                        }

                        var cropContainerPreload2 = new Croppic('croppic2', croppicContainerPreloadOptions2);
                    }
                    if($('#croppic3').length) {
                        var image3 = $('.croppic3').attr('src');
                        var eyeCandy3 = $('#croppic3');
                        var croppicContainerPreloadOptions3 = {
                            cropUrl:'{{ url('admin/image-uploader/save-crop')}}',
                            cropData: {
                                '_token' : $('meta[name="csrf-token"]').attr('content'),
                                'width' : eyeCandy3.data('width'),
                                'height': eyeCandy3.data('height'),
                                'size'  : eyeCandy3.data('size')
                            },
                            loadPicture:image3,
                            enableMousescroll:true,

                            loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',

                        }

                        var cropContainerPreload3 = new Croppic('croppic3', croppicContainerPreloadOptions3);
                    }
                    if($('#croppic4').length) {
                        var image4 = $('.croppic4').attr('src');
                        var eyeCandy4 = $('#croppic4');
                        var croppicContainerPreloadOptions4= {
                            cropUrl:'{{ url('admin/image-uploader/save-crop')}}',
                            cropData: {
                                '_token' : $('meta[name="csrf-token"]').attr('content'),
                                'width' : eyeCandy4.data('width'),
                                'height': eyeCandy4.data('height'),
                                'size'  : eyeCandy4.data('size')
                            },
                            loadPicture:image4,
                            enableMousescroll:true,

                            loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',

                        }
                        var cropContainerPreload4 = new Croppic('croppic4', croppicContainerPreloadOptions4);
                    }
                    if($('#croppic5').length) {
                        var image5 = $('.croppic5').attr('src');
                        var eyeCandy5 = $('#croppic5');
                        var croppicContainerPreloadOptions5= {
                            cropUrl:'{{ url('admin/image-uploader/save-crop')}}',
                            cropData: {
                                '_token' : $('meta[name="csrf-token"]').attr('content'),
                                'width' : eyeCandy5.data('width'),
                                'height': eyeCandy5.data('height'),
                                'size'  : eyeCandy5.data('size')
                            },
                            loadPicture:image5,
                            enableMousescroll:true,

                            loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',

                        }
                        var cropContainerPreload5 = new Croppic('croppic5', croppicContainerPreloadOptions5);
                    }
                    if($('#croppic6').length) {
                        var image6 = $('.croppic6').attr('src');
                        var eyeCandy6 = $('#croppic6');
                        var croppicContainerPreloadOptions6= {
                            cropUrl:'{{ url('admin/image-uploader/save-crop')}}',
                            cropData: {
                                '_token' : $('meta[name="csrf-token"]').attr('content'),
                                'width' : eyeCandy6.data('width'),
                                'height': eyeCandy6.data('height'),
                                'size'  : eyeCandy6.data('size')
                            },
                            loadPicture:image6,
                            enableMousescroll:true,

                            loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',

                        }
                        var cropContainerPreload6 = new Croppic('croppic6', croppicContainerPreloadOptions6);
                    }
                }
            });
        });
    </script>
@stop