@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            <div class="box-buttons">
                <a href="{{ route('images.add-image') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
            </div>
            Picture Manager
        </div>

        <div class="box-content">
            <div class="row">
                @foreach($images as $image)
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <img src="/images/{{ $image->name }}" alt="{{ $image->alt_tag }}" class="img-responsive"/>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>

    <a class="btn btn-success pull-right" href="{{ route('image-uploader.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Image</a>

@stop

@section('scripts')
    <script>
        $(function() {
            var oTable = $('#images-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('image-uploader.data')  }}',

                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'thumb', name: 'thumb' },
                    { data: 'name', name: 'name' },
                    { data: 'sizes', name: 'sizes' },
                    { data: 'tags', name: 'tags' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop