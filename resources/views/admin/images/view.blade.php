@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Image Details
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $image->name }}</dd>

                        <dt>Alt tag</dt>
                        <dd>{{ $image->alt_tag }}</dd>

                        <dt>Title tag</dt>
                        <dd>{{ $image->title_tag }}</dd>

                        <dt>Tags</dt>
                        <dd>{{ $image->title_tag }}</dd>

                    </dl>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12 tab-content">


            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="original-tab" data-toggle="tab" href="#original" role="tab" aria-controls="original" aria-selected="true">Original</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="thumb-tab" data-toggle="tab" href="#thumb" role="tab" aria-controls="thumb" aria-selected="false">Thumb</a>
                </li>
                @foreach($image->sizes as $size)
                    <li class="nav-item">
                        <a class="nav-link" id="{{ str_replace(' ','-', strtolower($size->size)) }}-tab" data-toggle="tab" href="#{{ str_replace(' ','-', strtolower($size->size)) }}" role="tab" aria-controls="{{ strtolower
                        ($size->size) }}"
                           aria-selected="false">{{
                        $size->size
                        }}</a>
                    </li>
                @endforeach

            </ul>
            <div class="box">
                <div class="box-content">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane active" id="original" role="tabpanel" aria-labelledby="original-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box">
                                        <div class="box-content">
                                            <img src="/images/original/{{ $image->name }}" alt="{{ $image->alt_tag }}" title="{{ $image->title_tag }}" class="img-responsive"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="thumb" role="tabpanel" aria-labelledby="thumb-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box">
                                        <div class="box-content">
                                            <img src="/images/thumbs/{{ $image->name }}" alt="{{ $image->alt_tag }}" title="{{ $image->title_tag }}" class="img-responsive"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach ($image->sizes as $size)
                            <div class="tab-pane" id="{{ str_replace(' ','-', strtolower($size->size)) }}" role="tabpanel" aria-labelledby="{{ str_replace(' ','-', strtolower($size->size)) }}-tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-content">
                                                <img src="/images/{{ str_replace(' ','_',strtolower($size->size)) }}/{{ $image->name }}" alt="{{ $image->alt_tag }}" title="{{ $image->title_tag }}" class="img-responsive"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('image-uploader.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit Image')
                <a href="{{ route('image-uploader.edit', $image->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @endcan
            @can('Delete Image')
                <a href="{{ route('image-uploader.destroy', $image->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan
        </div>
    </div>
@stop