@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            @can('Add Image')
                <div class="box-buttons">
                    <a href="{{ route('image-uploader.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
                </div>
            @endCan

            @if (isset($title))
                {{ $title  }}
            @else
                Images
            @endif

        </div>

        <div class="box-content">
            <table class="table table-bordered" id="images-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Thumb</th>
                    <th>Name</th>
                    <th>Sizes</th>
                    <th>Tags</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    @can('Add Image')
        <a class="btn btn-success pull-right" href="{{ route('image-uploader.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Image</a>
    @endcan

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#images-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('image-uploader.data')  }}',

                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'thumb', name: 'thumb' },
                    { data: 'name', name: 'name' },
                    { data: 'sizes', name: 'sizes' },
                    { data: 'tags', name: 'tags' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop