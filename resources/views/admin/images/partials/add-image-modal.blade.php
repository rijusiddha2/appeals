<div class="modal fade" id="imageModal{{ isset($i) ? $i : 0 }}"
     tabindex="-1" role="dialog"
     aria-labelledby="imagesModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="imagesModalLabel">Add Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    @if(isset($block))
                        @foreach(\App\Image::getImageSizes($block->pivot->column_width) as $image)
                            <div class="col-md-2">
                                <img src="/images/{{ strtolower(str_replace(' ','_',$image->size)) }}/{{ $image->name }}" alt="{{ $image->alt }}" data-id="{{ $image->image_id }}"class="image img-responsive"/>

                            </div>
                        @endforeach
                    @elseif(isset($pageTemplate))
                        @foreach(\App\Image::getImageSizes($pageTemplate->column_width) as $image)
                            <div class="col-md-2 {{ $pageTemplate->column_width }}">
                                <img src="/images/{{ strtolower(str_replace(' ','_',$image->size)) }}/{{ $image->name }}" alt="{{ $image->alt }}" data-image="{{ $pageTemplate->id }}" data-id="{{ $image->image_id }}"class="image
                                img-responsive"/>

                            </div>
                        @endforeach
                    @elseif(isset($appeal))
                        @foreach(\App\Image::getImages('Large Landscape') as $image)
                            <div class="col-md-2">
                                <img src="/images/large_landscape/{{ $image->name }}" alt="{{ $image->alt }}" data-id="{{ $image->image_id }}"class="image img-responsive"/>

                            </div>
                        @endforeach
                    @else
                        @foreach(\App\Image::getImages('Banner') as $image)
                            <div class="col-md-2">
                                <img src="/images/banner/{{ $image->name }}" alt="{{ $image->alt }}" data-id="{{ $image->image_id }}"class="image img-responsive"/>

                            </div>
                        @endforeach
                    @endif
                </div>

            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-danger"
                        data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>