<div class="row">
    <div class="col-md-12 image-tabs">

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="original-tab" data-toggle="tab" href="#original" role="tab" aria-controls="original" aria-selected="true">Original</a>
            </li>

            @foreach($sizes as $size)
                <li class="nav-item">

                        <a class="nav-link" id="{{ str_replace(' ','-', strtolower($size->size)) }}-tab" data-toggle="tab" href="#{{ str_replace(' ','-', strtolower($size->size)) }}" role="tab" aria-controls="{{ strtolower
                        ($size->size) }}"
                           aria-selected="false">{{
                        $size->size
                        }}</a>
                </li>
            @endforeach

        </ul>
        <div class="box">
            <div class="box-content">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane active" id="original" role="tabpanel" aria-labelledby="original-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="box-content">
                                        <img src="/images/original/{{ $filename }}" alt="{{ $imageDetails['alt_tag'] }}" title="{{ $imageDetails['title_tag'] }}" class="img-responsive"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @foreach ($sizes as $size)
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <div class="tab-pane" id="{{ str_replace(' ','-', strtolower($size->size)) }}" role="tabpanel" aria-labelledby="{{ str_replace(' ','-', strtolower($size->size)) }}-tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box">
                                        <div class="box-content">
                                            <div id="croppic{{ $size->id }}"
                                                      style="width:{{ $size->width }}px; height:{{ $size->height }}px; position:relative;"
                                                  data-width="{{ $size->width }}" data-height="{{ $size->height }}" data-size="{{ $size->size }}">
                                            <img src="/images/{{ $filename }}" alt="{{ $imageDetails['alt_tag'] }}" title="{{ $imageDetails['title_tag'] }}" class="croppic{{ $size->id }} img-responsive"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


    </div>
    {{ Form::close()  }}
</div>

