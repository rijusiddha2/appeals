@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($image, ['route' => ['image-uploader.update', $image->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data'])  }}

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Image Detail - {{ $image->name }}
                </div>
                <div class="box-content">

                    <div class="form-group">
                        {{ Form::label('alt_tag', 'Alt Tag')  }}
                        {{ Form::text('alt_tag', $image->alt_tag, ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('title_tag', 'Title Tag')  }}
                        {{ Form::text('title_tag', $image->title_tag, ['class' => 'form-control'])  }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Extra Details
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('image-size', 'Image Size')  }}
                        {{ Form::select('image-size[]', $sizes, old('image-size[]'), ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'multiple' => 'multiple'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('tags', 'Tags')  }}
                        {{ Form::select('tags[]', $tags, old('tags[]'), ['class' => 'form-control selectpicker', 'id' => 'tags', 'data-live-search' => 'true', 'multiple' => 'multiple']) }}
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('image-uploader.index')  }}" class="btn btn-secondary"><span data-feather="list"></span> Back to list</a>
            <button type="submit" class="btn btn-primary"><span data-feather="edit"></span> Update</button>
        </div>
    </div>
    {{ Form::close()  }}
@endsection
