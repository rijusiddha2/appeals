<div class="placeholder">
    <i class="fas fa-times-circle" class="img-responsive"></i>
    <div class="text-center">{{ $contentBlock->name }} - Not Editable</div>
</div>
