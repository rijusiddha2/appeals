<div class="stat-wrapper">
    <div class="row">
        <div class="col-md-8">
            <div class="placeholder">
                <i class="far fa-image" class="img-responsive"></i>
                <div class="stat stat-lg">Stat</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="stat-text-lg">
                3 million girls are
                out of school in India
            </div>
        </div>

    </div>
</div>
