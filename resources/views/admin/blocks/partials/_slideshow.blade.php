<div id="carousel">
    <div class="bxslider">
        <div>
            <img src="/images/banner/karunaappeals-homepageheader-web.jpg" class="img-responsive" alt="Girls Hostel" title="Girls Hostel"/>
            <div class="image-text">
                <div class="caption" style="font-size:18px;">
                    KARUNA FUNDRAISING
                </div>
                <div class="title" style="font-size:44px; line-height:44px;">
                    From Discrimination to Dignity in India and Nepal
                </div>
            </div>
        </div>
        <div>
            <img src="/images/banner_nishtawomen.jpg" class="img-responsive" alt="Nishta Women" title="Nishta Women"/>
            <div class="image-text">
                <div class="caption" style="font-size:18px;">
                    KARUNA FUNDRAISING
                </div>
                <div class="title" style="font-size:44px;">
                    From Discrimination to Dignity in India and Nepal
                </div>
            </div>
        </div>
    </div>
</div>