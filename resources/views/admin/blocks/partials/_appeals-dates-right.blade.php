<div class="appeals-dates">
     <div class="row">
         <div class="col-md-offset-5 col-md-7">
             <div class="placeholder">
                 <i class="far fa-image" class="img-responsive"></i>
             </div>
         </div>
         <div class="appeals-dates-text" style="left:25%;">
             <h1>Appeals Heading</h1>

             <p>Fusce sodales volutpat dignissim. Donec hendrerit lorem non quam suscipit, at tristique metus scelerisque. In feugiat eros tellus, quis aliquam dolor vulputate at. Sed cursus nulla ut arcu ornare, in efficitur sem ultricies.
                 Maecenas dapibus quam ut efficitur consequat. Praesent consequat egestas nibh, at convallis ligula sollicitudin sit amet. Nam vestibulum libero ut tristique pulvinar. In cursus, nisi vel porttitor hendrerit, velit justo
                 condimentum nisl, et tristique sem arcu in lectus. Nunc mauris velit, ultricies nec ornare nec, tincidunt sed velit. Curabitur ullamcorper in orci vel rutrum.</p>

             <a href="/applications" class="btn btn-rounded">APPLY NOW</a>
         </div>
     </div>
</div>