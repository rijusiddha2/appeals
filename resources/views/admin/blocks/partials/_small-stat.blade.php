<div class="stat-wrapper">
    <div class="row">
        <div class="col-md-offset-6 col-md-3">
            <div class="placeholder">
                <i class="far fa-image" class="img-responsive"></i>
                <div class="stat stat-sm">Stat</div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="stat-text-sm">
                12 million children are married
                before the age Of 10 in India
            </div>
        </div>

    </div>
</div>
