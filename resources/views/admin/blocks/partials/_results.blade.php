<div class="row">
    <div class="col-lg-5 col-lg-offset-1">
        <div class="img-wrapper">
            <div class="placeholder">
                <i class="far fa-image" class="img-responsive"></i>
            </div>
        </div>

    </div>
    <div class="col-lg-5 ">
        <div class="text">
            <h1>Title</h1>
            <h3>TEAM TOTAL: £xxxxx</h3>
            <h3 style="margin-top: 10px;">SUPPORTERS CONNECTED: xx</h3>
            <p>Intro Text</p>
        </div>
    </div>
</div>
