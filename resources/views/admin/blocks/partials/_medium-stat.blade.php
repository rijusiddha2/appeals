<div class="stat-wrapper">
    <div class="row">
        <div class="col-md-offset-1 col-md-7">
            <div class="placeholder">
                <i class="far fa-image" class="img-responsive"></i>
                <div class="stat stat-md">Stat</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="stat-text-md">
                96% of children in Karuna projects continue their education past the age of 14, almost double the national average.
            </div>
        </div>

    </div>
</div>