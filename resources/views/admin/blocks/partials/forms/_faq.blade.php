<div class="form-group">
    {{ Form::label('block['. $r .'][faq]', 'FAQ Category')  }}
    {{ Form::select('block['. $r .'][faq]', $faqs, old('block['. $r .'][faq]', isset($pageTemplate) ? $pageTemplate->faq : ''), ['class' => 'form-control', 'required'])  }}
</div>