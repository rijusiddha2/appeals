<div class="form-group">
    {{ Form::label('block['. $r .'][slider]', 'Slideshow')  }}
    {{ Form::select('block['. $r .'][slider]', $slideshows, old('block['. $r .'][slider]', isset($pageTemplate) ? $pageTemplate->slider : ''), ['class' => 'form-control selectpicker', 'required'])  }}
</div>