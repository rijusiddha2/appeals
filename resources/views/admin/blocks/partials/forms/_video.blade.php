<div class="form-group">
    {{ Form::label('block['. $r .'][text]', 'Video URL')  }}
    {{ Form::text('block['. $r .'][text]', old('block['. $r .'][text]', isset($pageTemplate) ? $pageTemplate->text : ''), ['class' => 'form-control', 'required'])  }}
</div>