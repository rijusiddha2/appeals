
<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            {{ Form::textarea('block['. $r .'][text]', old('text',  isset($pageTemplate) ? $pageTemplate->text : ''), ['class' => 'form-control editor'])  }}
        </div>
    </div>

    <div class="col-md-7 test">
        <div id="image{{ $i }}">
            @if(isset($pageTemplate->images))
                <img src="/images/large_landscape/{{ $pageTemplate->images->name }}" alt="{{ $pageTemplate->images->alt }}" title="{{
                $pageTemplate->images->title }}" class="img-responsive"
                     style="margin-bottom:20px;"/>

                <button
                        type="button"
                        class="btn btn-primary"
                        data-toggle="modal"
                        data-image="image{{ $i }}"
                        data-block="{{ $i }}"
                        data-target="#imageModal{{ $i}}"
                >
                    Change Image
                </button>

            @else
                <div class="placeholder">
                    <button
                            type="button"
                            class="btn btn-primary image-modal"
                            data-toggle="modal"
                            data-image="image{{ $i }}"
                            data-block="{{ $i }}"
                            data-target="#imageModal{{ $i }}"
                    >
                        Select Image
                    </button>
                </div>
            @endif
        </div>
    </div>
</div>
@include('admin.images.partials.add-image-modal')