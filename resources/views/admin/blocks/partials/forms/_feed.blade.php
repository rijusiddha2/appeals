<div class="form-group">
    {{ Form::label('block['. $r .'][link_text]', 'Feed URL')  }}
    {{ Form::text('block['. $r .'][link_text]', old('block['. $r .'][link_text]', isset($pageTemplate) ? $pageTemplate->link_text : ''), ['class' => 'form-control', 'required'])  }}
</div>