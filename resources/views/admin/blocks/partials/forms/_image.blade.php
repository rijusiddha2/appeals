<div id="image{{ $i }}">
    @if(isset($pageTemplate->images))
        {{ Form::hidden('block['.$r.'][image]', $pageTemplate->images->id) }}
        <img src="/images/{{ \App\Image::getImageSize($pageTemplate->column_width, $pageTemplate->images->name )}}/{{ $pageTemplate->images->name }}" alt="{{ $pageTemplate->images->alt }}" title="{{ $pageTemplate->images->title
        }}" class="img-responsive"
             style="margin-bottom:20px;"/>

        <button
                type="button"
                class="btn btn-primary"
                data-toggle="modal"
                data-image="image{{ $i}}"
                data-block="{{ $r }}"
                data-target="#imageModal{{ $i }}"
                >
            Change Image
        </button>

    @else
        <div class="placeholder">
            <button
                    type="button"
                    class="btn btn-primary image-modal"
                    data-toggle="modal"
                    data-image="image{{ $i }}"
                    data-block="{{ $r }}"
                    data-target="#imageModal{{ $i }}"
                    >
                Select Image
            </button>
        </div>
    @endif
</div>
@if($content->columns > 2 || (isset($pageTemplate->column_width) && $pageTemplate->column_width < 12))
<div class="form-group {{$content->columns}}">
    {!! Form::textarea('block['. $r .'][text]', old('block['. $r .'][text]', isset($pageTemplate) ? $pageTemplate->text : ''), ['class' => 'form-control editor'])  !!}
</div>
@endif

@include('admin.images.partials.add-image-modal')


