<div class="form-group">
    {{ Form::label('block['. $r .'][link]', 'Nav Link Anchor', ['class' => 'control-label']) }}
    {{ Form::text('block['. $r .'][link]', old('block['. $r .'][link]',$content->link), ['class' => 'form-control','required']) }}
</div>