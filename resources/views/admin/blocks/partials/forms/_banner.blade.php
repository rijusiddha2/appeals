<div id="image{{ $i }}">
    @if(isset($pageTemplate->images))
        <img src="/images/full_image/{{ $pageTemplate->images->name }}" alt="{{ $pageTemplate->images->alt }}" title="{{ $pageTemplate->images->title }}" class="img-responsive" style="margin-bottom:20px;"/>

        <button
                type="button"
                class="btn btn-primary"
                data-toggle="modal"
                data-image="image{{ $i }}"
                data-block="{{ $r }}"
                data-target="#imageModal{{ $i }}">
            Change Image
        </button>

    @else
        <div class="placeholder">
            <button
                    type="button"
                    class="btn btn-primary"
                    data-toggle="modal"
                    data-image="image{{ $i }}"
                    data-block="{{ $r }}"
                    data-target="#imageModal{{ $i }}">
                Select Image
            </button>
        </div>
    @endif
</div>
<p>&nbsp;</p>
<div class="form-group">
    {{ Form::label('block['. $r .'][title]', 'Title', ['class' => 'control-label']) }}
    {{ Form::text('block['. $r .'][title]', old('block['. $r .'][title]',  isset($pageTemplate) ? $pageTemplate->title : ''), ['class' => 'form-control', 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('block['. $r .'][caption]', 'Caption', ['class' => 'control-label']) }}
    {{ Form::text('block['. $r .'][caption]', old('block['. $r .'][caption]', isset($pageTemplate) ? $pageTemplate->title : ''), ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('block['. $r .'][link]', 'Link', ['class' => 'control-label']) }}
    {{ Form::text('block['. $r .'][link]', old('block['. $r .'][link]',isset($pageTemplate) ? $pageTemplate->link : ''), ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('block['. $r .'][link_text]', 'Link Text', ['class' => 'control-label']) }}
    {{ Form::text('block['. $r .'][link_text]', old('block['. $r .'][link_text]', isset($pageTemplate) ? $pageTemplate->link_text : ''), ['class' => 'form-control']) }}
</div>
@include('admin.images.partials.add-image-modal')

