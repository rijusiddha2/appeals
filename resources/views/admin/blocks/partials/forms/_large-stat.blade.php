<div class="row">
    <div class="col-md-8">
        <div id="image{{ $i }}">
            @if(isset($pageTemplate->images))
                <img src="/images/large_landscape/{{ $pageTemplate->images->name }}" alt="{{ $pageTemplate->images->alt }}" title="{{ $pageTemplate->images->title }}" class="img-responsive" style="margin-bottom:20px;"/>
                <button
                        type="button"
                        class="btn btn-primary"
                        data-toggle="modal"
                        data-image="image{{ $i }}"
                        data-block="{{ $r }}"
                        data-target="#imageModal{{ $i }}"
                >
                    Change Image
                </button>
            @else
                <div class="placeholder">
                    <button
                            type="button"
                            class="btn btn-primary"
                            data-toggle="modal"
                            data-image="image{{ $i }}"
                            data-block="{{ $r }}"
                            data-target="#imageModal{{ $i }}"
                            >
                        Select Image
                    </button>
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {{ Form::textarea('block['. $r .'][text]', old('block['. $r .'][text]',  isset($pageTemplate) ? $pageTemplate->text : ''), ['class' => 'form-control'])  }}
        </div>
        <div class="form-group">
            {{ Form::label('block['. $r .'][caption]', 'Statistic', ['class' => 'control-label']) }}
            {{ Form::text('block['. $r .'][caption]', old('block['. $r .'][caption]', isset($pageTemplate) ? $pageTemplate->caption : ''), ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>

@include('admin.images.partials.add-image-modal')

