@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::open(['url' => 'admin/blocks'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Block Details
                </div>
                <div class="box-content">
                    <div class="alert-info">A partial will need to be created manually for the block</div>
                    <div class="form-group">
                        {{ Form::label('name', 'Name')  }}
                        {{ Form::text('name', old('name'), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('partial', 'Partial')  }}
                        {{ Form::text('partial', old('partial'), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('type', 'Type')  }}
                        {{ Form::select('type', $types, old('type'), ['class' => 'form-control selectpicker'])  }}
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('blocks.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Create</button>
        </div>
    </div>

    {{ Form::close()  }}

@endsection
