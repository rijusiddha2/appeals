@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            <div class="box-buttons">
                <a href="{{ route('blocks.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
            </div>

            @if (isset($title))
                {{ $title  }}
            @else
                Blocks
            @endif

        </div>
        <div class="box-content">
            <table class="table table-bordered" id="blocks-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Partial</th>
                    <th>Type</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <a class="btn btn-success pull-right" href="{{ route('blocks.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Block</a>

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#blocks-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('blocks.data')  }}'
                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'partial', name: 'partial' },
                    { data: 'type', name: 'type' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop