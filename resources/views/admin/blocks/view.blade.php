@extends('admin.layouts.dashboard')

@section('content')
    <style>
        .placeholder .fa{
            font-size:36px;
            display:block;
            margin:0 auto;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    Block Details
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $block->name }}</dd>
                        <hr/>
                        @include('admin.blocks.partials.'.$block->partial)
                    </dl>
                </div>
            </div>
        </div>

    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('blocks.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit Block')
                <a href="{{ route('blocks.edit', $block->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @endcan
            @can('Delete Block')
                <a href="{{ route('blocks.delete', $block->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan
        </div>
    </div>
@stop