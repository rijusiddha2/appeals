@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($block, ['route' => ['blocks.update', $block->id], 'method' => 'PUT'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Block Details
                </div>
                <div class="box-content">


                    <div class="form-group">
                        {{ Form::label('name', 'Name')  }}
                        {{ Form::text('name', $block->name, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('partial', 'Partial')  }}
                        {{ Form::text('partial', $block->name, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('type', 'Type')  }}
                        {{ Form::select('type', $types, $block->type, ['class' => 'form-control selectpicker'])  }}
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('blocks.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            <button type="submit" class="btn btn-success pull-right"><i class="fas fa-check"></i> Save</button>

        </div>
    </div>
    {{ Form::close()  }}
@endsection
