<!DOCTYPE html>
<html lang="{!! app()->getLocale() !!}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @page { margin: 100px 25px; }
        header { margin-top:-60px;  height:100px; border-bottom: 1px solid #dee2e6;}
        header img {height:90px; margin: 0 auto; display: inline-block;text-align: center;}
        footer { position: fixed; bottom: -100px; left: 0px; right: 0px; padding-top:10px; text-align: center; color: grey; height: 50px; border-top: 1px solid lightgrey;}
        p:last-child { page-break-after: never; }
        table {
            margin-bottom:10px;
        }
        thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }
        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }
        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }
        .table th, .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }
        .table-bordered th, .table-bordered td {
            border: 1px solid #dee2e6;
        }

        .center-block img {
            text-align: center;
            margin:0 auto;
        }

    </style>

</head>
<body>
<header>
    <img src="https://www.appeals.karuna.org/images/logo-orange.png" alt="Karuna"/>
</header>
<h2>Job Application Report for {{ $jobApplication->first_name }} {{ $jobApplication->last_name }}</h2>


<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Personal Contact Details</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Name
        </td>

        <td width="50%">
            {{ $jobApplication->title  }} {{ $jobApplication->first_name }} {{ $jobApplication->last_name }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Gender
        </td>

        <td width="50%">
            {{ $jobApplication->gender  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Address
        </td>

        <td width="50%">
            {{ $jobApplication->address  }}<br/>
            {{ $jobApplication->postcode }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Email
        </td>

        <td width="50%">
            {{ $jobApplication->email  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Mobile
        </td>

        <td width="50%">
            {{ $jobApplication->mobile  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Alternative Telephone
        </td>

        <td width="50%">
            {{ $jobApplication->alt_tel  }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Personal Contact Details</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Name
        </td>

        <td width="50%">
            {{ $jobApplication->title  }} {{ $jobApplication->first_name }} {{ $jobApplication->last_name }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Address
        </td>

        <td width="50%">
            {{ $jobApplication->address  }}<br/>
            {{ $jobApplication->postcode }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Email
        </td>

        <td width="50%">
            {{ $jobApplication->email  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Mobile
        </td>

        <td width="50%">
            {{ $jobApplication->mobile  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Alternative Telephone
        </td>

        <td width="50%">
            {{ $jobApplication->alt_tel  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Work Permit
        </td>

        <td width="50%">
            {{ $jobApplication->work_permit == 1 ? 'Yes' : 'No'  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Driving Licence
        </td>

        <td width="50%">
            {{ $jobApplication->driving_licence == 1 ? 'Yes' : 'No'  }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Employment details (current or recent)</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Job Title
        </td>
        <td width="50%">
            {{ $jobApplication->currentHistory->job_title }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Employer Name / Address
        </td>
        <td width="50%">
            {{ $jobApplication->currentHistory->employer_details }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Date employed from
        </td>
        <td width="50%">
            {{ $jobApplication->currentHistory->employed_from->format('d/m/Y') }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Date employed to
        </td>
        <td width="50%">
            {{ $jobApplication->currentHistory->employed_to->format('d/m/Y') }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Responsibilities
        </td>
        <td width="50%">
            {{ $jobApplication->currentHistory->responsibilities }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Employment details (previous)</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Job Title
        </td>
        <td width="50%">
            {{ $jobApplication->previousHistory->job_title }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Employer Name / Address
        </td>
        <td width="50%">
            {{ $jobApplication->previousHistory->employer_details }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Date employed from
        </td>
        <td width="50%">
            {{ $jobApplication->previousHistory->employed_from->format('d/m/Y') }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Date employed to
        </td>
        <td width="50%">
            {{ $jobApplication->previousHistory->employed_to->format('d/m/Y') }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Responsibilities
        </td>
        <td width="50%">
            {{ $jobApplication->previousHistory->responsibilities }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Qualifications and Education</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Professional Qualifications
        </td>
        <td width="50%">
            {{ $jobApplication->qualifications->professional_qualifications }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Educational Qualifications
        </td>
        <td width="50%">
            {{ $jobApplication->qualifications->educational_qualifications }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Further Details</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Skills
        </td>
        <td width="50%">
            {{ $jobApplication->further->skills }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Disabilities
        </td>
        <td width="50%">
            {{ $jobApplication->further->disabilities }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Safeguarding</th>
    </tr>
    </thead>
    <tr>
        <td width="50%">
            Safeguarding Investigations
        </td>
        <td width="50%">
            {{ $jobApplication->safeguarding->vunerable_sanctions == 1 ? 'Yes' : 'No' }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Criminal Offences
        </td>
        <td width="50%">
            {{ $jobApplication->safeguarding->criminal_offences == 1 ? 'Yes' : 'No' }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Safeguarding Details (if applicable)
        </td>
        <td width="50%">
            {{ $jobApplication->safeguarding->safeguarding_details }}
        </td>
    </tr>
    </tbody>
</table>
<footer>
    Created {!! \Carbon\Carbon::now()->format('d/m/Y H:i')  !!} | {!! Auth::user()->name  !!}
</footer>
</body>
</html>
