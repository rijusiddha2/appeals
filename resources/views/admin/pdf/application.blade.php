<!DOCTYPE html>
<html lang="{!! app()->getLocale() !!}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @page { margin: 100px 25px; }
        header { margin-top:-60px;  height:100px; border-bottom: 1px solid #dee2e6;}
        header img {height:90px; margin: 0 auto; display: inline-block;text-align: center;}
        footer { position: fixed; bottom: -100px; left: 0px; right: 0px; padding-top:10px; text-align: center; color: grey; height: 50px; border-top: 1px solid lightgrey;}
        p:last-child { page-break-after: never; }
        table {
            margin-bottom:10px;
        }
        thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }
        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }
        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }
        .table th, .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }
        .table-bordered th, .table-bordered td {
            border: 1px solid #dee2e6;
        }

        .center-block img {
            text-align: center;
            margin:0 auto;
        }

    </style>

</head>
<body>
<header>
    <img src="https://www.appeals.karuna.org/images/logo-orange.png" alt="Karuna"/>
</header>
<h2>Job Application Report for {{ $application->first_name }} {{ $application->last_name }}</h2>


<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Personal Contact Details</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Name
        </td>

        <td width="50%">
            {{ $application->title  }} {{ $application->first_name }} {{ $application->last_name }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Gender
        </td>

        <td width="50%">
            {{ $application->gender  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Address
        </td>

        <td width="50%">
            {{ $application->address  }}<br/>
            {{ $application->postcode }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Email
        </td>

        <td width="50%">
            {{ $application->email  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Mobile
        </td>

        <td width="50%">
            {{ $application->mobile  }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Alternative Telephone
        </td>

        <td width="50%">
            {{ $application->alt_tel  }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Application Details</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Work Experience
        </td>
        <td width="50%">
            {{ $application->details->work_experience }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Triratna Involvement
        </td>
        <td width="50%">
            {{ $application->details->triratna_history }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            How found out about Karuna Appeals
        </td>
        <td width="50%">
            {{ $application->details->contact_source }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Health and Fitness level
        </td>
        <td width="50%">
            {{ $application->details->health }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Health Details
        </td>
        <td width="50%">
            {{ $application->details->health_details }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Any phsychiatric treatment or psychological medication?
        </td>
        <td width="50%">
            {{ $application->details->medication  == 1 ? 'Yes' : 'No' }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Phsychiatric details
        </td>
        <td width="50%">
            {{ $application->details->medication_details }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Safeguarding investigations?
        </td>
        <td width="50%">
            {{ $application->details->safeguarding_offence == 1 ? 'Yes' : 'No' }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Criminal Offences?
        </td>
        <td width="50%">
            {{ $application->details->criminal_offence  == 1 ? 'Yes' : 'No' }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Safegaurding and/or Criminal details
        </td>
        <td width="50%">
            {{ $application->details->offence_details }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Support Package
        </td>
        <td width="50%">
            {{ $application->details->support_package }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Additional Support Details
        </td>
        <td width="50%">
            {{ $application->details->support_extras }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Ethos Commitment?
        </td>
        <td width="50%">
            {{ $application->details->ethos  == 1 ? 'Yes' : 'No' }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Ethos Details if not agreed
        </td>
        <td width="50%">
            {{ $application->details->ethos_details }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Data Consent
        </td>
        <td width="50%">
            {{ $application->details->privacy  == 1 ? 'Yes' : 'No'}}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Keep details for future opportunities
        </td>
        <td width="50%">
            {{ $application->details->future_opportunites == 1 ? 'Yes' : 'No' }}
        </td>
    </tr>
    </tbody>
</table>
<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">Appeal Details</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Type of appeal(s) interested in
        </td>
        <td width="50%">
            {{ $application->appeals->type }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Season
        </td>
        <td width="50%">
            {{ $application->appeals->season }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Year
        </td>
        <td width="50%">
            {{ $application->appeals->year }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Other Comments
        </td>
        <td width="50%">
            {{ $application->appeals->comments }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            What is it that draws you to want to do a Karuna Appeal?
        </td>
        <td width="50%">
            {{ $application->appeals->draw }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            What do you think you would bring to the Appeal?
        </td>
        <td width="50%">
            {{ $application->appeals->skills }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            What difficulties might you anticipate doing an Appeal?
        </td>
        <td width="50%">
            {{ $application->appeals->difficulties }}
        </td>
    </tr>
    </tbody>
</table>

<table width="570" border="1" cellpadding="5" cellspacing="0" align="center">
    <thead>
    <tr>
        <th colspan="2">References</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td width="50%">
            Name of Referee 1
        </td>
        <td width="50%">
            {{ $application->references->name_1 }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Relationship of Referee 1
        </td>
        <td width="50%">
            {{ $application->references->relationship_1 }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Telephone of referee 1
        </td>
        <td width="50%">
            {{ $application->references->telephone_1 }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Email of Referee 1
        </td>
        <td width="50%">
            {{ $application->references->email_1 }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Name of Referee 1
        </td>
        <td width="50%">
            {{ $application->references->name_2 }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Relationship of Referee 1
        </td>
        <td width="50%">
            {{ $application->references->relationship_2 }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Telephone of referee 1
        </td>
        <td width="50%">
            {{ $application->references->telephone_2 }}
        </td>
    </tr>
    <tr>
        <td width="50%">
            Email of Referee 1
        </td>
        <td width="50%">
            {{ $application->references->email_2 }}
        </td>
    </tr>
    </tbody>
</table>
<footer>
    Created {!! \Carbon\Carbon::now()->format('d/m/Y H:i')  !!} | {!! Auth::user()->name  !!}
</footer>
</body>
</html>
