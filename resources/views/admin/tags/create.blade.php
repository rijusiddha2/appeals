@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::open(['url' => 'admin/tags'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Tag
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('name', 'Name')  }}
                        {{ Form::text('name', old('name'), ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('slug', 'Slug')  }}
                        {{ Form::text('slug', old('slug'), ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('type', 'Type')  }}
                        {{ Form::select('type', ['' => '-- Please Select --', 'image' => 'Image'], old('type'),['class' => 'form-control selectpicker'])  }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <button type="submit" class="btn btn-success pull-right"><i class="fas fa-check"></i> Create</button>
            {{ Form::close()  }}
        </div>
    </div>


@stop
