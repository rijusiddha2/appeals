@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Tag Details
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $tag->name }}</dd>

                        <dt>Slug</dt>
                        <dd>{{ $tag->slug }}</dd>

                        <dt>Type</dt>
                        <dd>{{ $tag->type }}</dd>
                    </dl>
                </div>
            </div>
        </div>

    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('tags.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Add Tag')
                <a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @endcan
            @can('Delete Tag')
                <a href="{{ route('tags.delete', $tag->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan
        </div>
    </div>
@stop