@extends('admin.layouts.dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            @can('Add Tag')
                <div class="box-buttons">
                    <a href="{{ route('tags.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
                </div>
            @endcan

            @if (isset($title))
                {{ $title  }}
            @else
                Tags
            @endif

        </div>

        <div class="box-content">
            <table class="table table-bordered" id="tags-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Type</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    @can('Add Tag')
        <a class="btn btn-success pull-right" href="{{ route('tags.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Tag</a>
    @endcan

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#tags-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('tags.data')  }}'
                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'slug', name: 'slug' },
                    { data: 'type', name: 'type' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop