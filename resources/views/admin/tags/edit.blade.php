@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($tag, ['route' => ['tags.update', $tag->id], 'method' => 'PUT'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Tag
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('name', 'Name')  }}
                        {{ Form::text('name', $tag->name, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('slug', 'Slug')  }}
                        {{ Form::text('slug', $tag->slug, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('type', 'Type')  }}
                        {{ Form::select('type', ['' => '-- Please Select --', 'image' => 'Image'], $tag->type,['class' => 'form-control selectpicker'])  }}
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <button type="submit" class="btn btn-success pull-right"><i class="fas fa-check"></i> Update</button>
            {{ Form::close()  }}
        </div>
    </div>

@stop
