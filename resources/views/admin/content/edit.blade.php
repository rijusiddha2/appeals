@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($content, ['route' => ['content.update', $content->id], 'method' => 'PUT'])  }}
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    Content Block Details
                </div>
                <div class="box-content">
                    <div class="form-group">
                        {{ Form::label('name', 'Name')  }}
                        {{ Form::text('name', old('name', $content->name), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('columns', 'columns')  }}
                        {{ Form::select('columns', $columns, old('columns', $content->columns), ['class' => 'form-control selectpicker'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::checkbox('editable', old('editable', $content->editable) ) }}
                        {{ Form::label('editable', 'Editable?')}}<br>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('content.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Update</button>
        </div>
    </div>

    {{ Form::close()  }}

@endsection