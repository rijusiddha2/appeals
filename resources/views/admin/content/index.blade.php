@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            <div class="box-buttons">
                <a href="{{ route('content.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
            </div>

            @if (isset($title))
                {{ $title  }}
            @else
                Content
            @endif

        </div>
        <div class="box-content">
            <table class="table table-bordered" id="content-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Columns</th>
                    <th>Editable</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <a class="btn btn-success pull-right" href="{{ route('content.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Content Block</a>

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#content-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('content.data')  }}'
                },
                order: [[ 1, "asc" ]],
                columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'columns', name: 'columns' },
                { data: 'editable', name: 'editable' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop