@extends('admin.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    Content Block Details
                </div>

                <div class="box-content">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $content->name }}</dd>
                        <dt>Columns</dt>
                        <dd>{{ $content->columns }}</dd>
                        <dt>Editable</dt>
                        <dd>{{ $content->editable == 1 ? 'Yes' : 'No' }}</dd>
                    </dl>
                </div>
            </div>
        </div>

    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('content.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit Content')
                <a href="{{ route('content.edit', $content->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @endcan
            @can('Delete Content')
                <a href="{{ route('content.delete', $content->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</a>
            @endcan
        </div>
    </div>
@stop