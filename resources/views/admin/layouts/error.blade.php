<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Karuna | Appeals') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" >
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
    @yield('styles')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


</head>

<body >
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <div class="col-sm-3 col-md-2">
        <a href="">{{ (Auth::user()->order_name ? Auth::user()->order_name :Auth::user()->name)   }}</a>
    </div>
    <div class="col-sm-5 col-md-7 karuna-brand">
        <img src=" {{ asset('images/karuna-logo.png') }}"/> <strong>Karuna Appeals</strong>
    </div>
    <div class="col-sm-1 offset-sm-1">
        <a href=""><i class="far fa-bell"></i>0</a>
    </div>

    <div class="col-sm-2">
        <button class="btn btn-default" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Logout
        </button>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>

</nav>

<div class="container-fluid" id="app">
    <div class="row">
        @include('admin.layouts.partials.sidebar')
        @if (Route::getCurrentRoute()->uri() != '/')
            @include('admin.layouts.partials.breadcrumbs')
        @endif

        <main role="main" class="col-md-10 col-lg-10">
            <div>
                @if(Session::has('flash_message'))
                    <div style="margin-top:15px;" class="col-md-8" id="success-message">
                        <div class="alert alert-success"><em> {{ session('flash_message')  }}</em>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div style="margin-top:15px;" class="col-md-8">
                        @include ('errors.index')
                    </div>
                </div>
                @if(Route::current()->getName() == 'dashboard')
                    <p>Dashboard Widgets Here</p>
                @else
                    @yield('content')
                @endif

            </div>
        </main>

    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
{{--<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>--}}
<script src="{{ asset('js/app.js') }}"></script>


@yield('scripts');

</body>
</html>
