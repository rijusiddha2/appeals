<div class="col-md-2">
    <nav class="sidebar">
        <div class="sidebar-sticky">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('dashboard')  }}">
                        <i class="fas fa-home"></i>
                        Dashboard <span class="sr-only">(current)</span>
                    </a>
                </li>

                <li class="nav-item top-menu">
                    <a class="nav-link clickable" href="#">
                        <i class="fas fa-cog"></i>
                        Admin
                        <i class="fas fa-caret-down pull-right"></i>
                    </a>
                    <ul class="sub-menu">
                        @hasanyrole('Admin|Appeal Leader')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('users.index') }}">
                                <i class="fas fa-users"></i>
                                Users
                            </a>
                        </li>
                        @endrole
{{--                        @role('Admin')--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="{{ route('roles.index') }}">--}}
{{--                                <i class="fas fa-layer-group"></i>--}}
{{--                                Roles--}}
{{--                            </a>--}}
{{--                        </li>--}}

{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="{{ route('permissions.index') }}">--}}
{{--                                <i class="fas fa-user-lock"></i>--}}
{{--                                Permissions--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        @endrole--}}
{{--                        @hasanyrole('Admin|Editor|Fundraising')--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="{{ route('contacts.index') }}">--}}
{{--                                <i class="far fa-envelope"></i>--}}
{{--                                Emails--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        @endrole--}}
{{--                        @hasanyrole('Admin|Editor')--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="{{ route('tags.index') }}">--}}
{{--                                <i class="fas fa-tags"></i>--}}
{{--                                Tags--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        @endrole--}}
{{--                        @role('Admin')--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="{{ route('image-sizes.index') }}">--}}
{{--                                <i class="far fa-image"></i>--}}
{{--                                Image Sizes--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="{{ route('api.index') }}">--}}
{{--                                <i class="fas fa-link"></i>--}}
{{--                                Everging API--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        @endrole--}}

                    </ul>
                </li>
                @hasanyrole('Admin|Editor')
                <li class="nav-item top-menu">
                    <a class="nav-link clickable" href="#">
                        <i class="fas fa-globe"></i>
                        Front End
                        <i class="fas fa-caret-down pull-right"></i>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('pages.index') }}">
                                <i class="far fa-file"></i>
                                Pages
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('menus') }}">
                                <i class="fas fa-bars"></i>
                                Menu Builder
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('templates.index') }}">
                                <i class="fas fa-columns"></i>
                                Templates
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('blocks.index') }}">
                                <i class="fas fa-stop"></i>
                                Blocks
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('content.index') }}">
                                <i class="fas fa-th"></i>
                                Content Blocks
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('image-uploader.index') }}">
                                <i class="far fa-image"></i>
                                Image Uploader
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('slideshows.index') }}">
                                <i class="far fa-images"></i>
                                Slideshows
                            </a>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('faqs.index') }}">
                                <i class="fas fa-question"></i>
                                FAQ's
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('appeals.index') }}">
                                <i class="fas fa-walking"></i>
                                Appeals Info
                            </a>
                        </li>
                    </ul>
                </li>
                @endrole
                <li class="nav-item top-menu">
                    <a class="nav-link clickable" href="#">
                        <i class="fas fa-briefcase"></i>
                        Jobs
                        <i class="fas fa-caret-down pull-right"></i>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('jobs-admin.index') }}">
                                <i class="fas fa-briefcase"></i>
                                Jobs
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('job-applications.index') }}">
                                <i class="fas fa-clipboard"></i>
                                Applications
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item top-menu">
                    <a class="nav-link clickable" href="#">
                        <i class="fa fa-door-open"></i>
                        Fundraisers
                        <i class="fas fa-caret-down pull-right"></i>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('applications.index') }}">
                                <i class="fas fa-users"></i>
                                Applications
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-chart-line"></i>
                        Reports
                    </a>
                </li>

            </ul>
        </div>
    </nav>
</div>
