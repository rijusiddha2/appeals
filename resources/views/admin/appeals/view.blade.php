@extends('admin.layouts.dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    Appeal Details
                </div>

                <div class="box-content">
                    <div class="row">
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>Title</dt>
                                <dd>{{ $appeal->title }}</dd>
                                <dt>Total Raised</dt>
                                <dd>£{{ $appeal->total_raised }}</dd>
                                <dt>No. Of supporters</dt>
                                <dd>{{ $appeal->supporters }}</dd>
                                <dt>Intro Text</dt>
                                <dd>{{ $appeal->intro }}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6">
                            <img src="/images/large_landscape/{!! $image->name !!}" class="img-responsive" alt="{!! $image->alt_tag !!}" title="{!! $image->alt_tag !!}"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('appeals.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit Appeal')
                <a href="{{ route('appeals.edit', $appeal->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @endcan

        </div>
    </div>
@stop