@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            <div class="box-buttons">
                <a href="{{ route('appeals.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
            </div>
            Appeals

        </div>
        <div class="box-content">
            <table class="table table-bordered" id="appeal-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Total Raised</th>
                    <th>No. of supporters</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <a class="btn btn-success pull-right" href="{{ route('appeals.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Appeal</a>

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#appeal-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('appeals.data')  }}'
                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'total_raised', name: 'total_raised' },
                    { data: 'supporters', name: 'supporters' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop