@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($appeal, ['route' => ['appeals.update', $appeal->id], 'method' => 'PUT'])  }}

    <div class="box">
        <div class="box-header">
            Appeal Details
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('title', 'Title')  }}
                        {{ Form::text('title', $appeal->title, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('total_raised', 'Total Raised')  }}
                        {{ Form::text('total_raised', $appeal->total_raised, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('supporters', 'Supporters')  }}
                        {{ Form::text('supporters', $appeal->supporters, ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('intro', 'Intro Text')  }}
                        {{ Form::text('intro', $appeal->intro, ['class' => 'form-control', 'rows' => 5])  }}
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="/images/large_landscape/{!! $image->name !!}" class="img-responsive" alt="{!! $image->alt_tag !!}" title="{!! $image->alt_tag !!}"/>
                </div>

            </div>

        </div>
    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('appeals.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Save</button>

        </div>
    </div>
    {{ Form::close()  }}
    @include('admin.images.partials.add-image-modal')
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#imageModal0').on("show.bs.modal", function (e) {
                var image = $(e.relatedTarget).data('image');
                $('.image').click(function(){
                    $('#' + image + '').html('<input type="hidden" name="image_id" value="' + $(this).data('id') + '"/><img src="' + $(this).attr('src') + '" alt="' +
                        $(this).attr('alt')+ '" class="img-responsive"/>');
                    $('#imageModal0').modal('hide');
                });

            });
        });
    </script>
@endsection
