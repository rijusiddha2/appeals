@extends('admin.layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    Evergiving API Details
                </div>

                <div class="box-content">
                    <div class="row">
                        <div class="col-md-8">
                            <dl class="dl-horizontal" >
                                <dt>API Key</dt>
                                <dd>{{ $apiKey }}</dd>
                                <dt>Campaign ID</dt>
                                <dd>{{ $api->campaign_id }}</dd>
                                <dt>Schema ID</dt>
                                <dd>{{ $api->schema_id }}</dd>
                                <dt>Type</dt>
                                <dd>{{ ucfirst($api->type)}}</dd>
                                <dt>Revision ID</dt>
                                <dd>{{ $api->revision_id}}</dd>
                            </dl>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('api.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            @can('Edit Evergiving API')
                <a href="{{ route('api.edit', $api->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt "></i> Edit</a>
            @endcan
        </div>
    </div>
@stop