@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
            <div class="box-buttons">
                <a href="{{ route('api.create') }}" data-toggle="tooltip" title="" class="box-btn tooltip-pivot" data-original-title="Add"><i class="fas fa-plus"></i></a>
            </div>
            Evergiving API's

        </div>
        <div class="box-content">
            <table class="table table-bordered" id="api-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>API Key (first 10 digits)</th>
                    <th>Campaign ID</th>
                    <th>Schema ID</th>
                    <th>Type</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <a class="btn btn-success pull-right" href="{{ route('api.create')  }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Evergiving API</a>

@stop

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            var oTable = $('#api-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('api.data')  }}'
                },
                order: [[ 1, "asc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'api_key', name: 'api_key' },
                    { data: 'campaign_id', name: 'campaign_id' },
                    { data: 'schema_id', name: 'schema_id' },
                    { data: 'type', name: 'type' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop