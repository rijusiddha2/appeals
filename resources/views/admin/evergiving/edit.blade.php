@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::model($api, ['route' => ['api.update', $api->id], 'method' => 'PUT'])  }}

    <div class="box">
        <div class="box-header">
            Appeal Details
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('api_key', 'API Key')  }}
                        {{ Form::text('api_key', $api->api_key, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('campaign_id', 'Campaign ID')  }}
                        {{ Form::text('campaign_id', $api->campaign_id, ['class' => 'form-control'])  }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('schema_id', 'Schema ID')  }}
                        {{ Form::text('schema_id', $api->schema_id, ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('type', 'Type')  }}
                        {{ Form::select('type',['' => '--- Please Select ---', 'contact' => 'Contact', 'pledge' => 'Pledge'],$api->type, ['class' => 'form-control selectpicker'])  }}

                    </div>

                </div>

            </div>

        </div>
    </div>

    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('api.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Save</button>

        </div>
    </div>
    {{ Form::close()  }}

@endsection

