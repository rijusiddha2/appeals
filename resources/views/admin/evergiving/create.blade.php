@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::open(['url' => 'admin/api'])  }}

    <div class="box">
        <div class="box-header">
            Evergiving API Details
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('api_key', 'API Key')  }}
                        {{ Form::text('api_key', old('api_key'), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('campaign_id', 'Campaign ID')  }}
                        {{ Form::text('campaign_id', old('campaign_id'), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('schema_id', 'Schema ID')  }}
                        {{ Form::text('schema_id',old('schema_id'), ['class' => 'form-control'])  }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('type', 'Type')  }}
                        {{ Form::select('type',['' => '--- Please Select ---', 'contact' => 'Contact', 'pledge' => 'Pledge'], old('type'), ['class' => 'form-control selectpicker'])  }}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('api.index') }}" class="btn btn-secondary"><i class="fas fa-list"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Create</button>
        </div>
    </div>

    {{ Form::close()  }}

@endsection