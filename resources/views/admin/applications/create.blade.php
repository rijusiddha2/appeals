@extends('admin.layouts.dashboard')
@section('content')
    {{ Form::open(['url' => 'admin/applications'])  }}
    <h1>Application Form</h1>

    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Your Details</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('title', 'Title', ['control-label']) }}
                        {{ Form::select('title', ['Mr' => 'Mr', 'Miss' => 'Miss', 'Mrs' => 'Mrs', 'Ms' => 'Ms'], old('title'), ['class' => 'form-control selectpicker']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('first_name', 'First Name', ['control-label']) }}
                        {{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'required']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('last_name', 'Last Name', ['control-label']) }}
                        {{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'required']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('order_name', 'Order Name (if applicable)', ['control-label']) }}
                        {{ Form::text('order_name', old('order_name'), ['class' => 'form-control']) }}
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('gender', 'Gender', ['control-label']) }}
                        {{ Form::select('gender', ['male' => 'Male', 'female' => 'Female', 'other' => 'Other'], old('gender'), ['class' => 'form-control selectpicker']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('address', 'Address', ['control-label']) }}
                        {{ Form::textarea('address', old('address'), ['class' => 'form-control', 'rows' => 4, 'required']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('postcode', 'Postcode', ['control-label']) }}
                        {{ Form::text('postcode', old('postcode'), ['class' => 'form-control', 'required']) }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ Form::label('email', 'Email', ['control-label']) }}
                        {{ Form::email('email', old('email'), ['class' => 'form-control', 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('mobile', 'Mobile', ['control-label']) }}
                        {{ Form::text('mobile', old('mobile'), ['class' => 'form-control', 'required']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('alt_tel', 'Alternative Telephone', ['control-label']) }}
                        {{ Form::text('alt_tel', old('alt_tel'), ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">About You</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        {{ Form::label('details[work_experience]', 'Please tell us a little about your work experience', ['control-label']) }}
                        {{ Form::textarea('details[work_experience]', old('details[work_experience]'), ['class' => 'form-control', 'rows' => 4, 'required']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('details[triratna_history]', 'What is your history of involvement with the Triratna community?', ['control-label']) }}
                        {{ Form::textarea('details[triratna_history]', old('details[triratna_history]'), ['class' => 'form-control', 'rows' => 4, 'required']) }}
                    </div>

                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <p>How did you find out about Karuna Appeals?</p>
                        <label>
                            {{ Form::checkbox('details[contact_source][]', 'friend', old('details[contact_source][]')) }}
                            Friend / Associate
                        </label><br/>
                        <label>
                            {{ Form::checkbox('details[contact_source][]', 'poster', old('details[contact_source][]')) }}
                            Poster / Leaflet
                        </label>
                        <br/>
                        <label>
                            {{ Form::checkbox('details[contact_source][]', 'talk', old('details[contact_source][]')) }}
                            Talk /Presentation
                        </label>
                        <br/>
                        <label>
                            {{ Form::checkbox('details[contact_source][]', 'previous', old('details[contact_source][]')) }}
                            Previous Appeal
                        </label>
                        <br/>
                        <label>
                            {{ Form::checkbox('details[contact_source][]', 'social_media', old('details[contact_source][]')) }}
                            Social Media / Web Search
                        </label>
                        <br/>
                        <label>
                            {{ Form::checkbox('details[contact_source][]', 'other', old('details[contact_source][]')) }}
                            Other
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">On the appeal</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <p>What type of appeal are you interested in?</p>
                        <label>
                            {{ Form::checkbox('appeals[type][]', 'door', old('appeals[type][]')) }}
                            Door to Door Appeal (Live-in community, locations vary)
                        </label>
                        <label>
                            {{ Form::checkbox('appeals[type][]', 'telephone', old('appeals[type][]')) }}
                            Telephone Appeal (Non-residential, Karuna office, London, N7)
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('appeals[season]', 'Season', ['class' => 'control-label']) }}
                        {{ Form::select('appeals[season]', ['winter' => 'Winter', 'spring' => 'Spring', 'summer' => 'Summer', 'autumn' => 'Autumn'], old('appeals[season]'), ['class' => 'control-label selectpicker', 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('appeals[year]', 'Year', ['class' => 'control-label']) }}
                        {{ Form::text('appeals[year]', old('appeals[year]'), ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('appeals[comments]', 'Please add any other comments regards your availability', ['class' => 'control-label']) }}
                        {{ Form::textarea('appeals[comments]', old('appeals[comments]'), ['class' => 'form-control', 'rows' => 4]) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('appeals[draw]', 'What is it that draws you to want to do a Karuna Appeal?', ['class' => 'control-label']) }}
                        {{ Form::textarea('appeals[draw]', old('appeals[draw]'), ['class' => 'form-control', 'rows' => 4, 'required']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('appeals[skills]', 'What do you think you would bring to the Appeal?', ['class' => 'control-label']) }}
                        {{ Form::textarea('appeals[skills]', old('appeals[skills]'), ['class' => 'form-control', 'rows' => 4, 'required']) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('appeals[difficulties]', 'What difficulties might you anticipate doing an Appeal?', ['class' => 'control-label']) }}
                        {{ Form::textarea('appeals[difficulties]', old('appeals[difficulties]'), ['class' => 'form-control', 'rows' => 4, 'required']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Health & Wellbeing</h3>
        </div>
        <div class="panel-body">
            <p>Karuna appeals can be both physically and mentally demanding. Giving us an idea of any physical or mental health challenges will help us make provisions on the appeal. If you would prefer not answer questions on your health please leave this section blank.</p>
            <hr/>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <p>How would you rate your health and fitness level?</p>
                        <label>
                            {{ Form::radio('details[health]', 'good', old('details[health]')) }}
                            Good
                        </label>
                        <br/>
                        <label>
                            {{ Form::radio('details[health]', 'Reasonable', old('details[health]')) }}
                            Reasonable
                        </label>
                        <br/>
                        <label>
                            {{ Form::radio('details[health]', 'Poor', old('details[health]')) }}
                            Poor
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    {{ Form::label('details[health_details]', 'If poor, please give details. There will be a lot of walking and standing.') }}
                    {{ Form::textarea('details[health_details]', old('details[health_details]'), ['class' => 'form-control', 'rows' => 4]) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <p>Have you ever had psychiatric treatment or been prescribed medication for psychological reasons (e.g. depression, eating difficulties, alcohol or drug abuse)?</p>
                        <label>
                            {{ Form::radio('details[medication]', 0, old('details[medication]')) }}
                            No
                        </label>
                        <br/>
                        <label>
                            {{ Form::radio('details[medication]', 1, old('details[medication]')) }}
                            Yes
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    {{ Form::label('details[medication_details]', 'If yes, please give details.') }}
                    {{ Form::textarea('details[medication_details]', old('details[medication_details]'), ['class' => 'form-control', 'rows' => 4]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Safeguarding</h3>
        </div>
        <div class="panel-body">
            <p>Karuna is committed to protecting children, young people and vulnerable adults from all forms of physical or psychological violence, injury or abuse, neglect, maltreatment, exploitation and sexual abuse. To read our safeguarding
                policy, please visit <a target="_blank" href="https://www.karuna.org">www.karuna.org</a>.</p>
            <hr/>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <p>Have you been the subject of any disciplinary investigation and/or sanction by any organisation due to concerns about your behaviour towards children and/or vulnerable adults?</p>
                        <label>
                            {{ Form::radio('details[safeguarding_offence]', 1, old('details[safeguarding_offence]')) }}
                            Yes
                        </label>
                        <br/>
                        <label>
                            {{ Form::radio('details[safeguarding_offence]', 0, old('details[safeguarding_offence]')) }}
                            No
                        </label>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>Do you have any criminal record offences which are currently unspent under the Rehabilitation of Offenders Act 1974 (You are not required to disclose anything that is deemed ‘spent’).</p>
                        <label>
                            {{ Form::radio('details[criminal_offence]', 1, old('details[criminal_offence]')) }}
                            Yes
                        </label>
                        <br/>
                        <label>
                            {{ Form::radio('details[criminal_offence]', 0, old('details[criminal_offence]')) }}
                            No
                        </label>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('details[offence_details]', 'If you have answered YES to either question above or if you have any reason to believe that by fundraising for Karuna you may pose a risk to the safety of children or
                        vulnerable adults,
                        please give additional details here.') }}
                        {{ Form::textarea('details[offence_details]', old('details[offence_details]'), ['class' => 'form-control', 'rows' => 4]) }}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">References</h3>
        </div>
        <div class="panel-body">
            <p>Please provide the name and contact details of 2 order members who know you well enough to give a reference.</p>
            <hr/>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {{ Form::label('reference[name_1]', 'Name Referee 1', ['control-label']) }}
                        {{ Form::text('reference[name_1]', old('reference[relationship_1]'), ['class' => 'form-control', 'required']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('reference[relationship_1]', 'Context of Relationship', ['control-label']) }}
                        {{ Form::text('reference[relationship_1]', old('reference[relationship_1]'), ['class' => 'form-control', 'required']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('reference[telephone_1]', 'Telephone', ['control-label']) }}
                        {{ Form::text('reference[telephone_1]', old('reference[telephone_1]'), ['class' => 'form-control', 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('reference[email_1]', 'Email', ['control-label']) }}
                        {{ Form::email('reference[email_1]', old('reference[email_1]'), ['class' => 'form-control', 'required']) }}
                    </div>

                </div>
                <div class="col-md-3">

                    <div class="form-group">
                        {{ Form::label('reference[name_2]', 'Name Referee 2', ['control-label']) }}
                        {{ Form::text('reference[name_2]', old('reference[name_2]'), ['class' => 'form-control', 'required']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('reference[relationship_2]', 'Context of Relationship', ['control-label']) }}
                        {{ Form::text('reference[relationship_2]', old('reference[relationship_2]'), ['class' => 'form-control', 'required']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('reference[telephone_2]', 'Telephone', ['control-label']) }}
                        {{ Form::text('reference[telephone_2]', old('reference[telephone_2]'), ['class' => 'form-control', 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('reference[email_2]', 'Email', ['control-label']) }}
                        {{ Form::email('reference[email_2]', old('reference[email_2]'), ['class' => 'form-control', 'required']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Support Package</h3>
        </div>
        <div class="panel-body">
            <p>We offer one of two support packages:</p>
            <ul>
                <li>Package 1 (for those in the UK and Ireland):Up to £250 per week to cover rent and bills incurred ‘back home’ while on the Appeal and travel to and from the Appeal. This includes a £70 per week allowance (from which one makes a
                    contribution to the Appeal food kitty).</li>

                <li>Package 2 (for those from overseas):Up to £600 towards a return flight to the UK, internal UK travel to and from the Appeal, £70 per week (from which one makes a contribution to the food kitty).</li>
            </ul>
            <p>On completion of an Appeal there is a 7 day retreat allowance for use in any UK Triratna retreat centre.</p>
            <hr/>
            <div class="row">
                <div class="col-md-4">
                    <p>Which support package will you be requesting?</p>
                    <div class="form-group">
                        {{ Form::label('details[support_package]', 'Which support package will you be requesting?', ['class' => 'control-label']) }}
                        {{ Form::select('details[support_package]', ['' => '--- Please Select ---', 1 => 'Package 1', 2 => 'Package 2', 3 => 'Neither'], old('details[support_package]'), ['class' => 'form-control selectpicker']) }}

                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        {{ Form::label('details[support_details]', 'Please use this section to give us any other details you\'d like us to know about your expenses or support package request.') }}
                        {{ Form::textarea('details[support_details]', old('details[support_details]'), ['class' => 'form-control', 'rows' => 4]) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Appeal ethos</h3>
        </div>
        <div class="panel-body">
            <p>Karuna is a Triratna charity and Team Based Right Livelihood. Our door to door appeals are run like retreats and it is important that everyone participating has a:</p>
            <ol>
                <li>Commitment to Equality and Diversity and addressing discrimination</li>
                <li>High standard of personal integrity</li>
                <li>Sympathy to Buddhism as practiced within the Triratna community</li>
                <li>Sympathy with the Aims, Objectives and Ethos of the Karuna Trust</li>
            </ol></p>
            <div class="row">
                <div class="col-md-4">

                    <p>Are you willing to commit to the above ethos during the duration of the appeal?</p>
                    <label>
                        {{ Form::radio('details[ethos]', 1, old('details[ethos]')) }}
                        Yes
                    </label>
                    <br/>
                    <label>
                        {{ Form::radio('details[ethos]', 0, old('details[ethos]')) }}
                        No
                    </label>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        {{ Form::label('details[ethos_details]', 'If No please give details') }}
                        {{ Form::textarea('details[ethos_details]', old('details[ethos_details]'), ['class' => 'form-control', 'rows' => 4]) }}
                    </div>
                </div>

            </div>

        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Your Privacy</h3>
        </div>
        <div class="panel-body">
            <p>Karuna takes your privacy seriously. We will use your personal information to process your application and only in other ways which you would reasonably expect.</p>

            <p>Please indicate if you consent to your details being used in this way.</p>
            <label>
                {{ Form::radio('details[privacy]', 1, old('details[privacy]')) }}
                Yes
            </label>
            <br/>
            <label>
                {{ Form::radio('details[privacy]', 0, old('details[privacy]')) }}
                No
            </label>
            <hr/>
            <p>Your information will be kept for the length of the application process and destroyed if you do not proceed to become a volunteer, unless you otherwise give your consent for us to keep information for the purposes of future opportunities. </p>

            <p>Please indicate if you are happy for us to keep your personal information for future opportunities.</p>
            <label>
                {{ Form::radio('details[future_opportunities]', 1, old('details[future_opportunities]')) }}
                Yes
            </label>
            <br/>
            <label>
                {{ Form::radio('details[future_opportunities]', 0, old('details[future_opportunities]')) }}
                No
            </label>
            <hr/>
            <p>We will never sell or trade your details. To read more about how we value and respect your privacy and data, please visit <a target="_blank" href="https://www.karuna.org/privacy-policy">www.karuna.org</a>.
                We are registered as a data controller with the UK Information Commissioner’s Office as Karuna Trust, Registration No. 327461.</p>
        </div>
    </div>
    <div class="form-action clearfix">
        <div class="pull-right">
            <a href="{{ route('applications.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Create</button>

        </div>
    </div>

    {{ Form::close() }}
@endsection