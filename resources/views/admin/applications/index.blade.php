@extends('admin.layouts.dashboard')

@section('content')

    <div class="box">
        <div class="box-header">
           Applications
        </div>

        <div class="box-content">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="applications-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Applicant</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Applied</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function() {
            $.noConflict(true)
            $('#applications-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('applications.data')  }}',
                columns: [
                    { data: 'id', id: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'created_at', name: 'created_at' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });

        });

    </script>
@stop