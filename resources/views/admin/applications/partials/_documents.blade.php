<div id="documents" class="tab-pane fade" role="tabpanel" aria-labelledby="documents-tab">
    <div class="box">
        <div class="box-content">
            <table class="table table-bordered" id="users-table">
                <thead>
                <tr>
                    <th>Document</th>
                    <th>Date Added</th>
                    <th>Added By</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if (count($application->documents))
                    @foreach ($application->documents as $document)
                        <tr>
                            <td><a target="_blank" href="{{ asset('/storage/app/'.$document->filename)  }}">{{ $document->original_filename  }}</a></td>
                            <td>{{ $document->created_at->format('d/m/Y H:i')  }}</td>
                            <td>{{ $document->created_by  }}</td>
                            <td>
                                <a data-toggle="tooltip" data-original-title="View" target="_blank" href="{{ asset('/storage/app/'.$document->filename)  }}" class="tooltip-pivot float-left"><i class="fas fa-eye"></i></a>
                                @can('Delete Document')
                                    <a data-toggle="tooltip" data-original-title="Delete" href="{{ route('documents.delete', $document->id)  }}" class="tooltip-pivot float-left"><i class="fas fa-trash"></i></a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">There are no documents for this user</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>