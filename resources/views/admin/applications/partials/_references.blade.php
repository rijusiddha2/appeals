<div id="reference" class="tab-pane fade" role="tabpanel" aria-labelledby="reference-tab">
    <div class="box">
        <div class="box-content">
            <div class="row">
                @if(isset($application->references))
                    <div class="col-md-6">
                        <dl>
                            <dt>Name Referee 1</dt>
                            <dd>{{ $application->references->name_1 }}</dd>
                            <dt>Context of Relationship</dt>
                            <dd>{{ $application->references->relationship_1 }}</dd>
                            <dt>Telephone</dt>
                            <dd>{{ $application->references->telephone_1 }}</dd>
                            <dt>Email</dt>
                            <dd>{{ $application->references->email_1 }}</dd>
                        </dl>
                    </div>
                    <div class="col-md-6">
                        <dl>
                            <dt>Name Referee 1</dt>
                            <dd>{{ $application->references->name_2 }}</dd>
                            <dt>Context of Relationship</dt>
                            <dd>{{ $application->references->relationship_2 }}</dd>
                            <dt>Telephone</dt>
                            <dd>{{ $application->references->telephone_2 }}</dd>
                            <dt>Email</dt>
                            <dd>{{ $application->references->email_2 }}</dd>
                        </dl>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                            No referee data recorded!
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>