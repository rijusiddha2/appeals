<div id="contact" class="tab-pane fade active in" role="tabpanel" aria-labelledby="contact-tab">
    <div class="box">

        <div class="box-content">
            <div class="row">
                <div class="col-md-6">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $application->title  }} {{ $application->first_name }} {{ $application->last_name }}</dd>
                        <dt>Order Name</dt>
                        <dd>{{ $application->order_name }}</dd>
                        <dt>Gender</dt>
                        <dd>{{ $application->gender  }}</dd>
                        <dt>Address</dt>
                        <dd>{{ $application->address  }} <br/> {{ $application->postcode }}</dd>
                    </dl>
                </div>

                <div class="col-md-6">
                    <dl class="dl-horizontal">
                        <dt>Email</dt>
                        <dd>{{ $application->email  }}</dd>
                        <dt>Mobile</dt>
                        <dd>{{ $application->mobile  }}</dd>
                        <dt>Telephone (Alt)</dt>
                        <dd>{{ $application->alt_tel }}</dd>
                        <dt>Created By</dt>
                        <dd>{{ $application->created_by != 0 ? $application->author->name : 'Web'  }}</dd>

                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
