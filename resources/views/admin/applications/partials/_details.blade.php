<div class="tab-pane fade" id="details" role="tabpanel" aria-labelledby="details-tab">
    <div class="box">
        <div class="box-content">
            @if(isset($application->details))
                <div class="row">
                    <div class="col-md-6">
                        <dl>
                            <dt>Work experience</dt>
                            <dd>{{ $application->details->work_experience }}</dd>

                            <dt>Triratna Involvement</dt>
                            <dd>{{ $application->details->triratna_history }}</dd>

                            <dt>How found out about Karuna Appeals</dt>
                            <dd>{{ $application->details->contact_source }}</dd>

                        </dl>
                    </div>
                    <div class="col-md-6">
                        <dl>
                            <dt>Health and Fitness level</dt>
                            <dd>{{ $application->details->health }}</dd>
                            <dt>Health Details</dt>
                            <dd>{{ $application->details->health_details }}</dd>
                            <dt>Any phsychiatric treatment or psychological medication?</dt>
                            <dd>{{ $application->details->medication == 1 ? 'Yes' : 'No' }}</dd>
                            <dt>Phsychiatric details</dt>
                            <dd>{{ $application->details->medication_details }}</dd>
                        </dl>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <dl>
                            <dt>Safeguarding investigations?</dt>
                            <dd>{{ $application->details->safeguarding_offence == 1 ? 'Yes' : 'No' }}</dd>

                            <dt>Criminal Offences?</dt>
                            <dd>{{ $application->details->criminal_offence == 1 ? 'Yes' : 'No'  }}</dd>

                            <dt>Safegaurding and/or Criminal details</dt>
                            <dd>{{ $application->details->offence_details }}</dd>

                        </dl>
                    </div>
                    <div class="col-md-6">
                        <dl>
                            <dt>Support Package</dt>
                            <dd>{{ $application->details->support_package }}</dd>
                            <dt>Additional Support Details</dt>
                            <dd>{{ $application->details->support_extras }}</dd>
                        </dl>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <dl>
                            <dt>Ethos Commitment?</dt>
                            <dd>{{ $application->details->ethos == 1 ? 'Yes' : 'No' }}</dd>

                            <dt>Ethos Details if not agreed</dt>
                            <dd>{{ $application->details->ethos_details  }}</dd>
                        </dl>
                    </div>
                    <div class="col-md-6">
                        <dl>
                            <dt>Data Consent</dt>
                            <dd>{{ $application->details->privacy == 1 ? 'Yes' : 'No' }}</dd>
                            <dt>Keep details for future opportunities</dt>
                            <dd>{{ $application->details->future_opportunities == 1 ? 'Yes' : 'No' }}</dd>
                        </dl>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning">No details data recorded!</div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>