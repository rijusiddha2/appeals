<div id="notes" class="tab-pane fade" role="tabpanel" aria-labelledby="notes-tab">
    <div class="box">
        <div class="box-header">
            Notes
        </div>

        <div class="box-content">
            <table class="table table-bordered" id="users-table">
                <thead>
                <tr>
                    <th>Content</th>
                    <th>Date</th>
                    <th>Added By</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @if(count($application->notes))
                    @foreach($application->notes as $note)
                        <tr>
                            <td>{{ $note->content  }}</td>
                            <td>{{ $note->created_at->format('d/m/Y h:i')  }}</td>
                            <td>{{ $note->author->name  }}</td>
                            <td>
                                @can('Edit Application Note')
                                    <a data-toggle="tooltip" data-original-title="Edit" href="{{ route('applications.note.edit', [$note->id, 'application' => $application->id])  }}" class="tooltip-pivot float-left"><i class="fas
                                    fa-pencil-alt"></i></a>
                                @endcan
                                @can('Delete Application Note')
                                    <a data-toggle="tooltip" data-original-title="Delete" href="{{ route('applications.note.delete', [$note->id, 'application' => $application->id])  }}" class="tooltip-pivot float-left"><i class="fas fa-trash"></i></a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">No notes exist for this user</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

    </div>
</div>