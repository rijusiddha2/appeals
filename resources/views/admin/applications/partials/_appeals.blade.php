<div id="appeals" class="tab-pane fade" role="tabpanel" aria-labelledby="appeals-tab">
    <div class="box">
        <div class="box-content">
            <div class="row">
                @if(isset($application->appeals))
                    <div class="col-md-6">
                        <dl>
                            <dt>Type of appeal(s) interested in</dt>
                            <dd>{{ $application->appeals->type }}</dd>
                            <dt>Season</dt>
                            <dd>{{ $application->appeals->season }}</dd>
                            <dt>Year</dt>
                            <dd>{{ $application->appeals->year }}</dd>
                            <dt>Other Comments</dt>
                            <dd>{{ $application->appeals->comments }}</dd>
                        </dl>
                    </div>
                    <div class="col-md-6">
                        <dl>
                            <dt>What is it that draws you to want to do a Karuna Appeal?</dt>
                            <dd>{{ $application->appeals->draw }}</dd>
                            <dt>What do you think you would bring to the Appeal?</dt>
                            <dd>{{ $application->appeals->skills }}</dd>
                            <dt>What difficulties might you anticipate doing an Appeal?</dt>
                            <dd>{{ $application->appeals->difficulties }}</dd>
                        </dl>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                            No appeals data recorded!
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>