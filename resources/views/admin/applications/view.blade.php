@extends('admin.layouts.dashboard')

@section('content')

    <ul class="nav nav-tabs" id="application" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="true">Contact</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="false">Details</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="appeals-tab" data-toggle="tab" href="#appeals" role="tab" aria-controls="appeals" aria-selected="false">Appeals</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="reference-tab" data-toggle="tab" href="#reference" role="tab" aria-controls="reference" aria-selected="false">Reference</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="documents-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="documents" aria-selected="false">Documents</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="notes-tab" data-toggle="tab" href="#notes" role="tab" aria-controls="notes" aria-selected="false">Notes</a>
        </li>
    </ul>

    <div class="tab-content" id="applicationContent">
        @include('admin.applications.partials._contact')
        @include('admin.applications.partials._details')
        @include('admin.applications.partials._appeals')
        @include('admin.applications.partials._references')
        @include('admin.applications.partials._documents')
        @include('admin.applications.partials._notes')
    </div>


<div class="form-action clearfix">
    <div class="pull-right">
        <a href="{{ route('applications.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
        @can('Edit Application')
            <a href="{{ route('applications.edit', $application->id) }}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>  Edit</a>
        @endcan
        @can('Add Application Note')
            <a href="{{ route('applications.note', $application->id) }}" class="btn btn-success"><i class="fas fa-paperclip"></i>  Add Note</a>
        @endcan
        <a href="{{route('applications.pdf', $application->id)}}" class="btn btn-secondary"><i class="fas fa-download"></i> Download and Print as PDF</a>
        @can('Add Document')
            <a href="{{ route('documents.create', ['model' => 'App\Application','application_id' => $application->id]) }}" class="btn btn-success"><i class="fas fa-file"></i>  Add Document</a>
        @endcan
        @can('Delete Application')
            <a href="{{ route('applications.delete', $application->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash"></i>  Delete</a>
        @endcan
    </div>
</div>
@stop
