@extends('admin.layouts.dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            {{ $title  }}
        </div>
        <div class="box-content">
            {{ Form::open(['route' => ['applications.note.update', $applicationNote->id], 'method' => 'PUT']) }}

            <div class="form-group">
                {{ Form::hidden('application_id', $application->id) }}
                {{ Form::label('content', 'Content') }}
                {{ Form::textarea('content',  $applicationNote->content, ['class' => 'form-control']) }}
            </div>

        </div>
    </div>
    <div class="form-action clearfix">
        <div class="float-right">
            <a href="{{ route('applications.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <button type="submit" class="btn btn-primary"> <i class="fas fa-pencil-alt"></i> Update Note</button>
            {{ Form::close() }}
        </div>
    </div>

@stop
