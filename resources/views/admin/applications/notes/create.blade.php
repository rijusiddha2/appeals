@extends('admin.layouts.dashboard')

@section('content')
    {{ Form::open(['route' => ['applications.note',$application->id]]) }}
    <div class="box">
        <div class="box-header">
            {{ $title  }}
        </div>
        <div class="box-content">

            <div class="form-group">
                {{ Form::hidden('application_id', $application->id) }}
                {{ Form::label('content', 'Content') }}
                {{ Form::textarea('content',  old('content'), ['class' => 'form-control']) }}
            </div>

        </div>
    </div>

    <div class="form-action clearfix">
        <div class="float-right">
            <a href="{{route('applications.index') }}" class="btn btn-secondary"><i class="fas fa-bars"></i> Back to list</a>
            <button type="submit" class="btn btn-success "> <i class="fas fa-plus"></i> Add Note</button>
        </div>
    </div>
    {{ Form::close() }}
@stop
