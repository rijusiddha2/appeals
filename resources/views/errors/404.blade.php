@extends('layouts.app')

@section('content')

    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li><i class="far fa-alert-circle"></i> {{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <img src="/images/404.jpg" class="img-responsive center-block"/>
    @endsection