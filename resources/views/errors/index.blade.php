<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li><i class="far fa-alert-circle"></i> {{ $error }}</li>
        @endforeach
    </ul>
</div>