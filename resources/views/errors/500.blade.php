@extends('admin.layouts.dashboard')

@section('content')
    <h1 class="text-center">Uh Oh! You've torn a hole in the internet!</h1>
    <img src="/images/500.png" class="img-responsive center-block"/>
    <h2 class="text-center">Don't worry IT will fix it. Better let them know so they can get the sticky tape out.</h2>
@endsection